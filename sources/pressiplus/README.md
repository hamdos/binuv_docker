
---

## Reuqirement to run this Spring Boot Application

You need to install Java 8 and Postgres 12.2

1. Create database schema with any name
2. Change database configuration in application.properties:
    ```
    sources/pressiplus/src/main/resources/application.properties
    
    spring.datasource.url=jdbc:postgresql://localhost:5432/binuv_db?user=root&password=root
    ```
    
    * binuv_db is name of database, you can change it
    * user and password are root here, please update your credentials
3. Run the following maven comand to download all dependencies:
    ```
    mvn clean install
    ```

4. Run spring boot with:
    ````
   mvn spring-boot:run
    ````

### You use IDE like Intellij, you can use maven clean and install, then run the application

---

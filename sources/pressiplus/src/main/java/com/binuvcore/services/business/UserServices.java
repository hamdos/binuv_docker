package com.binuvcore.services.business;

import com.binuvcore.businessobjects.access.User;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Implementation class for {@link ProviderServices}.
 *
 * @author doston
 * @since  24.12.2019
 */
@Component
@ComponentScan(lazyInit = true)
public interface UserServices {

    //~ Methods --------------------------------------------------------------------------------------------------------

    /**
     * Add a person to db.
     *
     * @param  user
     *
     * @return response
     */
    public boolean addUser(User user);

    /**
     * Get all deleted users
     *
     * @return list of users
     */
    public List<User> getAllDeletedUsers();

    /**
     * Get all undeleted users
     *
     * @return list of users
     */
    public List<User> getAllUnDeletedUsers();

    /**
     * Get all users.
     *
     * @return found users from db
     */
    public List<User> getAllUsers();

    /**
     * Get all users those have this first name
     *
     * @param  firstName given first name
     *
     * @return list of users
     */
    public List<User> getAllUsersByThisFirstName(String firstName);

    /**
     * Get a user by email.
     *
     * @param  email given email
     *
     * @return found user
     */
    User getUserByEmail(String email);

    /**
     * Get a user by id.
     *
     * @param  userId given id
     *
     * @return found user
     */
    public User getUserById(Integer userId);
    public User loginUser(User user);

    public boolean deleteUserById(Integer id);

    void updateUserRole(User user);

}

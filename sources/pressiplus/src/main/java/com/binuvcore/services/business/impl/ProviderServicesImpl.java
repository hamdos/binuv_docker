package com.binuvcore.services.business.impl;

import com.binuvcore.businessobjects.Provider;

import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.repositories.ProviderRepository;

import com.binuvcore.services.business.ProviderServices;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * Implementation class for {@link ProviderServices}
 *
 * @author doston
 * @since 24.12.2019
 */
@Service
public class ProviderServicesImpl implements ProviderServices {

    //~ Instance fields ------------------------------------------------------------------------------------------------

    @Autowired
    private ProviderRepository providerRepository;


    //~ Methods --------------------------------------------------------------------------------------------------------

    @Override
    public boolean addProvider(Provider provider) {

        boolean res = false;
        provider.setDeleted(false);
        provider.setCreatedDate(DateTimeUtils.getCurrentDateInLong());
        provider.setDeletedDate(0);

        List<Provider> providerList = providerRepository.findProvidersByNameAndAddressAndApiKey(provider.getName(),
                provider.getAddress(), provider.getApiKey());
        if (providerList == null || providerList.isEmpty()) {
            providerRepository.save(provider);
            if (!providerRepository.findProvidersByNameAndAddressAndApiKey(provider.getName(), provider.getAddress(),
                    provider.getApiKey()).isEmpty()) {
                res = true;
            }
        }
        return res;
    }


    @Override
    public List<Provider> getProviderByNameContaining(String providerName) {
        List<Provider> providerList = providerRepository.findByNameContaining(providerName);

        if (providerList.size() > 0) {
            return providerList;
        }

        return null;
    }

    @Override
    public List<Provider> getAllProviders() {
        List<Provider> providerList = new ArrayList<>();
        providerRepository.findAll().forEach(providerList::add);
        return providerList;
    }

    @Override
    public Provider getProviderById(int personId) {
        Optional<Provider> optionalProvider = providerRepository.findById(personId);

        return optionalProvider.orElse(null);
    }
}

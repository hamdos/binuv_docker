package com.binuvcore.services.admin;

import com.binuvcore.businessobjects.custom.CustomSharedDto;
import com.binuvcore.repositories.custom.CustomSharedDtoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomSharedServicesImpl implements CustomSharedServices {

    @Autowired
    private CustomSharedDtoRepository customSharedDtoRepository;


    @Override
    public void saveCustomSharedDtoForReport(CustomSharedDto customSharedDto) {

        CustomSharedDto customSharedDto1 = customSharedDtoRepository.findByUserIdAndProgressReport(customSharedDto.getUserId(),
                customSharedDto.isProgressReport());
        if (customSharedDto1 == null) {
            customSharedDtoRepository.save(customSharedDto);
        } else {
            customSharedDto1.setShared(customSharedDto.isShared());
        }
    }

    @Override
    public void updateCutomSharedDto(CustomSharedDto customSharedDto) {

    }

    @Override
    public CustomSharedDto getCustomSharedByUserIdAndProgressReport(Integer userId, boolean progressReport) {
        return customSharedDtoRepository.findByUserIdAndProgressReport(userId, progressReport);
    }
}

package com.binuvcore.services.webapp.impl;

import com.binuvcore.businessobjects.access.Token;
import com.binuvcore.businessobjects.access.User;
import com.binuvcore.helper.StringHelper;
import com.binuvcore.repositories.TokenRepository;
import com.binuvcore.repositories.UserRepository;
import com.binuvcore.services.webapp.MailServiceBinuv;
import com.binuvcore.services.webapp.ResetPasswordServices;
import com.binuvcore.services.webapp.TokenServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ResetPasswordServicesImpl implements ResetPasswordServices {

    @Autowired
    private TokenServices tokenServices;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private StringHelper stringHelper = new StringHelper();

    @Autowired
    private MailServiceBinuv mailServiceBinuv;

    @Override
    public boolean resetPassword(final String tokenValue, final String newPassword) {

        if (stringHelper.isWhiteSpaceOrNull(tokenValue) || stringHelper.isWhiteSpaceOrNull(newPassword)) {
            return false;
        }

        Token token = tokenRepository.findByValue(tokenValue);
        if (token == null) {
            return false;
        }

        User user = userRepository.findUserByEmail(token.getEmail());
        user.setPassword(bCryptPasswordEncoder.encode(newPassword));
        token.setValid(false);
        tokenRepository.delete(token);
        userRepository.save(user);
        return true;
    }

    @Override
    public void requestPasswordReset(final String email, final String baseUrl) {

        if (stringHelper.isWhiteSpaceOrNull(email)) {
            return;
        }

        User user = userRepository.findUserByEmail(email);

        if (user == null) {
            return;
        }

        final String tokenV = baseUrl + "?token=" + tokenServices.generateNewToken(user.getEmail(), true);

        final String subject = "Password reset requested - BINUV";
        final String body = "Dear " + user.getFirstName() + " " + user.getLastName() + ", <br> <br>We, are BINUV Team, would like to inform that you have" +
                "sent us a request to reset your password.<br><br>" +
                "Please go to this " +
                "<a hreh=\"" + tokenV
                + "\">link </a> to reset your password.<br><br>" +
                "If the link does not work, go copy this url and paste it into browser bar: " + tokenV
                + "<br> <br> Thank you so much for contacting us and have a great day. <br>  Kind regards, <br> BINUV Team" +
                "<br>http://www.binuv.com <br> info@binuv.com";

        mailServiceBinuv.sendEmail(user.getEmail(), subject, body);
    }
}

package com.binuvcore.services.pressifier;

import com.binuvcore.businessobjects.pressifier.ArticleDetailDtoTransformer;
import com.binuvcore.businessobjects.pressifier.ArticleDtoTransformer;
import com.binuvcore.businessobjects.pressifier.MainJsonTransformer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan(lazyInit = true)
public interface PressifierServices {

    MainJsonTransformer getAllArticles(String lang, String latitude, String longitude, String next);
    ArticleDetailDtoTransformer getArticleById(String id);
    List<ArticleDtoTransformer> getArticlesByCategoryId(Integer categoryId);
    List<ArticleDtoTransformer> getArticlesByCategoryName(String categoryName);
}

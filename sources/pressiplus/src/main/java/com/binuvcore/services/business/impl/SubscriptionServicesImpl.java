package com.binuvcore.services.business.impl;

import com.binuvcore.businessobjects.Provider;
import com.binuvcore.businessobjects.Subscription;
import com.binuvcore.businessobjects.access.User;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.repositories.SubscriptionRepository;
import com.binuvcore.services.business.SubscriptionServices;
import com.binuvcore.services.business.ProviderServices;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SubscriptionServicesImpl implements SubscriptionServices {

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    UserServices userServices;

    @Autowired
    ProviderServices providerServices;

    private List<Subscription> subscriptionList = new ArrayList<>();

    @Override
    public boolean addSubscriptionByUserIdAndProviderId(Integer userId, Integer providerId) {
        boolean response = false;
        User user = userServices.getUserById(userId);
        Provider provider = providerServices.getProviderById(providerId);

        if (user != null && provider != null){
            Subscription sub = subscriptionRepository.findSubscriptionsByUserAndProvider(user, provider);
            if (sub == null || !sub.getUser().getId().equals(user.getId())){
                sub = new Subscription();
                sub.setSubscribedDate(DateTimeUtils.getCurrentDateInLong());
                sub.setUnsubscribed(false);
                sub.setUser(user);
                sub.setProvider(provider);
                subscriptionRepository.save(sub);
                response = true;
            }

            if (sub.getUser().getId().equals(user.getId()) && sub.getProvider().getId().equals(provider.getId())
                    && sub.isUnsubscribed()){
                sub.setUnsubscribed(false);
                subscriptionRepository.save(sub);
            }
        }
        return response;
    }

    @Override
    public List<Subscription> getAllSubscriptions() {
        Iterable<Subscription> iterable = subscriptionRepository.findAll();
        iterable.forEach(subscriptionList::add);
        return subscriptionList;
    }

    @Override
    public Subscription getSubscriptionById(Integer id) {



        return null;
    }

    @Override
    public List<Subscription> getSubscriptionsByUserId(Integer userId) {

        final User user = userServices.getUserById(userId);
        if (user != null){
            subscriptionList = subscriptionRepository.findSubscriptionsByUser(user);
        }
        return subscriptionList;
    }

    @Override
    public List<Subscription> getSubscriptionsByProviderId(Integer providerId) {
        return null;
    }

    @Override
    public boolean unsubscribeUserFromProvider(Integer userId, Integer providerId) {
        boolean response = false;
        User user = userServices.getUserById(userId);
        Provider provider = providerServices.getProviderById(providerId);

        if (user != null && provider != null){
            Subscription sub = subscriptionRepository.findSubscriptionsByUserAndProvider(user, provider);
            if (sub != null && sub.getUser().getId().equals(user.getId()) && !sub.isUnsubscribed()){
                sub.setUnsubscribedDate(DateTimeUtils.getCurrentDateInLong());
                sub.setUnsubscribed(true);
                subscriptionRepository.save(sub);
                response = true;
            }
        }
        return response;
    }
}

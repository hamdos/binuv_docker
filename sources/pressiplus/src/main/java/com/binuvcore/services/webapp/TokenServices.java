package com.binuvcore.services.webapp;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@ComponentScan(lazyInit = true)
public interface TokenServices {

    String generateNewToken(final String email, final boolean isPasswordReset);

    boolean validateToken(final String value);
}

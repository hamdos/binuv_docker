package com.binuvcore.services.business;

import com.binuvcore.businessobjects.Article;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan(lazyInit = true)
public interface ArticleServices {

    public Article getArticleById(Integer id);

//    @Secured ({"ROLE_ADMIN"})
    public List<Article> getAllArticle();

    public List<Article> getAllArticleByProviderId(Integer provider_id);
    public List<Article> getAllArticleByCategoryId(Integer category_id);
    public List<Article> getAllArticleByProviderAndCategory(Integer provider_id, Integer category_id);
    public String getArticleMetaDatabyUrl(String url);
    
    public boolean addArticle(Article article);
    List<Article> getAllByTitleContains(String title);

}

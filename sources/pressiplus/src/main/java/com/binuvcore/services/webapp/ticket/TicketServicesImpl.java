package com.binuvcore.services.webapp.ticket;

import com.binuvcore.businessobjects.transformers.ticket.TicketContentOutDto;
import com.binuvcore.businessobjects.transformers.ticket.TicketMenuActions;
import com.binuvcore.businessobjects.transformers.ticket.TicketOutDto;
import com.binuvcore.businessobjects.webapp.ticket.Ticket;
import com.binuvcore.businessobjects.webapp.ticket.TicketContent;
import com.binuvcore.businessobjects.access.User;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.repositories.binuv.TicketContentRepository;
import com.binuvcore.repositories.binuv.TicketRepository;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class TicketServicesImpl implements TicketServices {

    @Autowired
    private UserServices userServices;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private TicketContentRepository ticketContentRepository;

    @Override
    public TicketOutDto getTicketById(Integer ticketId, User loggedUser) {
        Optional<Ticket> savedTicket = ticketRepository.findById(ticketId);
        if (savedTicket.isPresent()) {
            List<TicketContent> contentList = ticketContentRepository.findAllByTicketId(savedTicket.get().getId());
            return transformerTicket(savedTicket.get(), contentList, loggedUser);
        } else {
            return new TicketOutDto();
        }
    }

    @Override
    public List<TicketOutDto> allTicketsByUser(User loggedUser) {
        List<TicketOutDto> transformers = new ArrayList<>();

        List<Ticket> ticketSupportList = new ArrayList<>();

        ticketRepository.findAll().forEach(ticketSupportList::add);

        ticketSupportList.sort(Comparator.comparing(Ticket::getId).reversed());
        for (Ticket ticketSupport : ticketSupportList) {
            List<TicketContent> contentList = ticketContentRepository.findAllByTicketId(ticketSupport.getId());

            transformers.add(transformerTicket(ticketSupport, contentList, loggedUser));
        }
        return transformers;
    }

    @Override
    public List<TicketOutDto> allPublicTickets() {
        List<TicketOutDto> transformers = new ArrayList<>();

        List<Ticket> ticketSupportList = ticketRepository.findAllByPublicTicket(true);

        ticketSupportList.sort(Comparator.comparing(Ticket::getId).reversed());
        for (Ticket ticketSupport : ticketSupportList) {
            List<TicketContent> contentList = ticketContentRepository.findAllByTicketId(ticketSupport.getId());

            transformers.add(transformerTicket(ticketSupport, contentList, null));
        }
        return transformers;
    }

    private TicketOutDto transformerTicket(Ticket ticketSupport, List<TicketContent> ticketSupportContentList, User loggedUser) {

        TicketOutDto transformer = new TicketOutDto();
        transformer.setSubject(ticketSupport.getTitle());
        transformer.setCreatedDate(DateTimeUtils.getShortDateTime(ticketSupport.getCreatedDate()));

        List<TicketContentOutDto> contentTransformerList = new ArrayList<>();

        for (TicketContent content : ticketSupportContentList) {
            TicketContentOutDto singleContent = new TicketContentOutDto();

            singleContent.setCreatedDate(DateTimeUtils.getShortDateTime(content.getCreatedDate()));
            singleContent.setMessage(content.getMessage());

            if (loggedUser != null && content.getUserId().equals(loggedUser.getId())) {
                singleContent.setUserName(loggedUser.getFirstName() + " " + loggedUser.getLastName());
                singleContent.setUserRole(loggedUser.getRole());
                singleContent.setOwner(true);
            } else {
                User ownerUser = userServices.getUserById(content.getUserId());
                singleContent.setUserName(ownerUser.getFirstName() + " " + ownerUser.getLastName());
                singleContent.setUserRole(ownerUser.getRole());
                singleContent.setOwner(false);
            }

            contentTransformerList.add(singleContent);

        }
        transformer.setTicketContents(contentTransformerList);
        transformer.setFormAction("/app/publish-support-ticket-post?ticketId=" + ticketSupport.getId());
        transformer.setTicketMenuActions(makeMenuActions(ticketSupport));
        return transformer;
    }

    private TicketMenuActions makeMenuActions(Ticket ticket){
        TicketMenuActions ticketMenuActions = new TicketMenuActions();
        ticketMenuActions.setDeleteTicket("/app/support/actions/delete/" + ticket.getId());
        ticketMenuActions.setLabelClosed("/app/support/actions/closed/" + ticket.getId());
        ticketMenuActions.setLabelOpen("/app/support/actions/open/" + ticket.getId());
        ticketMenuActions.setLabelPrivate("/app/support/actions/private/" + ticket.getId());
        ticketMenuActions.setLabelPublic("/app/support/actions/public/" + ticket.getId());
        ticketMenuActions.setClosed(ticket.isClosed());
        ticketMenuActions.setPublicTicket(ticket.isPublicTicket());
        return ticketMenuActions;
    }
}

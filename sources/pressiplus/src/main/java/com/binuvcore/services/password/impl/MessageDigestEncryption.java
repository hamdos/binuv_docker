package com.binuvcore.services.password.impl;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MessageDigestEncryption {

    private MessageDigest messageDigest;

    public MessageDigestEncryption() {
    }

    private MessageDigest getMessageDigest() {
        return messageDigest;
    }

    private void setMessageDigest(String algorithm) {
        if (messageDigest == null){
            try {
                this.messageDigest = MessageDigest.getInstance(algorithm);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
    }

    public void prepareMessageDigest(String hashAlgorithm){
        setMessageDigest(hashAlgorithm);
    }

    public String encrypt(String saltedPlaintext) {

        byte[] hashInBytes = getMessageDigest().digest(saltedPlaintext.getBytes(StandardCharsets.UTF_8));

        // bytes to hex
        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }
        return  sb.toString();
    }
}

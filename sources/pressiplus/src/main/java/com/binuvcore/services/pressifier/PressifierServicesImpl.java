package com.binuvcore.services.pressifier;

import com.binuvcore.businessobjects.Article;
import com.binuvcore.businessobjects.Author;
import com.binuvcore.businessobjects.Provider;
import com.binuvcore.businessobjects.pressifier.*;
import com.binuvcore.helper.*;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.repositories.ArticleRepository;
import com.binuvcore.repositories.AuthorRepository;
import com.binuvcore.repositories.PressifierRequestResitory;
import com.binuvcore.repositories.ProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PressifierServicesImpl implements PressifierServices {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ProviderRepository providerRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private PressifierRequestResitory pressifierRequestResitory;

    private final CommonHelper commonHelper = new CommonHelper();
    private final StringHelper stringHelper = new StringHelper();
    private final ConstantCategoryList constantCategoryList = ConstantCategoryList.getInstance();


    @Override
    public MainJsonTransformer getAllArticles(final String lang, final String latitude, final String longitude,
                                                    final String next) {


        final PressifierRequest pressifierRequest = getOldRequest();
        String version = null;
        List<Article> articleList = null;
        if (!stringHelper.isWhiteSpaceOrNull(lang) && (stringHelper.isWhiteSpaceOrNull(latitude)
                || stringHelper.isWhiteSpaceOrNull(longitude))) {
            if (LanguageDictionary.isValidLanguage(lang.toUpperCase())) {
                articleList = articleRepository.findAllByLanguage(lang.toUpperCase());

                if (pressifierRequest == null) {
                    version = recordNewRequest(articleList);
                }
                else if (oldContentThere(pressifierRequest, articleList.size(), "all_articles_lang")) {
                    version = pressifierRequest.getVersion();
                }
            } else {
                articleList = new ArrayList<>();
                articleRepository.findAll().forEach(articleList::add);
                if (pressifierRequest == null) {
                    version = recordNewRequest(articleList);
                }
                else if (oldContentThere(pressifierRequest, articleList.size(), "all_articles")) {
                    version = pressifierRequest.getVersion();
                }
            }
            //search by location and language
        } else if (!stringHelper.isWhiteSpaceOrNull(latitude) && !stringHelper.isWhiteSpaceOrNull(longitude)
                && !stringHelper.isWhiteSpaceOrNull(lang)) {
            final LocationDto locationDto = commonHelper.getAddressByLatAndLong(Double.parseDouble(latitude),
                    Double.parseDouble(longitude));
            if (locationDto != null) {
                articleList =  articleRepository.findAllByLocationContainsAndLanguage(locationDto.getAddress().getPostcode(), lang);
                if (pressifierRequest == null) {
                    version = recordNewRequest(articleList);
                }
                else if (oldContentThere(pressifierRequest, articleList.size(), "all_articles_lang_and_location")) {
                    version = pressifierRequest.getVersion();
                }
            } else {
                articleList = articleRepository.findAllByLanguage(lang.toUpperCase());
                if (pressifierRequest == null) {
                    version = recordNewRequest(articleList);
                }
                else if (oldContentThere(pressifierRequest, articleList.size(), "all_articles_lang")) {
                    version = pressifierRequest.getVersion();
                }
            }
        } else {
            articleList = new ArrayList<>();
            articleRepository.findAll().forEach(articleList::add);
            if (pressifierRequest == null) {
                version = recordNewRequest(articleList);
            }
            else if (oldContentThere(pressifierRequest, articleList.size(), "all_articles")) {
                version = pressifierRequest.getVersion();
            }
        }

        final MainJsonTransformer mainJsonTransformer = new MainJsonTransformer();

        if (stringHelper.isWhiteSpaceOrNull(version)) {
            version = stringHelper.getRandomString();
        }

        mainJsonTransformer.setVersion(version);
        mainJsonTransformer.setLast_updated(DateTimeUtils.getCurrentDateInString());
        mainJsonTransformer.setArticles(transformerArticles(articleList));
        mainJsonTransformer.setNext(null);
        mainJsonTransformer.setCategories(ConstantCategoryList.getInstance().getCategoryList());
        return mainJsonTransformer;

    }

    private List<ArticleDtoTransformer> transformerArticles(List<Article> articleList) {
        if (articleList == null) {
            return null;
        }

        List<ArticleDtoTransformer> transformerList = new ArrayList<>();

        for (Article article: articleList) {
            transformerList.add(articleDtoTransformer(article));
        }
        return transformerList;
    }

    private ArticleDtoTransformer articleDtoTransformer(final Article article) {
        ArticleDtoTransformer transformer = new ArticleDtoTransformer();
        transformer.setId(article.getId().toString());
        transformer.setTitle(article.getTitle());
        transformer.setTags(stringHelper.convertStringIntoTagsList(article.getEntities()));
        transformer.setThumbnail_url(article.getThumbnailUrl());
        if (article.getDescription().length() > 350) {
            transformer.setShort_description(article.getDescription().substring(0, 345));
        } else {
            transformer.setShort_description(article.getDescription());
        }

        transformer.setProvider(transformProvider(article));
        return transformer;
    }

    private ProviderDtoTransformer transformProvider(Article article) {
        Provider provider = providerRepository.findById(article.getProvider_id()).get();
        return new ProviderDtoTransformer(provider.getId().toString(), provider.getName(), provider.getLogoUrl());
    }

    @Override
    public ArticleDetailDtoTransformer getArticleById(String id) {
        Optional<Article> optionalArticle = articleRepository.findById(Integer.parseInt(id));
        return optionalArticle.map(this::articleDetailDtoTransformer).orElse(null);
    }

    @Override
    public List<ArticleDtoTransformer> getArticlesByCategoryId(Integer categoryId) {
        CategoryDto categoryDto = null;

        for (CategoryDto category : constantCategoryList.getCategoryList()) {
            if (category.getId().equals(categoryId)) {
                categoryDto = category;
                break;
            }
        }

        if (categoryDto == null) {
            return null;
        }

        List<Article> articleList = articleRepository.findAllByCategory(categoryDto.getName());
        List<ArticleDtoTransformer> transformers = new ArrayList<>();
        for (Article article : articleList) {
            transformers.add(articleDtoTransformer(article));
        }
        return transformers;
    }

    @Override
    public List<ArticleDtoTransformer> getArticlesByCategoryName(String categoryName) {
        CategoryDto categoryDto = null;

        for (CategoryDto category : constantCategoryList.getCategoryList()) {
            if (category.getName().equals(categoryName.toUpperCase())) {
                categoryDto = category;
                break;
            }
        }

        if (categoryDto == null) {
            return null;
        }

        List<Article> articleList = articleRepository.findAllByCategory(categoryDto.getName());
        List<ArticleDtoTransformer> transformers = new ArrayList<>();
        for (Article article : articleList) {
            transformers.add(articleDtoTransformer(article));
        }
        return transformers;
    }

    private AuthorDtoTransformer getAuthor() {
        Author author = authorRepository.findByFirstNameAndLastNameAndImageUrl("John", "Smith",
                "http://covers.feedbooks.net/book/7237.jpg");

        if (author != null){
            return authorDtoTransformer(author);
        } else {
            Author author1 = new Author();
            author1.setFirstName("John");
            author1.setLastName("Smith");
            author1.setFullname("John Smith");
            author1.setImageUrl("http://covers.feedbooks.net/book/7237.jpg");
            author1.setPublicId(stringHelper.getRandomString());
            authorRepository.save(author1);
            return authorDtoTransformer(author1);
        }
    }

    private AuthorDtoTransformer authorDtoTransformer(Author author) {
        AuthorDtoTransformer transformer = new AuthorDtoTransformer();
        transformer.setFullname(author.getFullname());
        transformer.setFirstName(author.getFirstName());
        transformer.setLastName(author.getLastName());
        transformer.setPublic_id(author.getPublicId());
        transformer.setImage_url(author.getImageUrl());
        return transformer;
    }

    private ArticleDetailDtoTransformer articleDetailDtoTransformer(final Article article) {
        ArticleDetailDtoTransformer transformer = new ArticleDetailDtoTransformer();
        transformer.setId(article.getId().toString());
        transformer.setTitle(article.getTitle());
        transformer.setImage_link(article.getImage_url());
        transformer.setCategory(getCategoryForArticle(article));

        transformer.setImported_date(DateTimeUtils.convertLongDateToStringDate(article.getImportedDate()));

        transformer.setTags(stringHelper.convertStringIntoTagsList(article.getEntities()));
        transformer.setThumbnail_url(article.getThumbnailUrl());
        Optional<Provider> provider = providerRepository.findById(article.getProvider_id());
        if (provider.isPresent()) {
            ProviderDtoTransformer providerDtoTransformer = new ProviderDtoTransformer();
            providerDtoTransformer.setLogo_url(provider.get().getLogoUrl());
            providerDtoTransformer.setName(provider.get().getName());
            providerDtoTransformer.setId(provider.get().getId().toString());
            transformer.setProvider(providerDtoTransformer);
        }
        transformer.setContent(article.getDescription());
        transformer.setAuthor(getAuthor());
        transformer.setRelated_articles(getRelatedArticles(article));
        transformer.setProvider(transformProvider(article));
        return transformer;
    }

    private List<ArticleDtoTransformer> getRelatedArticles(final Article article) {
        List<Article> articleList = articleRepository.findAllByCategoryContainingAndSectionContaining(article.getCategory(), article.getSection());
        if (articleList == null) { return null;}

        return transformerArticles(articleList);
    }

    private boolean oldContentThere(final PressifierRequest pressifierRequest, final Integer newArticleListSize, final String requestType) {

        if (pressifierRequest == null) {
            return false;
        }

        switch (requestType){
            case "all_articles" :
                return pressifierRequest.getNumOfAllArticles().equals(newArticleListSize);
            case "all_articles_lang":
                return pressifierRequest.getNumOfAllArticlesWithLang().equals(newArticleListSize);
            case "all_articles_lang_and_location":
                return pressifierRequest.getNumOfAllArticlesWithLangAndLocation().equals(newArticleListSize);
            case "all_articles_location":
                return pressifierRequest.getNumOfAllArticlesWithLocation().equals(newArticleListSize);
            default :
                return false;
        }
    }

    private PressifierRequest getOldRequest() {
        Iterable<PressifierRequest> iterable = pressifierRequestResitory.findAllByDeleted(true);
        pressifierRequestResitory.deleteAll(iterable);
        iterable = pressifierRequestResitory.findAllByDeleted(false);
        List<PressifierRequest> list = new ArrayList<>();
        iterable.forEach(list::add);
        for (PressifierRequest re : list) {
            if (!re.isDeleted()) {
                return  re;
            }
        }
        return null;
    }

    private String recordNewRequest(final List<Article> articleList) {

        final String version = stringHelper.getRandomString();
        PressifierRequest request = new PressifierRequest();

        request.setVersion(version);
        request.setDeleted(false);
        request.setNumOfAllArticles(articleList.size());
        request.setNumOfAllArticlesWithLang(articleList.size());
        request.setNumOfAllArticlesWithLangAndLocation(articleList.size());
        request.setNumOfAllArticlesWithLocation(articleList.size());
        pressifierRequestResitory.save(request);
        return version;
    }

    private CategoryDto getCategoryForArticle(final Article article) {
        List<CategoryDto> categoryList = constantCategoryList.getCategoryList();

        CategoryDto categoryDtoResponse = categoryList.get(RandomValueGeneratorHelper.generareRandomInt(categoryList.size()));
        for (CategoryDto categoryDto: categoryList) {
            if (categoryDto.getName().equals(article.getCategory().toUpperCase())) {
                categoryDtoResponse = categoryDto;
            }
        }
        return categoryDtoResponse;
    }

}
//Path: /var/folders/lj/ylymgc490qzbg55mrdq1hpyh0000gp/T/1584482380169-0/MySpecificName.json
//Path: /var/folders/lj/ylymgc490qzbg55mrdq1hpyh0000gp/T/1584483082381-0/MySpecificName.json

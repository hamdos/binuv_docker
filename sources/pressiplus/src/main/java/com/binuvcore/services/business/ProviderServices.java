package com.binuvcore.services.business;

import com.binuvcore.businessobjects.Provider;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Implementation class for {@link ProviderServices}
 *
 * @author doston
 * @since 24.12.2019
 */
@Component
@ComponentScan(lazyInit = true)
public interface ProviderServices {

    //~ Methods --------------------------------------------------------------------------------------------------------

    public boolean addProvider(Provider provider);
    public Provider getProviderById(int providerId);
    public List<Provider> getProviderByNameContaining(String providerName);
    public List<Provider> getAllProviders();
}

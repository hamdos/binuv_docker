package com.binuvcore.services.business;

import com.binuvcore.businessobjects.BlockedProvider;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@ComponentScan(lazyInit = true)
public interface BlockedProviderServices {

    public Optional<BlockedProvider> getBlockedProviderById(Integer id);

    public List<BlockedProvider> getAllBlockedProviderWithUserAndProvider();
    public List<BlockedProvider> getBlockedProvidersByUser(Integer userId);
    public List<BlockedProvider> getBlockedProvidersByProvider(Integer providerId);
    public List<BlockedProvider> getBlockedProvidersByProviderIdAndUserId(Integer userId, Integer providerId);

    public boolean addBlockedProviderWithUserIdAndProviderId(Integer userId, Integer providerId);

}

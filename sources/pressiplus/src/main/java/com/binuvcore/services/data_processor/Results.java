package com.binuvcore.services.data_processor;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Results {

    @Getter
    @Setter
    private String class_label;

    @Getter
    @Setter
    private String rating;
}

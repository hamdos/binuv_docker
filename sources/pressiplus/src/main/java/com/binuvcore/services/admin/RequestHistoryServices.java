package com.binuvcore.services.admin;

import com.binuvcore.businessobjects.webapp.analytics.RequestHistoryTransformer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan(lazyInit = true)
public interface RequestHistoryServices {

    void addRequestHistory(String ipAddress, String requestUrl);

    List<RequestHistoryTransformer> getRequestHistroyByIpAddress(String ipAddress);
    List<RequestHistoryTransformer> getRequestHistroyByCity(String city);
    List<RequestHistoryTransformer> getRequestHistroyByRequestUrl(String requestUrl);
    List<RequestHistoryTransformer> getAll();


}

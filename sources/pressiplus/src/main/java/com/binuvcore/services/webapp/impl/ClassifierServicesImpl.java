package com.binuvcore.services.webapp.impl;

import com.binuvcore.businessobjects.webapp.ArticleRaterDto;
import com.binuvcore.businessobjects.webapp.ClassifierDto;
import com.binuvcore.services.webapp.ClassifierServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.simple.JSONObject;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Service
public class ClassifierServicesImpl implements ClassifierServices {

    private RestTemplate restTemplate;

    private static final String BASE_URL_OF_DATA_PROCESSOR = "https://pressifier.herokuapp.com/";
    private static final String CLASSIFIER_URL = BASE_URL_OF_DATA_PROCESSOR + "api/articles/classify";

    @Override
    public ClassifierDto classifyRequest(ArticleRaterDto articleRaterDto) {
        restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("binuv", "92eb5ffee6ae2fec3ad71c777531578f");
        List<ArticleRaterDto> articleRaterDtoArrayList = new ArrayList<>();
        articleRaterDtoArrayList.add(articleRaterDto);
        HttpEntity<List<ArticleRaterDto>> entity = new HttpEntity<List<ArticleRaterDto>>(articleRaterDtoArrayList, headers);

        JSONObject jArray = restTemplate.exchange(CLASSIFIER_URL, HttpMethod.POST, entity,
                JSONObject.class).getBody();

        if (jArray != null && !jArray.toString().startsWith("[")) {
            String stringBuilder = "[" +
                    jArray.toString() +
                    "]";
            return parseJSON(stringBuilder).get(0);
        }
        return parseJSON(jArray.toString()).get(0);

    }

    @Override
    public List<ClassifierDto> classifyRequest(List<ArticleRaterDto> articleRaterDtoList) {
        restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("binuv", "92eb5ffee6ae2fec3ad71c777531578f");
//        List<ArticleRaterDto> articleRaterDtoArrayList = new ArrayList<>();
//        articleRaterDtoArrayList.add(articleRaterDto);
        HttpEntity<List<ArticleRaterDto>> entity = new HttpEntity<List<ArticleRaterDto>>(articleRaterDtoList, headers);

        JSONObject jArray = restTemplate.exchange(CLASSIFIER_URL, HttpMethod.POST, entity,
                JSONObject.class).getBody();

        if (jArray != null && !jArray.toString().startsWith("[")) {
            String stringBuilder = "[" +
                    jArray.toString() +
                    "]";
            return parseJSON(stringBuilder);
        }

        return parseJSON(jArray.toString());
    }

    private List<ClassifierDto> parseJSON(final String jsonString) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<ClassifierDto>>(){}.getType();
        List<ClassifierDto> contactList = gson.fromJson(jsonString, type);

        return contactList;
    }
}

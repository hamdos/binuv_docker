package com.binuvcore.services.admin.master;

import com.binuvcore.businessobjects.webapp.master.ReportTimelineDto;
import com.binuvcore.businessobjects.webapp.master.ReportTimelineTransformer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan(lazyInit = true)
public interface ReportTimelineServices {

    List<ReportTimelineTransformer> getReportTimelines();
    List<ReportTimelineDto> getReportTimelineDtosAsList();
}

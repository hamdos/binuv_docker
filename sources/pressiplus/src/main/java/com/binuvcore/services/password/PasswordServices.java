package com.binuvcore.services.password;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@ComponentScan(lazyInit = true)
public interface PasswordServices {

    String encryptPlaintext(String plaintext);
    boolean verify(String userPassPlaintext, String encryptedString);
    String saltPlaintext(String plaintext);
}

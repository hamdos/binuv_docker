

# There are 3 ways to get values from .properties file

## 1. Using *@value* annotation
it is shown in BinuvCategory class

## 2. Using *@ConfigurationProperties(prefix = "message.default")* and accessing keywords, then converting those keyword into objects

## 3. Using *Environment* class of Spring boot to access .properties file
This approach is the best approach coz we just need to pass a keyword and get value


### Reference:
1. https://stackoverflow.com/questions/30528255/how-to-access-a-value-defined-in-the-application-properties-file-in-spring-boot
2. https://howtodoinjava.com/spring-boot2/datasource-configuration/
3. https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html
4. https://www.javadevjournal.com/spring-boot/how-spring-boot-auto-configuration-works/
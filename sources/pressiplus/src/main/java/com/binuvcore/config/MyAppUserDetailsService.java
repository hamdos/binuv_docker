package com.binuvcore.config;

import com.binuvcore.repositories.UserRepository;
import com.binuvcore.services.business.UserServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class MyAppUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    private Logger logger = LoggerFactory.getLogger(MyAppUserDetailsService.class);

    // actually we check with email
    @Override
    public UserDetails loadUserByUsername(String email)
            throws UsernameNotFoundException {
        com.binuvcore.businessobjects.access.User activeUserInfo = userRepository.findUserByEmail(email);

        if (activeUserInfo == null) {
            throw new UsernameNotFoundException("User does not exist");
        }
//        logger.info("Email: " + email);
//        logger.info("User name: " + activeUserInfo.getFirstName());
        GrantedAuthority authority = new SimpleGrantedAuthority(activeUserInfo.getRole());
        UserDetails userDetails = (UserDetails)new User(activeUserInfo.getEmail(),
                activeUserInfo.getPassword(), Arrays.asList(authority));
        return userDetails;
    }
}


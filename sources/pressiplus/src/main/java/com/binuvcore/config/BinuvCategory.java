//package com.binuvcore.config;
//
//import lombok.Getter;
//import lombok.Setter;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//
//@Configuration
//@PropertySource("classpath:config-files/messages.properties")
//public class BinuvCategory {
//
//
//    @Getter
//    @Setter
//    @Value( "${binuv.category.technology}" )
//    private String binuv_category_tech;
//
//    @Getter
//    @Setter
//    @Value( "${binuv.category.importer}" )
//    private String binuv_category_importer;
//
//    @Getter
//    @Setter
//    @Value( "${system.default.timestamp}" )
//    private String system_default_timestamp;
//
//
//}

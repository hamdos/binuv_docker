package com.binuvcore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EnableJpaRepositories("com.binuvcore.repositories")
@EntityScan("com.binuvcore.businessobjects")
public class BinuvCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(BinuvCoreApplication.class, args);
	}
}

package com.binuvcore.controllers.admin;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.businessobjects.transformers.ESubscriptionTransformer;
import com.binuvcore.businessobjects.webapp.analytics.ESubscription;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.repositories.ESubscriptionRepository;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Controller
public class ESubscriptionControllerAdmin {

    @Autowired
    private UserServices userServices;

    @Autowired
    private ESubscriptionRepository eSubscriptionRepository;

    @GetMapping("/admin/history/subscription-list")
    public ModelAndView subscriptionList(Model model) {
        ModelAndView modelAndView = new ModelAndView();
        ParameterPasser parameterPasser = new ParameterPasser();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
        } else {
            parameterPasser.setLogged_in(false);
        }


        parameterPasser.setError(false);
        parameterPasser.setSuccess(false);
        parameterPasser.setPageName("admin");
        modelAndView.addObject("parameterPasser", parameterPasser);
        modelAndView.addObject("subscriptions", getAllSubsriptions());
        modelAndView.setViewName("admin/subscription-emails-list");
        return modelAndView;
    }

    private List<ESubscriptionTransformer> getAllSubsriptions() {
        List<ESubscription> subscriptions = new ArrayList<>();
        eSubscriptionRepository.findAll().forEach(subscriptions::add);
        subscriptions.sort(Comparator.comparing(ESubscription::getId));

        List<ESubscriptionTransformer> transformers = new ArrayList<>();
        for (ESubscription subscription :
                subscriptions) {
            final String date = DateTimeUtils.convertLongDateToStringDateShortFormat(subscription.getSubscribed_date());
            transformers.add(new ESubscriptionTransformer(subscription.getEmail(), subscription.isUnsubscribed(), date));
        }
        return transformers;
    }
}

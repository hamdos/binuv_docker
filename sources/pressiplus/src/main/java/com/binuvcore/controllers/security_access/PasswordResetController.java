package com.binuvcore.controllers.security_access;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.config.keys.ConfigurationKeyServices;
import com.binuvcore.config.keys.ConfigurationMsgServices;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.helper.StringHelper;
import com.binuvcore.helper.http.HttpAndUrlHelper;
import com.binuvcore.services.business.UserServices;
import com.binuvcore.services.webapp.ResetPasswordServices;
import com.binuvcore.services.webapp.TokenServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class PasswordResetController {

    @Autowired
    private ResetPasswordServices resetPasswordServices;

    @Autowired
    private TokenServices tokenServices;

    @Autowired
    private UserServices userServices;

    @Autowired
    private ConfigurationMsgServices configurationMsgServices;

    @Autowired
    private ConfigurationKeyServices configurationKeyServices;

    private StringHelper stringHelper = new StringHelper();

    @GetMapping("app/passwordreset")
    public ModelAndView checkToken(@RequestParam(value = "token", required = false) String token, Model model) {
        ModelAndView modelAndView = new ModelAndView();

        ParameterPasser parameterPasser = (ParameterPasser) model.asMap().get("parameterPasser");

        if (parameterPasser != null) {
            modelAndView.addObject("parameterPasser", parameterPasser);
            modelAndView.setViewName("security/passwordreset");
            return modelAndView;
        }

        parameterPasser = new ParameterPasser();

        if (stringHelper.isWhiteSpaceOrNull(token) || token.equals("null")){
            parameterPasser.setPasswordResetRequest(true);
            parameterPasser.setPasswordResetValidate(false);
            parameterPasser.setToken(token);
            modelAndView.addObject("parameterPasser", parameterPasser);
            modelAndView.setViewName("security/passwordreset");
            return modelAndView;
        } else {
            boolean validToken = tokenServices.validateToken(token);
            if (validToken) {
                User user = new User();
                modelAndView.addObject("user", user);
                parameterPasser.setPasswordResetRequest(false);
                parameterPasser.setPasswordResetValidate(true);
                parameterPasser.setToken(token);
                modelAndView.addObject("parameterPasser", parameterPasser);
                modelAndView.setViewName("security/passwordreset");
                return modelAndView;
            } else {
                parameterPasser.setPasswordResetRequest(true);
                parameterPasser.setPasswordResetValidate(false);
                parameterPasser.setMessage(configurationMsgServices.getConfigValue("token.service.invalid"));
                parameterPasser.setError(true);
                modelAndView.addObject("parameterPasser", parameterPasser);
                modelAndView.setViewName("security/passwordreset");
                return modelAndView;
            }
        }
    }

    @RequestMapping(value= {"app/passwordreset-request"}, method=RequestMethod.POST)
    public String resetNewPasswordRequest(@Valid User user,
                                          Model model, HttpServletRequest request, RedirectAttributes redirectAttrs) {

        final ParameterPasser parameterPasser = new ParameterPasser();
        parameterPasser.setPasswordResetValidate(false);
        parameterPasser.setPasswordResetRequest(true);
        parameterPasser.setSuccess(false);
        parameterPasser.setError(true);

        // Request password change
        if (!stringHelper.isWhiteSpaceOrNull(user.getEmail())) {
            User userExists = userServices.getUserByEmail(user.getEmail());
            if(userExists == null) {

                parameterPasser.setMessage(configurationMsgServices.getConfigValue("password.reset.msg.nouser") +
                        " Email: " + user.getEmail());
                redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
                return "redirect:/app/passwordreset";

            } else {
                final String url = HttpAndUrlHelper.baseUrl(request.getHeader("referer")) + "/app/passwordreset";

                if (configurationKeyServices.getConfigValue("email.configuration.enabled").equals("true")){
                    resetPasswordServices.requestPasswordReset(userExists.getEmail(), url);
                    parameterPasser.setMessage(configurationMsgServices.getConfigValue("password.reset.msg.send.success") +
                            " Email: " + user.getEmail());
                    parameterPasser.setSuccess(true);
                    parameterPasser.setError(false);
                    redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
                    return "redirect:/app/login";
                }
                return "redirect:/email-configuration-disabled";
            }
        } else {
            parameterPasser.setMessage(configurationMsgServices.getConfigValue("token.service.email.notprovided"));
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            return "redirect:/app/passwordreset";

        }
    }

    @RequestMapping(value= {"app/passwordreset-validate"}, method=RequestMethod.POST)
    public String resetNewPassword(@Valid User user, @RequestParam(value = "token", required = false) String token,
                                         HttpServletRequest request, RedirectAttributes redirectAttrs, Model model) {

        final ParameterPasser parameterPasser = new ParameterPasser();
        parameterPasser.setSuccess(false);
        parameterPasser.setError(false);
        parameterPasser.setPasswordResetRequest(true);
        parameterPasser.setPasswordResetValidate(false);


        if (!stringHelper.isWhiteSpaceOrNull(token) && !stringHelper.isWhiteSpaceOrNull(user.getPassword())) {

            if (tokenServices.validateToken(token)) {
                boolean resetState = resetPasswordServices.resetPassword(token, user.getPassword());

                if (resetState) {
                    parameterPasser.setMessage(configurationMsgServices.getConfigValue("password.reset.msg.reset.success"));
                    parameterPasser.setSuccess(true);
                    redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
                    return "redirect:/app/login";
                }
            }
        }

        parameterPasser.setMessage(configurationMsgServices.getConfigValue("token.service.invalid"));
        parameterPasser.setError(true);
        redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
        return "redirect:/app/passwordreset";
    }
}

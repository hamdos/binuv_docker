package com.binuvcore.controllers.webapp.ticket;

import com.binuvcore.businessobjects.transformers.ticket.TicketContentOutDto;
import com.binuvcore.businessobjects.transformers.ticket.TicketInDto;
import com.binuvcore.businessobjects.transformers.ticket.TicketOutDto;
import com.binuvcore.businessobjects.webapp.ticket.Ticket;
import com.binuvcore.businessobjects.webapp.ticket.TicketContent;
import com.binuvcore.businessobjects.access.User;
import com.binuvcore.config.keys.ConfigurationMsgServices;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.repositories.binuv.TicketContentRepository;
import com.binuvcore.repositories.binuv.TicketRepository;
import com.binuvcore.services.business.UserServices;
import com.binuvcore.services.admin.RequestHistoryServices;
import com.binuvcore.services.webapp.ticket.TicketServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class TicketController {

    @Autowired
    private RequestHistoryServices requestHistoryServices;

    @Autowired
    private UserServices userServices;

    @Autowired
    private TicketRepository ticketSupportRepository;

    @Autowired
    private TicketContentRepository ticketSupportContentRepository;

    @Autowired
    private ConfigurationMsgServices configurationMsgServices;

    @Autowired
    private TicketServices ticketServices;

    @GetMapping("/app/support/new")
    public ModelAndView publisher(HttpServletRequest request, Model model,
                                  @RequestParam(value = "ticketId", required = false) Integer ticketId) {
        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());

        ParameterPasser parameterPasser = (ParameterPasser) model.asMap().get("parameterPasser");
        TicketInDto ticketInDto = (TicketInDto) model.asMap().get("ticketInputObject");
        TicketOutDto ticketOutDto = new TicketOutDto();

        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        if (parameterPasser == null) {
            parameterPasser = new ParameterPasser();
        }

        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
            modelAndView.addObject("userName", user.getFirstName() + " " + user.getLastName());
        } else {
            parameterPasser.setLogged_in(false);
        }

        if (ticketInDto == null) {
            ticketInDto = new TicketInDto();
            ticketInDto.setSubject("New Support Message");
        }

        if (ticketId != null && !ticketId.equals(0)) {
            Optional<Ticket> savedTicket = ticketSupportRepository.findById(ticketId);
            if (savedTicket.isPresent()){

                modelAndView.addObject("savedTicketObject",  ticketServices.getTicketById(ticketId, user));
            } else {
                parameterPasser.setError(true);
                parameterPasser.setMessage(configurationMsgServices.getConfigValue("ticket.support.noticket"));
            }
            modelAndView.addObject("ticketId",  ticketId);
        } else  {

            // empty ticket
            modelAndView.addObject("ticketId",  0);
            List<TicketContentOutDto> contentTransformers = new ArrayList<>();
            ticketOutDto.setTicketContents(contentTransformers);

            modelAndView.addObject("savedTicketObject",  ticketOutDto);
            ticketId = 0;
        }

        parameterPasser.setPageName("support-new");
        modelAndView.addObject("parameterPasser", parameterPasser);
        modelAndView.addObject("ticketInputTaker", ticketInDto);
        modelAndView.addObject("formAction",  "/app/publish-support-ticket-post?ticketId=" + ticketId);
        modelAndView.setViewName("home/ticket/support-ticket");
        return modelAndView;
    }

    @RequestMapping(value= {"/app/publish-support-ticket-post"}, method= RequestMethod.POST)
    public String resetNewPassword(@Valid TicketInDto ticketInDto,
                                   @RequestParam(value = "ticketId", required = true) Integer ticketId,
                                   HttpServletRequest request, RedirectAttributes redirectAttrs, Model model) {

        final ParameterPasser parameterPasser = new ParameterPasser();
        parameterPasser.setError(false);


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        if (user == null) {
            parameterPasser.setMessage(configurationMsgServices.getConfigValue("ticket.support.saved.auth.error"));
            parameterPasser.setError(true);
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            redirectAttrs.addFlashAttribute("ticketInputObject", ticketInDto);
            return "redirect:support";
        }

        Ticket ticketSupport = null;

        if (ticketId.equals(0)) {
            ticketSupport = new Ticket();
            ticketSupport.setCreatedDate(DateTimeUtils.getCurrentDateInLong());
            ticketSupport.setClosed(false);
            ticketSupport.setDeleted(false);
            ticketSupport.setOwnerId(user.getId());
        } else {

            ticketSupport = ticketSupportRepository.findById(ticketId).get();
        }

        ticketSupport.setTitle(ticketInDto.getSubject());
        ticketSupport.setPublicTicket(true);

        ticketSupportRepository.save(ticketSupport);

        Ticket ticketSupportSaved = ticketSupportRepository.findByTitleAndCreatedDate(ticketSupport.getTitle(), ticketSupport.getCreatedDate());

        TicketContent content = new TicketContent();
        content.setCreatedDate(DateTimeUtils.getCurrentDateInLong());
        content.setMessage(ticketInDto.getMessage());
        content.setDeleted(false);
        content.setTicketId(ticketSupportSaved.getId());
        content.setUserId(user.getId());

        if (user.getId().equals(ticketSupportSaved.getOwnerId())) {
            content.setOwner(true);
        } else {
            content.setOwner(false);
        }

        ticketSupportContentRepository.save(content);

        parameterPasser.setMessage(configurationMsgServices.getConfigValue("ticket.support.saved.success"));
        parameterPasser.setSuccess(true);
        redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
        redirectAttrs.addFlashAttribute("ticketInputObject", ticketInDto);
        return "redirect:/app/support/new?ticketId="+ticketSupportSaved.getId();
    }
}

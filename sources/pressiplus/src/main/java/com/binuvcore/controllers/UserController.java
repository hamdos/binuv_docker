package com.binuvcore.controllers;

import com.binuvcore.services.business.UserServices;
import com.binuvcore.businessobjects.access.User;
import com.binuvcore.helper.http.HttpAndUrlHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserServices userServices;

    private HttpAndUrlHelper httpHelper = new HttpAndUrlHelper();

    @PostMapping(path = "/add", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Boolean> addPerson(@RequestBody User user){

        return userServices.addUser(user) ? new ResponseEntity<>(true, httpHelper.getDefaultHeader(),
                HttpStatus.OK) : new ResponseEntity<>(false, httpHelper.getDefaultHeader(),
                HttpStatus.NOT_IMPLEMENTED);

    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<User>> getAllPersons(){

        return new ResponseEntity<>(userServices.getAllUsers(), httpHelper.getDefaultHeader(), HttpStatus.OK);
    }

    @RequestMapping(path = "/get-user-by-email", method = RequestMethod.GET)
    public ResponseEntity<User> findByEmail(@RequestParam String email){
        return new ResponseEntity<>(userServices.getUserByEmail(email), HttpStatus.OK);
    }

    @GetMapping(path = "/get-deleted-users")
    public ResponseEntity<List<User>> getAllDeletedUsers(){
        return new ResponseEntity<>(userServices.getAllDeletedUsers(), HttpStatus.OK);
    }

    @GetMapping(path = "/get-undeleted-users")
    public ResponseEntity<List<User>> getAllUnDeletedUsers(){
        return new ResponseEntity<>(userServices.getAllUnDeletedUsers(), HttpStatus.OK);
    }

    @GetMapping(path = "/get-users-by-this-firstname")
    public ResponseEntity<List<User>> getAllUsersByThisFirstName(@RequestParam String firstName){
        return new ResponseEntity<>(userServices.getAllUsersByThisFirstName(firstName), HttpStatus.OK);
    }

    @GetMapping(path = "/get-user-by-id")
    public ResponseEntity<User> getUserById(@RequestParam Integer id){
        return new ResponseEntity<>(userServices.getUserById(id), HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete")
    public ResponseEntity<Boolean> deleteUserById(@RequestParam Integer id){
        return userServices.deleteUserById(id) ? new ResponseEntity<>(true, HttpStatus.OK) :
                new ResponseEntity<>(false, HttpStatus.NOT_IMPLEMENTED);
    }

    @PostMapping(path = "/login", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<User> loginUser(@RequestBody User user){

        User foundUser = userServices.loginUser(user);
        if (foundUser != null){
            return new ResponseEntity<>(foundUser, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}

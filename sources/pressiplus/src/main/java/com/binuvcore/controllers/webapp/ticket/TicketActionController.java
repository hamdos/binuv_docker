package com.binuvcore.controllers.webapp.ticket;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.businessobjects.webapp.ticket.Ticket;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.repositories.binuv.TicketRepository;
import com.binuvcore.repositories.binuv.TicketContentRepository;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Controller
public class TicketActionController {

    @Autowired
    private UserServices userServices;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private TicketContentRepository ticketContentRepository;

    @GetMapping(path = "/app/support/actions/delete/{ticketId}")
    public String deleteTicket(@PathVariable("ticketId") Integer ticketId, RedirectAttributes redirectAttrs) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        ParameterPasser parameterPasser = new ParameterPasser();
        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
        } else {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("You are not logged in!");
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            parameterPasser.setError(true);
            return "redirect:/app/login";
        }

        Optional<Ticket> optionalTicket = ticketRepository.findById(ticketId);
        if (!optionalTicket.isPresent()) {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("Invalid ticket cannot be deleted!");
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            parameterPasser.setError(true);
            return "redirect:/app/support/new";
        }

        Ticket ticket = optionalTicket.get();
        if (!ticket.getOwnerId().equals(user.getId()) && !user.getRole().equals("ROLE_ADMIN")) {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("You do not have rights to delete the ticket!");
            parameterPasser.setError(true);
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            return "redirect:/app/support/new?ticketId=" +ticket.getId();
        }

        ticketRepository.deleteById(ticket.getId());
        ticketContentRepository.deleteAll(ticketContentRepository.findAllByTicketId(ticketId));

        parameterPasser.setSuccess(true);
        parameterPasser.setMessage("Ticket has been deleted!");
        redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
        return "redirect:/start";
    }

    @GetMapping(path = "/app/support/actions/closed/{ticketId}")
    public String closeTicket(@PathVariable("ticketId") Integer ticketId, RedirectAttributes redirectAttrs) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        ParameterPasser parameterPasser = new ParameterPasser();
        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
        } else {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("You are not logged in!");
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            parameterPasser.setError(true);
            return "redirect:/app/login";
        }

        Optional<Ticket> optionalTicket = ticketRepository.findById(ticketId);
        if (!optionalTicket.isPresent()) {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("Invalid ticket cannot be closed!");
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            parameterPasser.setError(true);
            return "redirect:/app/support/new";
        }

        Ticket ticket = optionalTicket.get();
        if (!ticket.getOwnerId().equals(user.getId()) && !user.getRole().equals("ROLE_ADMIN")) {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("You do not have rights to close the ticket!");
            parameterPasser.setError(true);
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            return "redirect:/app/support/new?ticketId=" +ticket.getId();
        }

        ticket.setClosed(true);
        ticketRepository.save(ticket);

        parameterPasser.setSuccess(true);
        parameterPasser.setMessage("Ticket has been closed!");
        redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
        return "redirect:/start";
    }

    @GetMapping(path = "/app/support/actions/open/{ticketId}")
    public String openTicket(@PathVariable("ticketId") Integer ticketId, RedirectAttributes redirectAttrs) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        ParameterPasser parameterPasser = new ParameterPasser();
        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
        } else {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("You are not logged in!");
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            parameterPasser.setError(true);
            return "redirect:/app/login";
        }

        Optional<Ticket> optionalTicket = ticketRepository.findById(ticketId);
        if (!optionalTicket.isPresent()) {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("Invalid ticket cannot be reopened!");
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            parameterPasser.setError(true);
            return "redirect:/app/support/new";
        }

        Ticket ticket = optionalTicket.get();
        if (!ticket.getOwnerId().equals(user.getId()) && !user.getRole().equals("ROLE_ADMIN")) {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("You do not have rights to reopened the ticket!");
            parameterPasser.setError(true);
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            return "redirect:/app/support/new?ticketId=" +ticket.getId();
        }

        ticket.setClosed(false);
        ticketRepository.save(ticket);

        parameterPasser.setSuccess(true);
        parameterPasser.setMessage("Ticket has been reopened again!");
        redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
        return "redirect:/start";
    }

    @GetMapping(path = "/app/support/actions/private/{ticketId}")
    public String makePrivate(@PathVariable("ticketId") Integer ticketId, RedirectAttributes redirectAttrs) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        ParameterPasser parameterPasser = new ParameterPasser();
        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
        } else {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("You are not logged in!");
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            parameterPasser.setError(true);
            return "redirect:/app/login";
        }

        Optional<Ticket> optionalTicket = ticketRepository.findById(ticketId);
        if (!optionalTicket.isPresent()) {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("Invalid ticket cannot be private!");
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            parameterPasser.setError(true);
            return "redirect:/app/support/new";
        }

        Ticket ticket = optionalTicket.get();
        if (!ticket.getOwnerId().equals(user.getId()) && !user.getRole().equals("ROLE_ADMIN")) {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("You do not have rights to make the ticket private!");
            parameterPasser.setError(true);
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            return "redirect:/app/support/new?ticketId=" +ticket.getId();
        }

        ticket.setPublicTicket(false);
        ticketRepository.save(ticket);

        parameterPasser.setSuccess(true);
        parameterPasser.setMessage("Ticket has been made private!");
        redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
        return "redirect:/start";
    }

    @GetMapping(path = "/app/support/actions/public/{ticketId}")
    public String makePublic(@PathVariable("ticketId") Integer ticketId, RedirectAttributes redirectAttrs) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        ParameterPasser parameterPasser = new ParameterPasser();
        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
        } else {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("You are not logged in!");
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            parameterPasser.setError(true);
            return "redirect:/app/login";
        }

        Optional<Ticket> optionalTicket = ticketRepository.findById(ticketId);
        if (!optionalTicket.isPresent()) {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("Invalid ticket cannot be public!");
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            parameterPasser.setError(true);
            return "redirect:/app/support/new";
        }

        Ticket ticket = optionalTicket.get();
        if (!ticket.getOwnerId().equals(user.getId()) && !user.getRole().equals("ROLE_ADMIN")) {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("You do not have rights to make the ticket public!");
            parameterPasser.setError(true);
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            return "redirect:/app/support/new?ticketId=" +ticket.getId();
        }

        ticket.setPublicTicket(true);
        ticketRepository.save(ticket);

        parameterPasser.setSuccess(true);
        parameterPasser.setMessage("Ticket has been made public!");
        redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
        return "redirect:/start";
    }
}

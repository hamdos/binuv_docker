package com.binuvcore.controllers.admin;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.businessobjects.webapp.analytics.RequestHistoryTransformer;
import com.binuvcore.config.keys.ConfigurationKeyServices;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.helper.StringHelper;
import com.binuvcore.services.business.UserServices;
import com.binuvcore.services.admin.RequestHistoryServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class RequestHistoryController {

    @Autowired
    private RequestHistoryServices requestHistoryServices;

    @Autowired
    private ConfigurationKeyServices configurationKeyServices;

    @Autowired
    private UserServices userServices;

    private StringHelper stringHelper = new StringHelper();

    private Logger logger = LoggerFactory.getLogger(RequestHistoryController.class);

    @GetMapping("admin/history/requests")
    public ModelAndView requests(){
        ModelAndView modelAndView = new ModelAndView();
        ParameterPasser parameterPasser = new ParameterPasser();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
        } else {
            parameterPasser.setLogged_in(false);
        }

        parameterPasser.setError(false);
        parameterPasser.setSuccess(false);
        parameterPasser.setPageName("admin");
        modelAndView.addObject("parameterPasser", parameterPasser);
        modelAndView.addObject("requests", requestHistoryServices.getAll());
        modelAndView.setViewName("admin/request-history");
        return modelAndView;
    }

    @GetMapping(path = "/admin/history/all")
    ResponseEntity<List<RequestHistoryTransformer>> getAllRequest(@RequestHeader(name = "key", required = true) String key) {

        final String binuvKey = configurationKeyServices.getConfigValue("binuv.request.history.key");
        logger.info("Requested key: " + key);
        if (!stringHelper.isWhiteSpaceOrNull(key) && binuvKey.equals(key)) {
            logger.info("Keys are equal");
            return new ResponseEntity<>(requestHistoryServices.getAll(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(path = "/admin/history/ip")
    ResponseEntity<List<RequestHistoryTransformer>> getAllRequestsByIpAddress(@RequestHeader(name = "key", required = true) String key,
                                                                              @RequestParam(name = "ip") String ip) {

        final String binuvKey = configurationKeyServices.getConfigValue("binuv.request.history.key");
        if (!stringHelper.isWhiteSpaceOrNull(key) && binuvKey.equals(key)) {
            return new ResponseEntity<>(requestHistoryServices.getRequestHistroyByIpAddress(ip), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(path = "/admin/history/city")
    ResponseEntity<List<RequestHistoryTransformer>> getAllRequestsByCity(@RequestHeader(name = "key", required = true) String key,
                                                                              @RequestParam(name = "city") String city) {

        final String binuvKey = configurationKeyServices.getConfigValue("binuv.request.history.key");
        if (!stringHelper.isWhiteSpaceOrNull(key) && binuvKey.equals(key)) {
            return new ResponseEntity<>(requestHistoryServices.getRequestHistroyByCity(city), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(path = "/admin/history/url")
    ResponseEntity<List<RequestHistoryTransformer>> getAllRequestsByRequestUrl(@RequestHeader(name = "key", required = true) String key,
                                                                         @RequestParam(name = "url") String url) {

        final String binuvKey = configurationKeyServices.getConfigValue("binuv.request.history.key");
        if (!stringHelper.isWhiteSpaceOrNull(key) && binuvKey.equals(key)) {
            return new ResponseEntity<>(requestHistoryServices.getRequestHistroyByRequestUrl(url), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
    }

}

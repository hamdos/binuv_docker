package com.binuvcore.controllers.admin;

import com.binuvcore.businessobjects.Article;
import com.binuvcore.businessobjects.access.User;
import com.binuvcore.businessobjects.custom.CustomSharedDto;
import com.binuvcore.businessobjects.webapp.master.ReportTimelineDto;
import com.binuvcore.businessobjects.webapp.master.ReportTimelineTransformer;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.helper.StringHelper;
import com.binuvcore.helper.http.HttpAndUrlHelper;
import com.binuvcore.repositories.master.ReportTimelineRepository;
import com.binuvcore.services.business.ArticleServices;
import com.binuvcore.services.business.UserServices;
import com.binuvcore.services.admin.CustomSharedServices;
import com.binuvcore.services.webapp.TokenServices;
import com.binuvcore.services.admin.master.ReportTimelineServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ReportTimelineController {

    @Autowired
    private ArticleServices articleServices;

    @Autowired
    private ReportTimelineRepository reportTimelineRepository;

    @Autowired
    private ReportTimelineServices reportTimelineServices;

    @Autowired
    private CustomSharedServices customSharedServices;

    @Autowired
    private TokenServices tokenServices;

    @Autowired
    private UserServices userServices;

    private StringHelper stringHelper = new StringHelper();

    List<ReportTimelineDto> reportTimelineDtos = new ArrayList<>();

    @GetMapping("/app/master-thesis/report")
    public ModelAndView reportTimePage(@RequestParam(value = "token", required = false) String token, HttpServletRequest request){

        ModelAndView modelAndView = new ModelAndView();
        ParameterPasser parameterPasser = new ParameterPasser();
        parameterPasser.setPageName("admin");

        if (!stringHelper.isWhiteSpaceOrNull(token)) {
            parameterPasser.setLogged_in(false);

            if (tokenServices.validateToken(token)) {
                modelAndView.addObject("reportTimelineList", reportTimelineServices.getReportTimelines());
                parameterPasser.setMessage("Welcome to Doston Hamrakulov's Weekly Report on his Master Thesis Research");
                parameterPasser.setSuccess(true);
            } else {
                parameterPasser.setMessage("Sorry. Invalid token, please send a request and use valid token.");
                parameterPasser.setError(true);
                modelAndView.addObject("reportTimelineList", null);
            }
            modelAndView.addObject("parameterPasser", parameterPasser);
            modelAndView.setViewName("master/report-timeline");
            return modelAndView;
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        if (auth.isAuthenticated() && user == null) {
            parameterPasser.setMessage("Sorry. The web page is not accessible without a valid token or user account.");
            parameterPasser.setError(true);
            modelAndView.addObject("reportTimelineList", null);
            modelAndView.addObject("parameterPasser", parameterPasser);
            modelAndView.setViewName("master/report-timeline");
            return modelAndView;
        }

        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
            modelAndView.addObject("parameterPasser", parameterPasser);
            modelAndView.addObject("reportTimelineList", reportTimelineServices.getReportTimelines());
            modelAndView.setViewName("master/report-timeline");
            return modelAndView;
        } else {
            parameterPasser.setLogged_in(false);
            parameterPasser.setMessage("Sorry. The web page is not accessible without a valid token or user account.");
            parameterPasser.setError(true);
            modelAndView.addObject("reportTimelineList", null);
            modelAndView.addObject("parameterPasser", parameterPasser);
            modelAndView.setViewName("master/report-timeline");
            return modelAndView;
        }
    }

    @GetMapping("app/master-thesis/report-publisher")
    public ModelAndView postReportTimelinePage(Model model) {
        ModelAndView modelAndView = new ModelAndView();

        ParameterPasser parameterPasser = (ParameterPasser) model.asMap().get("parameterPasser");
        ReportTimelineDto reportTimelineDto = (ReportTimelineDto) model.asMap().get("reportTimelineDto");
        CustomSharedDto customSharedDto = (CustomSharedDto) model.asMap().get("customSharedDto");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        if (parameterPasser == null) {
            parameterPasser = new ParameterPasser();
        }

        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());

            if (customSharedDto == null) {
                customSharedDto = customSharedServices.getCustomSharedByUserIdAndProgressReport(user.getId(), true);
            }

        } else {
            parameterPasser.setLogged_in(false);
        }

        if (customSharedDto == null) {
            customSharedDto = new CustomSharedDto();
            customSharedDto.setShared(false);
            customSharedDto.setUserId(user.getId());
            customSharedDto.setProgressReport(true);
        }

        modelAndView.addObject("customSharedDto", customSharedDto);
        modelAndView.addObject("parameterPasser", parameterPasser);

        if (reportTimelineDto != null) {
            modelAndView.addObject("reportTimelineDto", reportTimelineDto);
        } else {
            modelAndView.addObject("reportTimelineDto", new ReportTimelineDto());
        }

        modelAndView.addObject("reportUpdateObject", new ReportTimelineTransformer());
        modelAndView.addObject("reports", reportTimelineServices.getReportTimelineDtosAsList());
        modelAndView.setViewName("master/report-timeline-edit-page");
        return modelAndView;
    }

    @RequestMapping(value= {"app/master-thesis/report-publisher"}, method=RequestMethod.POST)
    public String publishReport(@Valid ReportTimelineDto reportTimelineDto, HttpServletRequest request, RedirectAttributes redirectAttrs){

        final ParameterPasser parameterPasser = new ParameterPasser();
        parameterPasser.setSuccess(false);
        parameterPasser.setError(false);
        parameterPasser.setPasswordResetRequest(true);
        parameterPasser.setPasswordResetValidate(false);

        reportTimelineDtos.add(reportTimelineDto);

        if (validReport(reportTimelineDto)) {

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            User user = userServices.getUserByEmail(auth.getName());

            ReportTimelineDto reportTimelineDto1 = reportTimelineRepository.findByWeekAndUserId(reportTimelineDto.getWeek(), user.getId());
            // check if it is for update
            if (reportTimelineDto1 != null && reportTimelineDto1.getDay() > 0) {
                reportTimelineDto1.setUpdatedDate(DateTimeUtils.getCurrentDateInLong());
                reportTimelineDto1.setImplemented(reportTimelineDto.getImplemented());
                reportTimelineDto1.setPlannedToDo(reportTimelineDto.getPlannedToDo());
                reportTimelineDto1.setMonth(reportTimelineDto.getMonth());
                reportTimelineDto1.setDay(reportTimelineDto.getDay());
                reportTimelineRepository.save(reportTimelineDto1);
                parameterPasser.setMessage("Your report is updated and please check here: ");
            } else {
                reportTimelineDto.setUserId(user.getId());
                reportTimelineDto.setPublishedDate(DateTimeUtils.getCurrentDateInLong());
                reportTimelineDto.setUpdatedDate(DateTimeUtils.getCurrentDateInLong());
                reportTimelineRepository.save(reportTimelineDto);
                parameterPasser.setMessage("Your report is saved and please check here: ");
            }

            parameterPasser.setSuccess(true);
            redirectAttrs.addFlashAttribute("reportTimelineDto", reportTimelineDto);
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            return "redirect:report-publisher";
        }

        parameterPasser.setError(true);
        parameterPasser.setMessage("Your changes cannot be saved: ");
        redirectAttrs.addFlashAttribute("reportTimelineDto", reportTimelineDto);
        redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
        return "redirect:report-publisher";
    }

    @RequestMapping(value= {"app/master-thesis/report-publisher-update"}, method=RequestMethod.POST)
    public String updateReport(@Valid ReportTimelineTransformer reportTimelineTransformer, RedirectAttributes redirectAttrs){

        final ParameterPasser parameterPasser = new ParameterPasser();
        parameterPasser.setSuccess(false);
        parameterPasser.setError(false);
        parameterPasser.setPasswordResetRequest(true);
        parameterPasser.setPasswordResetValidate(false);

        ReportTimelineDto reportTimelineDto1 = reportTimelineRepository.findById(reportTimelineTransformer.getDay()).get();
        if (reportTimelineDto1.getDay() > 0) {
            parameterPasser.setMessage("You can update the report");
            parameterPasser.setSuccess(true);
            redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            redirectAttrs.addFlashAttribute("reportTimelineDto", reportTimelineDto1);
            return "redirect:report-publisher";
        }

        parameterPasser.setError(true);
        parameterPasser.setMessage("Your selection cannot be chosen");
        redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
        return "redirect:report-publisher";
    }

    @RequestMapping(value= {"app/master-thesis/report-publisher-share"}, method=RequestMethod.POST)
    public String shareReport(@Valid CustomSharedDto customSharedDtoInput, RedirectAttributes redirectAttrs, HttpServletRequest request) {

        final ParameterPasser parameterPasser = new ParameterPasser();
        parameterPasser.setSuccess(false);
        parameterPasser.setError(false);
        parameterPasser.setPasswordResetRequest(true);
        parameterPasser.setPasswordResetValidate(false);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());


        if (user != null) {
            CustomSharedDto customSharedDto = customSharedServices.getCustomSharedByUserIdAndProgressReport(user.getId(), true);


            parameterPasser.setSuccess(true);
            if (customSharedDto != null && customSharedDtoInput.isShared() != customSharedDto.isShared()) {

                customSharedDto.setShared(customSharedDtoInput.isShared());
                customSharedServices.saveCustomSharedDtoForReport(customSharedDto);
                redirectAttrs.addFlashAttribute("customSharedDto", customSharedDto);
                String message = "You have updated sharing option for all reports, please find link below:<br> " + makePreviewUrl(request.getHeader("referer"), customSharedDto.getToken());
                parameterPasser.setMessage(message);
                redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            } else if (customSharedDto != null && customSharedDtoInput.isShared() == customSharedDto.isShared()) {
                String message = "You have already enabled sharing option for all reports, please preview: <br>" + makePreviewUrl(request.getHeader("referer"), customSharedDto.getToken());
                parameterPasser.setMessage(message);
                redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
            } else {
                customSharedDto = new CustomSharedDto();
                customSharedDto.setUserId(user.getId());
                customSharedDto.setDeleted(false);
                customSharedDto.setToken(tokenServices.generateNewToken(user.getEmail(), false));
                customSharedDto.setSharedDate(DateTimeUtils.getCurrentDateInLong());
                customSharedDto.setProgressReport(true);
                customSharedDto.setShared(customSharedDtoInput.isShared());
                customSharedServices.saveCustomSharedDtoForReport(customSharedDto);
                String message = "You have disabled sharing option for all reports, please check it does not work:<br> " + makePreviewUrl(request.getHeader("referer"), customSharedDto.getToken());
                parameterPasser.setMessage(message);

                redirectAttrs.addFlashAttribute("customSharedDto", customSharedDto);
            }
        }

        return "redirect:report-publisher";
    }

    @GetMapping("/app/master-thesis/article-details")
    public ModelAndView getAllUserArticles() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        ParameterPasser parameterPasser = new ParameterPasser();

        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());

        } else {
            parameterPasser.setLogged_in(false);
        }

        ModelAndView mav = new ModelAndView();
        List<Article> articleList = articleServices.getAllArticle();
        mav.addObject("userArticles", articleList);
        mav.addObject("parameterPasser", parameterPasser);
        mav.setViewName("custom/articles");
        return mav;
    }

    private String makePreviewUrl(final String referer, final String token) {

        return HttpAndUrlHelper.baseUrl(referer) + "/app/master-thesis/report"+ "?token=" + token;
    }

    private boolean validReport(ReportTimelineDto reportTimelineDto) {
        if (reportTimelineDto == null) {
            return false;
        }

        if (stringHelper.isWhiteSpaceOrNull(reportTimelineDto.getImplemented())
                || stringHelper.isWhiteSpaceOrNull(reportTimelineDto.getPlannedToDo())) {
            return false;
        }

        if (stringHelper.isWhiteSpaceOrNull(reportTimelineDto.getMonth()) || stringHelper.isWhiteSpaceOrNull(reportTimelineDto.getWeek())) {
            return false;
        }
        return true;
    }
}

package com.binuvcore.controllers;

import com.binuvcore.businessobjects.Provider;

import com.binuvcore.helper.http.HttpAndUrlHelper;

import com.binuvcore.services.business.ProviderServices;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * Controller class for {@link Provider}
 *
 * @author doston
 * @since 24.12.2019
 */
@RequestMapping("/provider")
@RestController
public class ProviderController {

    //~ Instance fields ------------------------------------------------------------------------------------------------

    /**
     * Instance
     */
    @Autowired
    ProviderServices providerServices;

    /**
     * Instance
     */
    private HttpAndUrlHelper httpHelper = new HttpAndUrlHelper();

    //~ Methods --------------------------------------------------------------------------------------------------------

    /**
     * Post method to add a provider
     *
     * @param  providerJpa TODO DOCUMENT ME!
     *
     * @return response for Front-end
     */
    @PostMapping(
                 path = "/add", consumes = {MediaType.APPLICATION_JSON_VALUE}
                )
    public ResponseEntity<Boolean> addProvider(@RequestBody Provider providerJpa) {
        return providerServices.addProvider(providerJpa) ? new ResponseEntity<>(true, httpHelper.getDefaultHeader(),
                HttpStatus.OK) : new ResponseEntity<>(false, httpHelper.getDefaultHeader(),
                HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * Get method to get all providers with theirs categories or without categories if they dont have
     *
     * @return response for Front-end
     */
    @RequestMapping(path = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<Provider>> getAllProviders() {
        return new ResponseEntity<>(providerServices.getAllProviders(), HttpStatus.OK);
    }

    /**
     * Get method to get a provider by id
     *
     * @return response for Front-end
     */
    @RequestMapping(path = "/get-provider-by-id", method = RequestMethod.GET)
    public ResponseEntity<Provider> getProviderById(@RequestParam Integer id) {
        Provider provider = providerServices.getProviderById(id);
        if (provider != null){
            return new ResponseEntity<>(provider, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    /**
     * Get method to get all provider whose name contains this String
     *
     * @return response for Front-end
     */
    @RequestMapping(path = "/get-provider-by-name", method = RequestMethod.GET)
    public ResponseEntity<List<Provider>> getProviderByNameContaining(@RequestParam String name) {
        List<Provider> providerList = providerServices.getProviderByNameContaining(name);
        if(providerList != null && !providerList.isEmpty()){
            return new ResponseEntity<>(providerList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

}

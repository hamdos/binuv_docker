package com.binuvcore.controllers.security_access;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.repositories.UserRepository;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class UserProfileController {

    @Autowired
    private UserServices userServices;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @GetMapping("/app/user-profile")
    public ModelAndView userPage(Model model) {


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());
        ParameterPasser parameterPasser = (ParameterPasser) model.asMap().get("parameterPasser");

        if (parameterPasser == null) {
            parameterPasser = new ParameterPasser();
        }

        ModelAndView mav = new ModelAndView();
        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());

            mav.addObject("user", user);

        } else {
            parameterPasser.setLogged_in(false);
            mav.addObject("user", new User());
        }

        parameterPasser.setPageName("user-profile");
        mav.addObject("parameterPasser", parameterPasser);
        mav.setViewName("home/user-page");
        return mav;
    }

    @RequestMapping(value = {"/app/user-page-update"}, method = RequestMethod.POST)
    public String updateUser(@Valid User user, RedirectAttributes redirectAttrs) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User authenticatedUser = userServices.getUserByEmail(auth.getName());

        ParameterPasser parameterPasser = new ParameterPasser();

        if (authenticatedUser != null) {
            authenticatedUser.setFirstName(user.getFirstName());
            authenticatedUser.setLastName(user.getLastName());
            if (user.getPassword() != null || user.equals("")) {
                authenticatedUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            }
            userRepository.save(authenticatedUser);
            parameterPasser.setSuccess(true);
            parameterPasser.setMessage("Your profile is updated!");

        } else {
            parameterPasser.setMessage("Your profile is not updated!");
            parameterPasser.setError(true);
        }

        redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
        return "redirect:/app/user-profile";
    }
}

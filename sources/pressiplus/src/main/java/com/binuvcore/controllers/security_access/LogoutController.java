package com.binuvcore.controllers.security_access;

import com.binuvcore.helper.ParameterPasser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;

@Controller
public class LogoutController {

    @GetMapping("/app/logout")
    public ModelAndView logout() throws ServletException {

        ParameterPasser parameterPasser = new ParameterPasser();
        parameterPasser.setSuccess(true);
        parameterPasser.setMessage("You have successfully logged out!");
        ModelAndView mav = new ModelAndView();
        mav.addObject("parameterPasser", parameterPasser);
        mav.setViewName("redirect:signup");
        return mav;
    }
}

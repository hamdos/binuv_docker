package com.binuvcore.controllers.security_access;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.config.keys.ConfigurationKeyServices;
import com.binuvcore.config.keys.ConfigurationMsgServices;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.services.business.UserServices;
import com.binuvcore.services.webapp.MailServiceBinuv;
import com.binuvcore.services.admin.RequestHistoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class RegistrationController {

    @Autowired
    private UserServices userServices;

    @Autowired
    private MailServiceBinuv mailServiceBinuv;

    @Autowired
    private ConfigurationMsgServices configurationMsgServices;

    @Autowired
    private RequestHistoryServices requestHistoryServices;

    @Autowired
    private ConfigurationKeyServices configurationKeyServices;

    @RequestMapping(value = {"/app/signup"}, method = RequestMethod.GET)
    public ModelAndView signup(RedirectAttributes redirectAttrs, HttpServletRequest request, Model model) {

        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());

        ParameterPasser parameterPasser = (ParameterPasser) model.asMap().get("parameterPasser");

        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        if (auth.isAuthenticated() && !request.getSession(false).isNew() && user != null) {
            redirectAttrs.addFlashAttribute("loggedInMessage", "You have already logged in.");
            modelAndView.setViewName("redirect:home");
            return modelAndView;
        }

        if (parameterPasser == null) {
            parameterPasser = new ParameterPasser();
            parameterPasser.setSuccess(false);
            parameterPasser.setError(false);
            parameterPasser.setLogged_in(false);
        }

        modelAndView.addObject("parameterPasser", parameterPasser);
        modelAndView.addObject("user", new User());
        modelAndView.setViewName("security/signup");
        return modelAndView;
    }

    @RequestMapping(value = {"/app/signup"}, method = RequestMethod.POST)
    public String createUser(@Valid User user, RedirectAttributes redirectAttrs) {

        ParameterPasser parameterPasser = new ParameterPasser();
        parameterPasser.setSuccess(false);
        parameterPasser.setError(false);
        parameterPasser.setLogged_in(false);


        User userExists = userServices.getUserByEmail(user.getEmail());

        if (userExists != null) {
            parameterPasser.setError(true);
            parameterPasser.setMessage(configurationMsgServices.getConfigValue("registration.error.existing-user-for-email"));
        } else {
            userServices.addUser(user);
            if (configurationKeyServices.getConfigValue("email.configuration.enabled").equals("true")){
                sendEmail(user.getEmail());
            }

            parameterPasser.setSuccess(true);
            parameterPasser.setMessage(configurationMsgServices.getConfigValue("registration.success"));
        }

        redirectAttrs.addFlashAttribute("parameterPasser", parameterPasser);
        return "redirect:signup";
    }

    private void sendEmail(final String email) {
        String subjectEmail = configurationMsgServices.getConfigValue("registration.success.email.send.subject");
        String bodyEmail = configurationMsgServices.getConfigValue("registration.success.email.send.body");
        mailServiceBinuv.sendEmail(email, subjectEmail, bodyEmail);

    }
}

package com.binuvcore.controllers.webapp;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.businessobjects.webapp.ContactForm;
import com.binuvcore.businessobjects.webapp.ContactMail;
import com.binuvcore.businessobjects.webapp.analytics.ESubscription;
import com.binuvcore.config.keys.ConfigurationMsgServices;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.repositories.ContactFormRepository;
import com.binuvcore.services.business.UserServices;
import com.binuvcore.services.webapp.MailServiceBinuv;
import com.binuvcore.services.admin.RequestHistoryServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private ContactFormRepository contactFormRepository;

    @Autowired
    private MailServiceBinuv mailServiceBinuv;

    @Autowired
    private RequestHistoryServices requestHistoryServices;

    @Autowired
    private ConfigurationMsgServices configurationMsgServices;

    @Autowired
    private UserServices userServices;

    private Logger logger = LoggerFactory.getLogger(HomeController.class);

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = {"", "index.html", "/index", "/contact"})
    public ModelAndView index(@RequestParam(value="name", required=false, defaultValue="World") String name, HttpServletRequest request,
                              RedirectAttributes redirectAttrs) {
        ModelAndView model = new ModelAndView();
        model.addObject("name", name);
        model.addObject("contactMail", new ContactMail());
        model.addObject("subscription", new ESubscription());
//        resendFailedEmails();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());
        ParameterPasser parameterPasser = new ParameterPasser();
        if (auth.isAuthenticated() && !request.getSession(false).isNew() && user != null) {

            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
        } else {
            parameterPasser.setLogged_in(false);
        }

        model.addObject("parameterPasser", parameterPasser);
        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());
        model.setViewName("index");
        return model;
    }


    @GetMapping(path = {"/yandex_49648b3cd29c3a26", "/yandex_49648b3cd29c3a26.html"})
    public String yandexEmailConfigFile(Model model, HttpServletRequest request) {

        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());
        return "yandex_49648b3cd29c3a26";
    }


    private void resendFailedEmails() {
        List<ContactForm> contactFormList = contactFormRepository.findAllByDelivered(false);
        if (contactFormList != null && contactFormList.size() > 0) {
            for (ContactForm contactForm: contactFormList) {
                boolean re = mailServiceBinuv.sendEmailContactForm(contactForm);
                if (re) {
                    contactForm.setDelivered(true);
                    contactFormRepository.save(contactForm);
                }
            }
        }
    }
}

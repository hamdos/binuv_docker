package com.binuvcore.controllers.security_access;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AccessDeniedController {

    @Autowired
    private UserServices userServices;

    @GetMapping("/accessDenied")
    public ModelAndView accessDenied(Model model) {
        ModelAndView modelAndView = new ModelAndView();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        ParameterPasser parameterPasser = (ParameterPasser) model.asMap().get("parameterPasser");
        if (parameterPasser == null) {
            parameterPasser = new ParameterPasser();
        }

        if (user != null) {
            parameterPasser.setLogged_in(true);
            modelAndView.addObject("user", user);
        } else {
            parameterPasser.setLogged_in(false);
        }

        parameterPasser.setSuccess(false);
        parameterPasser.setError(false);
        parameterPasser.setPageName("accessDenied");

        modelAndView.addObject("parameterPasser", parameterPasser);
        modelAndView.setViewName("error/accessDenied_403");
        return modelAndView;
    }
}

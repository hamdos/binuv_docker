package com.binuvcore.controllers.webapp;

import com.binuvcore.businessobjects.Article;
import com.binuvcore.businessobjects.webapp.*;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.helper.StringHelper;
import com.binuvcore.repositories.ArticlePublisherRepository;
import com.binuvcore.repositories.ContentRepository;
import com.binuvcore.services.business.ArticleServices;
import com.binuvcore.services.admin.RequestHistoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping(path = "/publish")
public class ArticlePublisherController {

    @Autowired
    private ArticlePublisherRepository articlePublisherRepository;

    @Autowired
    private ContentRepository contentRepository;

    @Autowired
    private ArticleServices articleServices;

    @Autowired
    private RequestHistoryServices requestHistoryServices;

    private StringHelper stringHelper = new StringHelper();

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping(path = "/rate", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ArticlePublisherResultDto> publishArticle(@RequestBody ArticlePublisherDto articlePublisherDto,
                                                                    HttpServletRequest request){

        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());

        ArticlePublisher articlePublisher = null;
        if (!stringHelper.isWhiteSpaceOrNull(articlePublisherDto.getArticle_unique_id())) {
            articlePublisher = articlePublisherRepository.findArticlePublisherByArticleUniqueId(articlePublisherDto.getArticle_unique_id());
        }

        if (!stringHelper.isWhiteSpaceOrNull(articlePublisherDto.getArticle_unique_id()) && articlePublisher == null) {
            articlePublisher = createNewPublishedArticle(articlePublisherDto);

            return new ResponseEntity<>(generateRandomResult(articlePublisher.getArticleUniqueId()), HttpStatus.OK);
        } else if (stringHelper.isWhiteSpaceOrNull(articlePublisherDto.getArticle_unique_id())) {
            articlePublisher = createNewPublishedArticle(articlePublisherDto);

            return new ResponseEntity<>(generateRandomResult(articlePublisher.getArticleUniqueId()), HttpStatus.OK);
        }

        storeContent(articlePublisherDto);
        updatePublishedArticle(articlePublisher, articlePublisherDto);
        // TODO - send request to

        return new ResponseEntity<>(generateRandomResult(articlePublisher.getArticleUniqueId()), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "/article")
    public ResponseEntity<ArticlePublisherDto> getArticleByUniqueId(@RequestParam String article_unique_id,
                                                                    HttpServletRequest request){

        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());

        final ArticlePublisher articlePublisher = articlePublisherRepository.findArticlePublisherByArticleUniqueId(article_unique_id);
        List<ContentDto> contentDtoList = contentRepository.findAllByArticleUniqueId(article_unique_id);

        if (articlePublisher != null && contentDtoList == null){
            return new ResponseEntity<>(createNewObject(articlePublisher, new ArrayList<>()), HttpStatus.OK);
        } else if (articlePublisher != null) {
            return new ResponseEntity<>(createNewObject(articlePublisher, contentDtoList), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping(value = "/related-title", method = RequestMethod.GET)
    public ResponseEntity getRelatedArticlesByTitle(@RequestParam String title,
                                                    HttpServletRequest request) {

        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());

        List<Article> articleList = articleServices.getAllByTitleContains(title);
        if (articleList != null) {
            return new ResponseEntity<>(articleList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    private void storeContent(final ArticlePublisherDto articlePublisherDto){
        ContentDto contentDto = new ContentDto();
        contentDto.setTitle(articlePublisherDto.getTitle());
        contentDto.setContent(articlePublisherDto.getContent());
        contentDto.setArticleUniqueId(articlePublisherDto.getArticle_unique_id());
        contentDto.setCreatedDate(DateTimeUtils.getCurrentDateInLong());
        contentRepository.save(contentDto);
    }

    private void updatePublishedArticle(final ArticlePublisher articlePublisher,
                                        final ArticlePublisherDto articlePublisherDto) {
        articlePublisher.setTitle(articlePublisherDto.getTitle());
        articlePublisher.setContent(articlePublisherDto.getContent());
        if (!stringHelper.isWhiteSpaceOrNull(articlePublisher.getTags())
                && stringHelper.isWhiteSpaceOrNull(articlePublisherDto.getTags())) {
            articlePublisher.setTags(stringHelper.getUniqueTags(articlePublisher.getTags(),articlePublisherDto.getTags()));
        }
        articlePublisherRepository.save(articlePublisher);
    }

    private ArticlePublisher createNewPublishedArticle(final ArticlePublisherDto articlePublisherDto){
        ArticlePublisher articlePublisher = new ArticlePublisher();
        articlePublisher.setArticleUniqueId(createUniqueId());
        articlePublisher.setContent(articlePublisherDto.getContent());
        articlePublisher.setTitle(articlePublisherDto.getTitle());
        articlePublisher.setCreatedDate(DateTimeUtils.getCurrentDateInLong());
        articlePublisher.setLastUpdatedDate(DateTimeUtils.getCurrentDateInLong());
        articlePublisher.setTags(articlePublisherDto.getTags());
        articlePublisherRepository.save(articlePublisher);
        return articlePublisherRepository.findArticlePublisherByArticleUniqueId(articlePublisher.getArticleUniqueId());
    }

    private String createUniqueId() {
        boolean not_generated_new_unique_id = true;
        String unique_id = "";
        while (not_generated_new_unique_id) {
            unique_id = new StringHelper().getRandomString();
            if (articlePublisherRepository.findArticlePublisherByArticleUniqueId(unique_id) == null){
                not_generated_new_unique_id = false;
            }
        }
        return unique_id;
    }

    private ArticlePublisherDto createNewObject(ArticlePublisher articlePublisher, List<ContentDto> contentDtoList) {
        final ArticlePublisherDto articlePublisherDto = new ArticlePublisherDto(articlePublisher.getTitle(),
                articlePublisher.getArticleUniqueId(),
                articlePublisher.getContent(),
                articlePublisher.getTags(), contentDtoList);
        return articlePublisherDto;
    }

    private void sendToDataProcessor(final ArticleRaterDto articleRaterDto) {

    }

    private ArticlePublisherResultDto generateRandomResult(String unique_id){
        ArticlePublisherResultDto articlePublisherResultDto = new ArticlePublisherResultDto(
                unique_id, new StringHelper().generateRandomClass(), new StringHelper().generateRandomRating(),
                generateGoodWord(), generateBadWord());
        return articlePublisherResultDto;
    }



    private List<String> generateGoodWord(){
        final String[] goodWord = {
                "Accomplish", "Believe", "Action", "Challenge", "Commitment", "Confidence",
                "Courage", "Determination", "Focus", "Fulfillment", "Gratitude", "Imagination", "Inspiration",
                "Kindness", "Joy", "Nurture", "Money", "Free Food", "Cheap products", "Outstanding", "Lovely",
                "Patience", "Prioritize", "Smile", "Succeed"
        };
        final List<String> returnL = new ArrayList<>();
        int randomReturner = new Random().nextInt((11-5) + 1) + 5;
        for (int i = 0; i < randomReturner; i++) {
            returnL.add(goodWord[new Random().nextInt(goodWord.length)]);
        }
        return returnL;
    }

    private List<String> generateBadWord(){
        final String[] badWords = { "Abandoned", "Defeated", "Demoralized", "Doomed", "Hopeless", "Helpless",
                "Injured", "Oppressed", "Depression", "Sadness", "self-conscious", "suffering", "unhappy", "weak",
                "wretched", "lousy", "lonely", "freaking"};

        final List<String> returnL = new ArrayList<>();
        int randomReturner = new Random().nextInt((11-5) + 1) + 5;
        for (int i = 0; i < randomReturner; i++) {
            returnL.add(badWords[new Random().nextInt(badWords.length)]);
        }
        return returnL;
    }
}

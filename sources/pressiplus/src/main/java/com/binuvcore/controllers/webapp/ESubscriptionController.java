package com.binuvcore.controllers.webapp;

import com.binuvcore.businessobjects.webapp.analytics.ESubscription;
import com.binuvcore.services.webapp.ESubscriptionServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

@RestController
@RequestMapping(path = "/public")
public class ESubscriptionController {

    @Autowired
    private ESubscriptionServices eSubscriptionServices;

    private Logger logger = LoggerFactory.getLogger(ESubscriptionController.class);

    @GetMapping(path = "/email-subscription/all")
    public ResponseEntity<Iterable<ESubscription>> getAllSubscriptions() {
        return new ResponseEntity<>(eSubscriptionServices.getAllSubscription(), HttpStatus.OK);
    }

    @RequestMapping(value = "/subscribe-news", method = RequestMethod.POST)
    @ResponseBody
    String home(ESubscription subscription, HttpServletResponse httpResponse, HttpServletRequest request) {

        eSubscriptionServices.subscribe(subscription.getEmail());
        try {
            URL url = new URL(request.getHeader("referer"));
            httpResponse.sendRedirect(url.getPath());
        } catch (MalformedURLException e) {
            logger.info("Cannot construct redirect url. " + e);
        } catch (IOException e) {
            logger.info("Cannot set Redirect Url. " + e);
        }
        return null;
    }
}

package com.binuvcore.controllers.admin;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.businessobjects.transformers.UserDtoTransformer;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class UserControllerAdmin {

    @Autowired
    private UserServices userServices;

    @GetMapping("/admin/all-user")
    public ModelAndView userPage(Model model) {


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());
        ParameterPasser parameterPasser = (ParameterPasser) model.asMap().get("parameterPasser");

        if (parameterPasser == null) {
            parameterPasser = new ParameterPasser();
        }

        ModelAndView mav = new ModelAndView();
        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
            mav.addObject("users", transformUsers(userServices.getAllUsers()));
        } else {
            parameterPasser.setLogged_in(false);
            mav.addObject("users", new ArrayList<User>());
        }

        parameterPasser.setPageName("admin");
        mav.addObject("parameterPasser", parameterPasser);
        mav.setViewName("admin/all-users");
        return mav;
    }

    private List<UserDtoTransformer> transformUsers(List<User> userList) {
        List<UserDtoTransformer> transformerList = new ArrayList<>();
        for (User user :
                userList) {
            UserDtoTransformer userDtoTransformer = new UserDtoTransformer();
            userDtoTransformer.setId(user.getId());
            userDtoTransformer.setEmail(user.getEmail());
            userDtoTransformer.setRole(user.getRole());
            userDtoTransformer.setFirstName(user.getFirstName());
            userDtoTransformer.setLastName(user.getLastName());
            userDtoTransformer.setCreatedDate(DateTimeUtils.convertLongDateToStringDateShortFormat(user.getCreatedDate()));
            userDtoTransformer.setLastUpdatedDate(DateTimeUtils.convertLongDateToStringDateShortFormat(user.getLastUpdatedDate()));
            userDtoTransformer.setDeleted(user.isDeleted());
            transformerList.add(userDtoTransformer);
        }
        return transformerList;
    }
}

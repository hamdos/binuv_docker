package com.binuvcore.controllers.pressifier;

import com.binuvcore.businessobjects.pressifier.ArticleDetailDtoTransformer;
import com.binuvcore.businessobjects.pressifier.ArticleDtoTransformer;
import com.binuvcore.businessobjects.pressifier.MainJsonTransformer;
import com.binuvcore.helper.StringHelper;
import com.binuvcore.services.pressifier.PressifierServices;
import com.binuvcore.services.admin.RequestHistoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(path = "/pressifier/article")
public class PressifierController {

    @Autowired
    private PressifierServices pressifierServices;

    @Autowired
    private RequestHistoryServices requestHistoryServices;

    private StringHelper stringHelper = new StringHelper();

    @GetMapping(path = "/all")
    public ResponseEntity getAllArticles(@RequestParam(required=false) String lang,
                                         @RequestParam(required=false) String lat,
                                         @RequestParam(required=false) String lon,
                                         @RequestParam(required=false) String next,
                                         HttpServletRequest request) {

        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());
        return new ResponseEntity<MainJsonTransformer>(pressifierServices.getAllArticles(lang, lat, lon, next), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity getArticleById(@PathVariable(value = "id") String id){
        ArticleDetailDtoTransformer transformer = pressifierServices.getArticleById(id);
        if (transformer != null) {
            return new ResponseEntity<ArticleDetailDtoTransformer> (transformer, HttpStatus.OK);
        } else {
            return new ResponseEntity<ArticleDetailDtoTransformer>(transformer, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping
    public ResponseEntity getArticleByCategoryId(@RequestParam(value = "categoryId", required = false) Integer categoryId,
                                                 @RequestParam(value = "categoryName", required = false) String categoryName){

        if (categoryId != null) {
            List<ArticleDtoTransformer> transformer = pressifierServices.getArticlesByCategoryId(categoryId);
            if (transformer != null) {
                return new ResponseEntity<List<ArticleDtoTransformer>> (transformer, HttpStatus.OK);
            } else {
                return new ResponseEntity<String>("[]", HttpStatus.NOT_FOUND);
            }
        } else if (!stringHelper.isWhiteSpaceOrNull(categoryName)) {
            List<ArticleDtoTransformer> transformer = pressifierServices.getArticlesByCategoryName(categoryName);
            if (transformer != null) {
                return new ResponseEntity<List<ArticleDtoTransformer>> (transformer, HttpStatus.OK);
            }else {
                return new ResponseEntity<String>("[]", HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<String>("[]", HttpStatus.NOT_FOUND);
        }
    }
}

package com.binuvcore.businessobjects.pressifier;

import com.binuvcore.helper.CategoryDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class MainJsonTransformer {

    @Getter
    @Setter
    private String version;

    @Getter
    @Setter
    private String last_updated;

    @Getter
    @Setter
    private List<CategoryDto> categories;

    @Getter
    @Setter
    private List<ArticleDtoTransformer> articles;

    // used for pagination purpose
    @Getter
    @Setter
    private String next;
}

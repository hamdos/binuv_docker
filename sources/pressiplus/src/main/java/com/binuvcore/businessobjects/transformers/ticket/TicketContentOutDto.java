package com.binuvcore.businessobjects.transformers.ticket;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class TicketContentOutDto {

    @Getter
    @Setter
    private String message;

    @Getter
    @Setter
    private String createdDate;

    @Getter
    @Setter
    private String userName;

    @Getter
    @Setter
    private String userRole;

    @Getter
    @Setter
    private boolean owner;
}

package com.binuvcore.businessobjects.webapp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity(name = "contact_form")
@Table(name = "contact_form", schema = "public")
public class ContactForm {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "name")
    private String name;

    @Getter
    @Setter
    @Column(name = "email")
    private String email;

    @Getter
    @Setter
    @Column(name = "phone")
    private String phone;

    @Getter
    @Setter
    @Column(name = "message")
    private String message;

    @Getter
    @Setter
    @Column(name = "created_date")
    private long createdDate;

    @Getter
    @Setter
    @Column(name = "delivered")
    private boolean delivered;
}


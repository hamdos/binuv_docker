package com.binuvcore.businessobjects.pressifier;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class ArticleDtoTransformer {

    @Getter
    @Setter
    private String id;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String thumbnail_url;

    @Getter
    @Setter
    private List<String> tags;

    @Getter
    @Setter
    private String short_description;

    @Getter
    @Setter
    private ProviderDtoTransformer provider;
}

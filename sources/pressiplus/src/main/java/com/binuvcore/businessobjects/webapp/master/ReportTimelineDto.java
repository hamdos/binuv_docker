package com.binuvcore.businessobjects.webapp.master;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
@Table(name = "progress_report", schema = "public")
public class ReportTimelineDto {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "day")
    private int day;

    @Getter
    @Setter
    @Column(name = "month")
    private String month;

    @Getter
    @Setter
    @Column(name = "week")
    private String week;

    @Getter
    @Setter
    @Column(name = "implemented")
    private String implemented;

    @Getter
    @Setter
    @Column(name = "planned")
    private String plannedToDo;

    @Getter
    @Setter
    @Column(name = "published_date")
    private long publishedDate;

    @Getter
    @Setter
    @Column(name = "updated_date")
    private long updatedDate;

    @Getter
    @Setter
    @Column(name = "deleted", columnDefinition = "BOOLEAN")
    private boolean deleted;

    @Getter
    @Setter
    @Column(name = "user_id", columnDefinition = "BOOLEAN")
    private Integer userId;

}

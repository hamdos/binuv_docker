package com.binuvcore.businessobjects.transformers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class TicketSupportTransformer {

    @Getter
    @Setter
    private String subject;

    @Getter
    @Setter
    private String message;

    @Getter
    @Setter
    private String createdDate;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private List<TicketSupportContentTransformer> ticketContents;

    @Getter
    @Setter
    private String formAction;
}

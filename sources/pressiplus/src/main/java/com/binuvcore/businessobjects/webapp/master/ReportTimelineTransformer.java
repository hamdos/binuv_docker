package com.binuvcore.businessobjects.webapp.master;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class ReportTimelineTransformer {

    @Getter
    @Setter
    private int day;

    @Getter
    @Setter
    private String month;

    @Getter
    @Setter
    private String week;

    @Getter
    @Setter
    private List<String> implemented;

    @Getter
    @Setter
    private List<String> plannedToDo;

    @Getter
    @Setter
    private String createdDate;

    @Getter
    @Setter
    private String updateDate;
}

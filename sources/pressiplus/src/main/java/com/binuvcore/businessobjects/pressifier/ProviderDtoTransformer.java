package com.binuvcore.businessobjects.pressifier;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class ProviderDtoTransformer {

    @Getter
    @Setter
    private String id;

    @Getter
    @Setter
    private String name;


    @Getter
    @Setter
    private String logo_url;
}

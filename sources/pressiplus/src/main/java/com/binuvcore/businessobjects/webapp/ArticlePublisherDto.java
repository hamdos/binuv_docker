package com.binuvcore.businessobjects.webapp;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ArticlePublisherDto {

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String article_unique_id;

    @Getter
    @Setter
    private String content;

    @Getter
    @Setter
    private String tags;

    @Getter
    @Setter
    private List<ContentDto> contentVersions;
}

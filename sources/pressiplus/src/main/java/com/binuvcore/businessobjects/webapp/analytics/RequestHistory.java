package com.binuvcore.businessobjects.webapp.analytics;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity(name = "request_history")
@Table(name = "request_history", schema = "public")
public class RequestHistory {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "request_url")
    private String requestUrl;

    @Getter
    @Setter
    @Column(name = "ip_address")
    private String ipAddress;

    @Getter
    @Setter
    @Column(name = "visited_dates")
    private long visitedDates;

    @Getter
    @Setter
    @Column(name = "postcode")
    private String postcode;

    @Getter
    @Setter
    @Column(name = "state")
    private String state;

    @Getter
    @Setter
    @Column(name = "city")
    private String city;

    @Getter
    @Setter
    @Column(name = "country")
    private String country;
}

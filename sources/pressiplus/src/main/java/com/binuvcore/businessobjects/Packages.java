package com.binuvcore.businessobjects;

import com.binuvcore.businessobjects.access.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity(name = "package")
@Table(name = "package", schema = "public")
public class Packages implements Serializable {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "created_date")
    private long createdDate;

    @Getter
    @Setter
    @Column(name = "deleted_date")
    private long deletedDate;

    @Getter
    @Setter
    @Column(name = "deleted", columnDefinition = "BOOLEAN")
    private boolean deleted;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User userPackage;

    @Getter
    @Setter
    @JoinColumn(name = "category_id")
    private Integer category_id;

    public Packages(long createdDate, long deletedDate, boolean deleted, User userPackage, Integer categoryPackage) {
        this.createdDate = createdDate;
        this.deletedDate = deletedDate;
        this.deleted = deleted;
        this.userPackage = userPackage;
        this.category_id = categoryPackage;
    }
}
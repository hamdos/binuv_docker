package com.binuvcore.businessobjects.transformers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class ESubscriptionTransformer {

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private boolean unsubscribed;

    @Getter
    @Setter
    private String subscribed_date;
}

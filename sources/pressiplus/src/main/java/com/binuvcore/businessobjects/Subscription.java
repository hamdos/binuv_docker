package com.binuvcore.businessobjects;

import com.binuvcore.businessobjects.access.User;
import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@ToString
@Entity
@Table(name = "subscription", schema = "public")
public class Subscription {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "subscribed_date")
    private long subscribedDate;

    @Getter
    @Setter
    @Column(name = "unsubscribed_date")
    private long unsubscribedDate;

    @Getter
    @Setter
    @Column(name = "unsubscribed", columnDefinition = "BOOLEAN")
    private boolean unsubscribed;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "provider_id", nullable = false)
    private Provider provider;

    public Subscription(long subscribedDate, long unsubscribedDate, boolean unsubscribed, User user, Provider provider) {
        this.subscribedDate = subscribedDate;
        this.unsubscribed = unsubscribed;
        this.unsubscribedDate = unsubscribedDate;
        this.user = user;
        this.provider = provider;
    }
}

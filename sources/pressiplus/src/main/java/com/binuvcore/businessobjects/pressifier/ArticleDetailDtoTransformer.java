package com.binuvcore.businessobjects.pressifier;

import com.binuvcore.helper.CategoryDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class ArticleDetailDtoTransformer {

    @Getter
    @Setter
    private String id;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String image_link;

    @Getter
    @Setter
    private String thumbnail_url;

    @Getter
    @Setter
    private String imported_date;

    @Getter
    @Setter
    private List<String> tags;

    @Getter
    @Setter
    private String content;

    @Getter
    @Setter
    private AuthorDtoTransformer author;

    @Getter
    @Setter
    private ProviderDtoTransformer provider;

    @Getter
    @Setter
    private CategoryDto category;

    @Getter
    @Setter
    private List<ArticleDtoTransformer> related_articles;
}

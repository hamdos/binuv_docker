package com.binuvcore.businessobjects.transformers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class UserDtoTransformer {

    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    private String firstName;

    @Getter
    @Setter
    private String lastName;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String createdDate;

    @Getter
    @Setter
    private String lastUpdatedDate;

    @Getter
    @Setter
    private boolean deleted;

    @Getter
    @Setter
    private String role;
}

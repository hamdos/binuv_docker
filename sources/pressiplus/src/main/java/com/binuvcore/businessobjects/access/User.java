package com.binuvcore.businessobjects.access;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity(name = "user")
@Table(name = "user", schema = "public")
public class User implements Serializable {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "public_id")
    private String publicId;

    @Getter
    @Setter
    @Column(name="username")
    private String username;

    @Getter
    @Setter
    @Column(name="first_name")
    private String firstName;

    @Getter
    @Setter
    @Column(name="last_name")
    private String lastName;

    @Getter
    @Setter
    @Column(name="email")
    private String email;

    @Getter
    @Setter
    @Column(name="password")
    private String password;

    @Getter
    @Setter
    @Column(name = "created_date")
    private long createdDate;

    @Getter
    @Setter
    @Column(name = "last_updated_date")
    private long lastUpdatedDate;

    @Getter
    @Setter
    @Column(name = "login_attempt_count")
    private Integer loginAttemptCount;

    @Getter
    @Setter
    @Column(name = "password_change_required", columnDefinition = "BOOLEAN")
    private boolean passwordChangeRequired;

    @Getter
    @Setter
    @Column(name = "deleted", columnDefinition = "BOOLEAN")
    private boolean deleted;

    @Getter
    @Setter
    @Column(name = "role")
    private String role;

    public User(String publicId, String username, String firstName, String lastName, String email, String password,
                long createdDate, long lastUpdatedDate, Integer loginAttemptCount,
                boolean passwordChangeRequired, boolean deleted, String role) {
        this.publicId = publicId;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.createdDate = createdDate;
        this.lastUpdatedDate = lastUpdatedDate;
        this.loginAttemptCount = loginAttemptCount;
        this.passwordChangeRequired = passwordChangeRequired;
        this.deleted = deleted;
        this.role = role;
    }
}

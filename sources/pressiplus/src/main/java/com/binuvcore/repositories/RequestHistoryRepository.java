package com.binuvcore.repositories;

import com.binuvcore.businessobjects.webapp.analytics.RequestHistory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RequestHistoryRepository extends CrudRepository<RequestHistory, Integer> {

    List<RequestHistory> findAllByIpAddress(String ipAddress);
    List<RequestHistory> findAllByCity(String city);
    List<RequestHistory> findAllByRequestUrl(String requestUrl);
}

package com.binuvcore.repositories;

import com.binuvcore.businessobjects.Shared;
import com.binuvcore.businessobjects.Article;
import com.binuvcore.businessobjects.access.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SharedRepository extends CrudRepository<Shared, Integer> {

    List<Shared> findSharedByUser(User user);
    List<Shared> findSharedByArticle(Article article);
    List<Shared> findSharedByUserAndArticle(User user, Article article);


}

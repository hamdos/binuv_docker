package com.binuvcore.repositories;

import com.binuvcore.businessobjects.BlockedProvider;
import com.binuvcore.businessobjects.Provider;
import com.binuvcore.businessobjects.access.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BlockedProviderRepository extends CrudRepository<BlockedProvider, Integer> {

    List<BlockedProvider> findBlockedProvidersByUser(User user);
    List<BlockedProvider> findBlockedProvidersByProvider(Provider provider);
    List<BlockedProvider> findBlockedProvidersByUserAndProvider(User user, Provider provider);


}

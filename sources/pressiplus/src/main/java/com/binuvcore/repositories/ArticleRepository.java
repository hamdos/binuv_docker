package com.binuvcore.repositories;

import com.binuvcore.businessobjects.Article;
import com.binuvcore.businessobjects.Provider;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ArticleRepository extends CrudRepository<Article, Integer> {

    List<Article> findArticleByUrl(String url);
    List<Article> findAllByTitleAndCategoryAndSection(String title, String category, String section);
    List<Article> findAllByTitleContains(String title);
    List<Article> findAllByLanguage(String language);
    List<Article> findAllByLocationContains(String location);
    List<Article> findAllByLocationContainsAndLanguage(String location, String language);
    List<Article> findAllByCategoryContainingAndSectionContaining(String category, String section);
    List<Article> findAllByCategory(String category);

}

package com.binuvcore.repositories;

import com.binuvcore.businessobjects.webapp.ContactForm;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContactFormRepository extends CrudRepository<ContactForm, Integer> {

    List<ContactForm> findAllByDelivered(boolean delivered);
}

package com.binuvcore.repositories;

import com.binuvcore.businessobjects.webapp.ArticlePublisher;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticlePublisherRepository extends CrudRepository<ArticlePublisher, Integer> {
    ArticlePublisher findArticlePublisherByArticleUniqueId(String articleUniqueId);
}

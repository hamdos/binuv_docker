package com.binuvcore.repositories;

import com.binuvcore.businessobjects.Provider;
import com.binuvcore.businessobjects.Subscription;
import com.binuvcore.businessobjects.access.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SubscriptionRepository extends CrudRepository<Subscription, Integer> {

    Subscription findSubscriptionsByUserAndProvider(User user, Provider provider);
    List<Subscription> findSubscriptionsByUser(User user);

}

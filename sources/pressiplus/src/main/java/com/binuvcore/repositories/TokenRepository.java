package com.binuvcore.repositories;

import com.binuvcore.businessobjects.access.Token;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TokenRepository  extends CrudRepository<Token, Integer> {

//    List<Token> findByValue(String value);

    Token findByValue(String value);
}

package com.binuvcore.repositories.binuv;

import com.binuvcore.businessobjects.webapp.ticket.Ticket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends CrudRepository<Ticket, Integer> {

    Ticket findByTitleAndCreatedDate(String title, long createdDate);
    List<Ticket> findAllByPublicTicket(boolean publicTicker);
}

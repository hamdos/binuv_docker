package com.binuvcore.repositories;

import com.binuvcore.businessobjects.webapp.analytics.ESubscription;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ESubscriptionRepository extends CrudRepository<ESubscription, Integer> {

    boolean existsByEmail(String email);
}

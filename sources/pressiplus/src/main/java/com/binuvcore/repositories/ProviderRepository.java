package com.binuvcore.repositories;

import com.binuvcore.businessobjects.Provider;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProviderRepository extends CrudRepository<Provider, Integer> {

    List<Provider> findByNameContaining(String name);

    List<Provider> findProvidersByIdAndName(Integer id, String name);

    List<Provider> findProvidersByNameAndAddressAndApiKey(String name, String address, String apiKey);


    Provider findByName(String name);

}

package com.binuvcore.repositories.binuv;

import com.binuvcore.businessobjects.webapp.ticket.TicketContent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketContentRepository extends CrudRepository<TicketContent, Integer> {

    List<TicketContent> findAllByTicketId(Integer ticketId);

    @Override
    void deleteAll(Iterable<? extends TicketContent> iterable);
}

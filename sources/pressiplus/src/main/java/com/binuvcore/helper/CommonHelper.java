package com.binuvcore.helper;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.binuvcore.helper.date.DateTimeUtils;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

public class CommonHelper {

    public LocationDto getAddressByLatAndLong(final Double lat, final Double longt) {
        StringBuilder query = new StringBuilder();
        query.append("https://nominatim.openstreetmap.org/reverse.php");
        query.append("?lat=").append(lat.toString());
        query.append("&lon=").append(longt.toString());

        query.append("&format=jsonv2");
        final RestTemplate restTemplate = new RestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        final ResponseEntity<LocationDto> response = restTemplate.exchange(query.toString(), HttpMethod.GET, entity,
                LocationDto.class);
        if (response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        } else {
            return null;
        }
    }

    private static List<String> makeListFromString(final String text) {

        String[] slitted = text.split("==");
        for (int i = 0; i < slitted.length; i++) {
            slitted[i] = slitted[i].trim();
        }
        return Arrays.asList(slitted);
    }


    public static void main(String[] args) {

        long re = 1585686075;
        System.out.println();
        System.out.println(DateTimeUtils.convertLongDateToStringDate(new Date().getTime()));
        System.out.println(DateTimeUtils.convertLongDateToStringDateShortFormat(new Date().getTime()));
        System.out.println(DateTimeUtils.convertLongDateToStringDate(re));

    }
}

package com.binuvcore.helper.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


/**
 * This class is responsible to do date and time operations
 *
 * @author doston, 17.12.2019
 */
public class DateTimeUtils {

    static SimpleDateFormat sdf;

    public static final String DATE_AND_TIME_FORMAT = "EEE, dd MMM yyyy HH:mm:ss z";

    public DateTimeUtils() {
    }

    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, days);

        return cal.getTime();
    }

    public static Date addMinutes(Date date, int minutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes);

        return cal.getTime();
    }

    public static String convertDateToString(Date date) {
        sdf = new SimpleDateFormat(DATE_AND_TIME_FORMAT);

        Date d = date;

        return sdf.format(d);
    }

    public static Date convertStringToDate(String date) throws Exception {
        sdf = new SimpleDateFormat(DATE_AND_TIME_FORMAT);

        return sdf.parse(date);
    }

    public static boolean olderThanOneDay(long date) {
        return differenceTime(date) / (24 * 60 * 60 * 1000) > 0;
    }

    public static long differenceTime(long updateDate) {
        long difference = 0;

        try {
            Date updated = new Date(updateDate);

            System.out.println("Now: " + convertLongDateToStringDate(getCurrentDateInDate().getTime()));
            System.out.println("Given: " + convertLongDateToStringDate(updateDate));

            difference = getCurrentDateInDate().getTime() - updated.getTime();

//            difference = difference / 1000;

            long diffSeconds = difference / 1000 % 60;
            long diffMinutes = difference / (60 * 1000) % 60;
            long diffHours = difference / (60 * 60 * 1000) % 24;
            long diffDays = difference / (24 * 60 * 60 * 1000);

            System.out.println(diffDays + " days, ");
            System.out.println(diffHours + " hours, ");
            System.out.println(diffMinutes + " minutes, ");
            System.out.println(diffSeconds + " seconds.");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return difference;
    }

    public static Date getCurrentDateInDate() throws ParseException {
        sdf = new SimpleDateFormat(DATE_AND_TIME_FORMAT);

        return sdf.parse(getCurrentDateInString());
    }

    public static String getCurrentDateInString() {
        sdf = new SimpleDateFormat(DATE_AND_TIME_FORMAT);

        Date d = new Date();

        return sdf.format(d);
    }

    public Date convertLongDateToDate(long longtime) throws ParseException {
        return new Date(longtime);
    }

    public static String getShortDateTime(long longtime) {
        final SimpleDateFormat dt = new SimpleDateFormat("HH:mm dd-MM-yyyy");
        return dt.format(new Date(longtime));
    }

    public static String getOnlyTime(long longtime) {
        final SimpleDateFormat dt = new SimpleDateFormat("HH:mm");
        return dt.format(new Date(longtime));
    }

    public static String convertLongDateToStringDate(long longtime) {
        Date d = new Date(longtime);
        return convertDateToString(d);
    }

    public static String convertLongDateToStringDateShortFormat(long longtime) {

        final SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return dt.format(new Date(longtime));
    }

    public static long getCurrentDateInLong() {
        long response = 0;
        try {
            sdf = new SimpleDateFormat(DATE_AND_TIME_FORMAT);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+1"));

            response = sdf.parse(getCurrentDateInString()).getTime();
        } catch (ParseException e){
            e.printStackTrace();
        }
        return response;

    }
}

package com.binuvcore.static_data;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.helper.date.DateTimeUtils;

import static org.apache.tomcat.util.http.FastHttpDateFormat.parseDate;

import java.util.ArrayList;
import java.util.List;


/**
 * Class where store static/constant data to be used in Tests
 *
 * @author doston, 18.12.2019
 */
public class PersonStaticData {

    //~ Methods --------------------------------------------------------------------------------------------------------

    /**
     * Get all persons
     *
     * @return list of persons
     */
    public static List<User> getStaticListOfPerson() {
        User personJpa1 = new User("", "doston", "Doston", "Ham", "doston@binuv.com", "password", DateTimeUtils.getCurrentDateInLong(),
                DateTimeUtils.getCurrentDateInLong(), 1, false, false, "ADMIN"
        );
        List<User> personJpaList = new ArrayList<>();
        personJpaList.add(personJpa1);
        personJpaList.add(getStaticPerson());

        return personJpaList;
    }

    /**
     * Get a person
     *
     * @return a single person
     */
    public static User getStaticPerson() {
        User personJpa1 = new User("", "john", "Johnton", "Smith", "john@binuv.com", "password", DateTimeUtils.getCurrentDateInLong(),
                DateTimeUtils.getCurrentDateInLong(), 1, false, false, "USER");

        return personJpa1;
    }
}

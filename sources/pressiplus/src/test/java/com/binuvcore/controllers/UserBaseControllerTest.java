package com.binuvcore.controllers;

import com.binuvcore.businessobjects.access.User;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

import org.junit.runner.RunWith;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import org.springframework.core.ParameterizedTypeReference;

import org.springframework.http.*;

import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;


/**
 * Test class for {@link UserController}
 *
 * @author doston, 18.12.2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserBaseControllerTest {

    //~ Instance fields ------------------------------------------------------------------------------------------------

    /**
     * a variable
     */
    @LocalServerPort
    int randomServerPort;

    //~ Methods --------------------------------------------------------------------------------------------------------

    /**
     * Test for {@link UserController#getAllPersons()}
     *
     * @throws URISyntaxException
     */
    @Ignore
    @Test
    public void testGetAllPersons() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();

        final String baseUrl = "http://localhost:" + randomServerPort + "/person/all";
        URI uri = new URI(baseUrl);

        HttpHeaders headers = new HttpHeaders();

        // headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        // headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity requestEntity = new HttpEntity(headers);

        ResponseEntity<List<User>> result = restTemplate.exchange(uri, HttpMethod.GET, requestEntity,
                                                                       new ParameterizedTypeReference<List<User>>() {
                                                                       });

        User personJpa = new User();

        // Verify request succeed
        assertEquals(200, result.getStatusCodeValue());
    }
}

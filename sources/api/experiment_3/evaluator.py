import pandas as pd
import numpy as np
from . import model_builder
import json
from . import APP_DIR

def evaluate(docs):

  with open(f'{APP_DIR}/model/features.json', 'r') as fp:
    features = json.load(fp)

  vocab = features['vocab']
  class_labels = features['class_labels']

  x_test = []
  for doc in docs:
      x_test.append(model_builder.build_bow(doc, vocab))


  model = model_builder.build_nn(len(vocab), len(class_labels))
  model.load(f'{APP_DIR}/model/fp.tfl')


  results = []
  for x in x_test :
      prediction_result = model.predict([x])  # here list representation is important
      predicted_class = class_labels[np.argmax(prediction_result)]  # fancy stuffs
      rating = float(predicted_class[1:])/100

      result = {"rating": rating}
      results.append(result)

  return results
import pandas as pd
import json
from . import evaluator
from . import APP_DIR

df = pd.read_json(f'{APP_DIR}/data/input.json', orient='records')
# df = pd.read_json(f'{APP_DIR}/data/fp_ds_combined_no_val.json', orient='records')
# print(df.groupby('class').count())
docs = df['entities']

result = evaluator.evaluate(docs)

print(result)
with open(f'{APP_DIR}/data/output.json', 'w') as fp:
  json.dump(result, fp)
import pandas as pd
import numpy as np
import json
from . import model_builder
from . import APP_DIR
import ast


# Loading data
df = pd.read_json(f'{APP_DIR}/data/fp_ds_combined.json', orient='records')
df['entities'] = df['entities'].apply(lambda x: ast.literal_eval(x))

articles = df[["entities", "class", "value"]]


# Feature extraction
words = sorted(set(articles['entities'].sum()))
class_labels = sorted(pd.unique(articles['class']))

with open(f'{APP_DIR}/model/features.json', 'w') as fp:
  features = {'words': words, 'class_labels': class_labels}
  json.dump(features, fp, ensure_ascii=False)


# Building training data
x_train, y_train = [], []
for index, article in articles.iterrows():
  tokens = article['entities']
  bow = model_builder.build_bow(tokens, words)
  input_row = [article['value']] + bow

  output_row = [0] * len(class_labels)
  output_row[class_labels.index(article['class'])] = 1

  x_train.append(input_row)
  y_train.append(output_row)


# Building model
model = model_builder.build_nn(len(x_train[0]), len(y_train[0]))
model.fit(x_train,
          y_train,
          validation_set=0.33,
          n_epoch=10,
          batch_size=8,
          show_metric=True
)
model.save(f'{APP_DIR}/model/fp.tfl')

print('Done!')

import pandas as pd
import numpy as np
from . import model_builder
from . import APP_DIR
import json

def evaluate(docs):

  import os
  print(os.listdir(os.getcwd()))

  with open(f'{APP_DIR}/model/features.json', 'r') as fp:
    features = json.load(fp)

  vocab = features['vocab']
  class_labels = features['class_labels']

  x_test = []
  for doc in docs:
      x_test.append(model_builder.build_bow(doc, vocab))


  model = model_builder.build_nn(len(vocab), len(class_labels))
  model.load(f'{APP_DIR}/model/fp.tfl')


  results = []
  for i, x in enumerate(iterable=x_test, start=1) :
      prediction_result = model.predict([x])  # here list representation is important
      predicted_class = class_labels[np.argmax(prediction_result)]  # fancy stuffs

      result = {"class_label": predicted_class}
      results.append(result)

  return results
# BINUV API

## Requirements
Runtime: Python

## Install dependencies
    pip install -r requirements.txt

## How to run:
    python main.py

## API endpoints
+ Host: localhost
+ Port: 5000
+ Urls:
    - POST: /api/articles/classify
    - POST: /api/articles/rate

## Sample request
```
POST /api/articles/rate HTTP/1.1
Host: localhost
Content-Type: application/json

[
  {
    "value": 1,
    "category": "Ratgeber",
    "section": "Ratgeber",
    "image": 0,
    "entities": [
      "Steinschlag",
      "Verkehrsmeldungen",
      "Siebitz",
      "Prag",
      "Wilsdruff",
      "Schkeuditz"
    ],
    "title": "Verkehrsmeldungen",
    "subtitle": "article subtitle",
    "text": "article text"
  },
  {
    "value": 1,
    "category": "Nachrichten",
    "section": "Regioticker",
    "image": 0,
    "entities": [
      "Gericht",
      "Untreue",
      "Plauen",
      "Oelsnitz"
    ],
    "title": "Plauen\/Oelsnitz: Gesch\u00e4ftsleiter der Volkssolidarit\u00e4t vor Gericht",
    "subtitle": "article subtitle",
    "text": "article content"
  }
]
```
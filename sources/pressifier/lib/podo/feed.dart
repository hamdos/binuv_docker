import 'dart:convert';

class Category {
  int id;
  String name;

  Category(this.id, this.name);

  factory Category.fromJson(Map<String, dynamic> json) {
    //jsonEncode(json['id'].toString())
    return Category(int.parse(json['id'].toString()), json['name'] as String);
  }

  @override
  String toString() {
    return 'name: ${this.name}';
  }
}

class Provider {
  String id;
  String name;
  String logoURL;

  Provider(this.id, this.name, this.logoURL);

  factory Provider.fromJson(dynamic json) {
    return Provider(
        json['id'] as String,
        json['name'] as String,
        json['logo_url'] as String);
  }

  @override
  String toString() {
    return 'name: ${this.name}';
  }
}

class FeedContent {
  String id;
  String thumbnailUrl;
  String title;
  List<String> tags;
  String shortDescription;
  Provider provider;
  List<Category> categories;
  FeedContent(this.id, this.title, this.thumbnailUrl, this.tags, this.shortDescription, this.provider, this.categories);


  factory FeedContent.fromJson(dynamic json) {
    List<String> tagsData = new List<String>();

    json['tags'].forEach((v) => { tagsData.add(v.toString())});

    var categoriesData = new List<Category>();
    return FeedContent(
        json['id'] as String,
        json['title'] as String,
        json['thumbnail_url'] as String,
        tagsData,
        json['short_description'] as String,
        Provider.fromJson(json['provider']),
        categoriesData);
  }

  @override
  String toString() {
    return 'title: ${this.title}';
  }
}

class Feed {
  String version;
  String lastUpdated;
  List<FeedContent> content;
  String next;
  List<Category> categories;
  Feed({this.version, this.lastUpdated, this.content ,this.next});

  Feed.fromJson(dynamic json) {

    content = new List<FeedContent>();
    categories = new List<Category>();
    json['articles'].forEach((v) => {
      content.add(new FeedContent.fromJson(v))
    });

    json['categories'].forEach((v) => {
      //print(jsonEncode(v))
      categories.add(new Category.fromJson(v))
    });

    version = json['version'] as String;
    lastUpdated = json['last_updated'] as String;
    next = json['next'] as String;
  }


  @override
  String toString() {
    return 'feed: ok';
  }
}

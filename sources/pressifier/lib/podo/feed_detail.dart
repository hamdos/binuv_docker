
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_ebook_app/podo/feed.dart';

class Author {
  String id;
  String firstName;
  String lastName;
  String name;
  String imageUrl;


  Author(this.id, this.firstName, this.lastName, this.name, this.imageUrl);


  factory Author.fromJson(dynamic json) {
    return Author(
      json['public_id'] as String,
      json['firstName'] as String,
      json['lastName'] as String,
      json['fullname'] as String,
      json['image_url'] as String
    );
  }

}


class FeedDetail {

  String id;
  String title;
  String imageLink;
  String thumbnailUrl;
  String publishDate;
  List<String> tags;
  String content;
  Author author;
  Provider provider;
  List<FeedContent> relatedContent;
  Category category;

  FeedDetail({this.id, this.title, this.imageLink, this.thumbnailUrl ,this.publishDate, this.tags, this. content, this. author, this.provider, this.relatedContent});

  FeedDetail.fromJson(dynamic json) {

    id = json['id'] as String;
    title = json['title'] as String;
    imageLink= json['image_link'] as String;
    thumbnailUrl = json['thumbnail_url'] as String;
    publishDate = json['imported_date'] as String;
    tags = new List<String>();

    json['tags'].forEach((v) => {
      tags.add(v.toString())
    });
    category = Category.fromJson(json['category']);
    content = json['content'] as String;
    author = Author.fromJson(json['author']);
    provider = Provider.fromJson(json['provider']);
    relatedContent = new List<FeedContent>();
    json['related_articles'].forEach((v) => {
      relatedContent.add(new FeedContent.fromJson(v))
    });
  }

}
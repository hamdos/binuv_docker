import 'package:flutter/material.dart';
import 'package:flutter_ebook_app/podo/feed.dart';
import 'package:flutter_ebook_app/providers/home_provider.dart';
import 'package:flutter_ebook_app/screen/genre.dart';
import 'package:flutter_ebook_app/util/api.dart';
import 'package:flutter_ebook_app/util/consts.dart';
import 'package:flutter_ebook_app/widgets/book_card.dart';
import 'package:flutter_ebook_app/widgets/book_list_item.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class Home extends StatelessWidget {
  List<bool> isSelected = [false, false, false];
  @override
  Widget build(BuildContext context) {
    return Consumer<HomeProvider>(
      builder: (BuildContext context, HomeProvider homeProvider, Widget child) {
        return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            /*
            actions: <Widget>[
              ToggleButtons(
                children: <Widget>[
                  Icon(Feather.file),
                ],
                isSelected: [false],
                onPressed: (int index) {
                  //todo: add event
                },
              )
            ],
             */
            title: Text(
              "${Constants.appName}",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          body: homeProvider.loading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : RefreshIndicator(
                  onRefresh: () => homeProvider.getFeeds(null),
                  child: ListView(
                    children: <Widget>[
                      /*
                Container(
                  height: 200,
                  child: Center(
                    child: ListView.builder(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      scrollDirection: Axis.horizontal,
                      itemCount: homeProvider.top.content.length,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                        FeedContent entry = homeProvider.top.content[index];
                        return Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                          child: BookCard(
                            img: entry.thumbnailUrl,
                            entry: null,
                          ),
                        );
                      },
                    ),
                  ),
                ),

                SizedBox(height: 20,),
                */

                      /*Categories*/
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Categories",
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),


                      Container(
                        height: 50,
                        child: Container(
                          child: ListView.builder(
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            scrollDirection: Axis.horizontal,
                            itemCount: homeProvider.recent.categories.length,
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context, int index) {
                              Category cate = homeProvider.recent.categories[index];

                              return Padding(
                                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).accentColor,
                                    borderRadius: BorderRadius.all(Radius.circular(20),),
                                  ),
                                  child: InkWell(
                                    borderRadius: BorderRadius.all(Radius.circular(20),),
                                    onTap: (){
                                      Navigator.push(
                                        context,
                                        PageTransition(
                                          type: PageTransitionType.rightToLeft,
                                          child: Genre(
                                            title: cate.name,
                                            id: cate.id.toString(),
                                          ),
                                        ),
                                      );
                                    },
                                    child: Center(
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(horizontal: 10),
                                        child: Text(
                                          cate.name,
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),

                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Recently Added",
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      ListView.builder(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: homeProvider.recent.content.length,
                        itemBuilder: (BuildContext context, int index) {
                          FeedContent entry =
                              homeProvider.recent.content[index];

                          return Padding(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            child: BookListItem(
                              img: entry.thumbnailUrl,
                              title: entry.title,
                              desc: entry.shortDescription,
                              entry: entry,
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
        );
      },
    );
  }
}

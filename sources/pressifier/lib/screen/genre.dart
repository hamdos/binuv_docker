import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_ebook_app/podo/category.dart';
import 'package:flutter_ebook_app/podo/feed.dart';
import 'package:flutter_ebook_app/util/api.dart';
import 'package:flutter_ebook_app/widgets/book_list_item.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Genre extends StatefulWidget{
  final String title;
  final String id;

  Genre({
    Key key,
    @required this.title,
    @required this.id,
  }): super(key:key);

  @override
  _GenreState createState() => _GenreState();
}

class _GenreState extends State<Genre> {

  ScrollController controller = ScrollController();
  List<FeedContent> items;
  bool loading = true;
  int page = 1;
  bool pagi = false;
  bool showListenerFlag = true;
  bool loadMore = true;

  scrollListener(){
    if(showListenerFlag){
      if (controller.offset >= controller.position.maxScrollExtent &&
          !controller.position.outOfRange) {
        //paginate();
      }
    }
  }

  getFeed(id) async{
    setState(() {
      loading = true;
    });
    Api.GetAllArticleByCate(id).then((feeds){
      if(mounted){
        setState(() {
          items = new List();
          items.addAll(feeds);
          print(items.length);
        });
      }
   }).catchError((e){
      Fluttertoast.showToast(
        msg: "Something went wrong",
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIos: 1,
      );
      throw(e);
    }).whenComplete(() {
      print(items.length);
      setState(() {
        loading = false;
      });
    });
  }



  @override
  void initState() {
    super.initState();
    getFeed(widget.id);
    //controller.addListener(scrollListener);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("${widget.title}"),
      ),
      body: loading
          ? Center(
        child: CircularProgressIndicator(),
      )
          : ListView(
        controller: controller,
        children: <Widget>[
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            padding: EdgeInsets.symmetric(horizontal: 10),
            shrinkWrap: true,
            itemCount: items.length,
            itemBuilder: (BuildContext context, int index) {
              FeedContent entry = items[index];
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: BookListItem(
                  img: entry.thumbnailUrl,
                  title: entry.title,
                  //author: entry.author.name.t,
                  desc: entry.shortDescription,
                  entry: entry,
                ),
              );
            },
          ),
          SizedBox(height: 10,),

          /*
          pagi
              ?Container(
            height: 80,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          )
              :SizedBox(),
          */
        ],
      ),
    );
  }
}

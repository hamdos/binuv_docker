import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:epub_kitty/epub_kitty.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ebook_app/podo/feed.dart';
import 'package:flutter_ebook_app/podo/feed_detail.dart';
import 'package:flutter_ebook_app/providers/details_provider.dart';
import 'package:flutter_ebook_app/util/consts.dart';
import 'package:flutter_ebook_app/widgets/book_list_item.dart';
import 'package:flutter_ebook_app/widgets/description_text.dart';
import 'package:flutter_ebook_app/widgets/download_alert.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';


// ignore: must_be_immutable
class Details extends StatelessWidget {
  final FeedDetail entry;
  final String imgTag;
  final String titleTag;

  Details({
    Key key,
    @required this.entry,
    @required this.imgTag,
    @required this.titleTag,
  }): super(key:key);
  static const pageChannel = const EventChannel('com.xiaofwang.epub_kitty/page');

  @override
  Widget build(BuildContext context) {
    return Consumer<DetailsProvider>(
      builder: (BuildContext context, DetailsProvider detailsProvider, Widget child) {
        return Scaffold(
          appBar: AppBar(
            actions: <Widget>[
//          IconButton(
//            onPressed: (){},
//            icon: Icon(
//              Feather.download,
//            ),
//          ),

              IconButton(
                onPressed: () async{
                  if(detailsProvider.faved){
                    detailsProvider.removeFav();
                  }else{
                    detailsProvider.addFav();
                  }
                },
                icon: Icon(
                  detailsProvider.faved
                      ? Icons.favorite
                      : Feather.heart,
                  color: detailsProvider.faved
                      ? Colors.red
                      : Theme.of(context).iconTheme.color,
                ),
              ),
            ],
          ),

          body: ListView(
            padding: EdgeInsets.symmetric(horizontal: 20),
            children: <Widget>[
              SizedBox(height: 10,),

              Container(
                height: 200,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Hero(
                      tag: imgTag,
                      child: CachedNetworkImage(
                        imageUrl: detailsProvider.entry.thumbnailUrl,
                        placeholder: (context, url) => Container(
                          height: 200,
                          width: 130,
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                        errorWidget: (context, url, error) => Icon(Feather.x),
                        fit: BoxFit.cover,
                        height: 200,
                        width: 130,
                      ),
                    ),

                    SizedBox(width: 20,),

                    Flexible(
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 5,),
                          Hero(
                            tag: titleTag,
                            child: Material(
                              type: MaterialType.transparency,
                              child: Text(
                                entry.title,
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                                maxLines: 3,
                              ),
                            ),
                          ),
                          SizedBox(height: 5,),

                          Hero(
                            tag: detailsProvider.entry.author.id,
                            child: Material(
                              type: MaterialType.transparency,
                              child: Text(
                                detailsProvider.entry.author.name,
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w800,
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),

                          SizedBox(height: 5,),
                          /*
                          entry.categories == null?SizedBox():Container(
                            height: entry.categories.length<3?40:80,
                            child: GridView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: entry.categories.length>4?4:entry.categories.length,
                              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                childAspectRatio: 210/80,
                              ),
                              itemBuilder: (BuildContext context, int index) {
                                Category cat = entry.categories[index];
                                return Padding(
                                  padding: EdgeInsets.fromLTRB(0, 5, 5, 5),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Theme.of(context).backgroundColor,
                                      borderRadius: BorderRadius.all(Radius.circular(20),),
                                      border: Border.all(
                                        color: Theme.of(context).accentColor,
                                      ),
                                    ),
                                    child: Center(
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(horizontal: 2),
                                        child: Text(
                                          cat.name,
                                          style: TextStyle(
                                            color: Theme.of(context).accentColor,
                                            fontSize: 6,
                                            //fontSize: cat.label.length >18 ? 6:10,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                          */
                        ],
                      ),
                    ),
                  ],
                ),
              ),


              SizedBox(height: 30,),

              DescriptionTextWidget(
                text: detailsProvider.entry.content,
              ),

              SizedBox(height: 30,),

              Text(
                "Recent Articles",
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),

              Divider(color: Theme.of(context).textTheme.caption.color,),

              SizedBox(height: 10,),

              detailsProvider.loading
                  ? Container(
                height: 100,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              )
                  : ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: detailsProvider.related.length,
                itemBuilder: (BuildContext context, int index) {
                  //Entry entry = detailsProvider.related.feed.entry[index];
                  FeedContent content = detailsProvider.related[index];
                  return Padding(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    child: BookListItem(
                      img: content.thumbnailUrl,
                      title: content.title,
                      //author: entry.author.name.t,
                      desc: content.shortDescription,
                      entry: content,
                    ),
                  );
                },
              ),
            ],
          ),

        );
      },
    );
  }




}

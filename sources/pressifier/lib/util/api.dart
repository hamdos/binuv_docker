import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter_ebook_app/podo/feed.dart';
import 'package:flutter_ebook_app/podo/feed_detail.dart';
import 'package:geolocator/geolocator.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:xml2json/xml2json.dart';

class Api {

  /*
  static String baseURL =
      "https://catalog.feedbooks.com";
  static String popular =
      "http://localhost:5000/flutter_app_sample.json";
  static String recent =
      "https://catalog.feedbooks.com/publicdomain/browse/en/recent.atom";
  static String awards =
      "https://catalog.feedbooks.com/publicdomain/browse/en/awards.atom";
  static String noteworthy =
      "https://catalog.feedbooks.com/publicdomain/browse/en/homepage_selection.atom";
  static String shortStory =
      "https://catalog.feedbooks.com/publicdomain/browse/en/top.atom?cat=FBFIC029000";
  static String sciFi =
      "https://catalog.feedbooks.com/publicdomain/browse/en/top.atom?cat=FBFIC028000";
  static String actionAdventure =
      "https://catalog.feedbooks.com/publicdomain/browse/en/top.atom?cat=FBFIC002000";
  static String mystery =
      "https://catalog.feedbooks.com/publicdomain/browse/en/top.atom?cat=FBFIC022000";
  static String romance =
      "https://catalog.feedbooks.com/publicdomain/browse/en/top.atom?cat=FBFIC027000";
  static String horror =
      "https://catalog.feedbooks.com/publicdomain/browse/en/top.atom?cat=FBFIC015000";
  */

  static String baseURL = "http://www.binuv.com/pressifier";

  static String allArticle = "/article/all";

  static String articleBase = "/article";

  static String allArticleByCategory = "/category";

  static String detailArticle = "/";


  static String combine2Path(String firstPath, String secondPath) {
    return firstPath + secondPath;
  }

  static Position _lastPosition = Position();


  static Future<Feed> getAllAticles(String url,position) async {
    Dio dio = Dio();

    if(position as Position != null)
      _lastPosition = position;

    if(_lastPosition != null)
      url += "?latitute=" + _lastPosition.latitude.toString() + "&longitute=" +
          _lastPosition.longitude.toString();

    var res = await dio.get(url);
    print(url);
    Feed feed;
    if(res.statusCode == 200){

      var json = jsonDecode(res.toString());

      feed = Feed.fromJson(json);

    }else{
      throw("Error ${res.statusCode}");
    }
    print(feed);
    return feed;
  }


  static Future<List<FeedContent>> GetAllArticleByCate(String id) async {

    Dio dio = Dio();
    var url = combine2Path(baseURL, articleBase);
    

    /*
    if(_lastPosition != null)
      url += "?latitute=" + _lastPosition.latitude.toString() + "&longitute=" +
          _lastPosition.longitude.toString();
    */
    url += "?&categoryId=" + id;
    print(url);
    var res = await dio.get(url);

    List<FeedContent> feeds;

    if(res.statusCode == 200){
      feeds = new List<FeedContent>();
      var jsonE = jsonEncode(res.data);

      var json = jsonDecode(jsonE);
      print(json);
      json.forEach((t) {
        feeds.add(new FeedContent.fromJson(t));
      });

    }else{
      throw("Error ${res.statusCode}");
    }
    return feeds;
  }



  static Future<FeedDetail> getDetail(String id) async {

    Dio dio = Dio();
    String url = combine2Path(baseURL, articleBase) + "/" + id;
    var res = await dio.get(url);
    print(url);

    FeedDetail detail;
    if(res.statusCode == 200) {
      var json = jsonDecode(res.toString());

      detail = FeedDetail.fromJson(json);
    }else{
      throw("Error ${res.statusCode}");
    }

    return detail;


  }


}

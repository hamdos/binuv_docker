import 'package:flutter/foundation.dart';
import 'package:flutter_ebook_app/podo/feed.dart';
import 'package:flutter_ebook_app/util/api.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';

class HomeProvider with ChangeNotifier {
  String message;
  Feed top = Feed();
  Feed recent = Feed();
  bool loading = true;


  getFeeds(position) async{
    setLoading(true);
    Api.getAllAticles(Api.combine2Path(Api.baseURL, Api.allArticle), position).then((popular){
      setRecent(popular);
      setLoading(false);
    }).catchError((e){
      throw(e);
    });
  }


  void setLoading(value) {
    loading = value;
    notifyListeners();
  }

  bool isLoading() {
    return loading;
  }

  void setMessage(value) {
    message = value;
    Fluttertoast.showToast(
      msg: value,
      toastLength: Toast.LENGTH_SHORT,
      timeInSecForIos: 1,
    );
    notifyListeners();
  }

  String getMessage() {
    return message;
  }

  void setTop(value) {
    top = value;
    notifyListeners();
  }

  Feed getTop() {
    return top;
  }

  void setRecent(value) {
    recent = value;
    notifyListeners();
  }

  Feed getRecent() {
    return recent;
  }

}
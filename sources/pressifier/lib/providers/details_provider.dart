import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ebook_app/database/download_helper.dart';
import 'package:flutter_ebook_app/database/favorite_helper.dart';
import 'package:flutter_ebook_app/podo/feed.dart';
import 'package:flutter_ebook_app/podo/feed_detail.dart';
import 'package:flutter_ebook_app/util/api.dart';
import 'package:fluttertoast/fluttertoast.dart';

class DetailsProvider extends ChangeNotifier{
  String message;
  List<FeedContent> related = new List<FeedContent>();
  bool loading = true;
  FeedDetail entry;

  var favDB = FavoriteDB();

  bool faved = false;
  bool downloaded = false;

  static var httpClient = HttpClient();


  getFeed() async{
    setLoading(true);
    checkFav();
    related = entry.relatedContent;
    setLoading(false);
  }

  checkFav() async{
    /*
    List c = await favDB.check({"id": entry.published.t});
    if(c.isNotEmpty){
      setFaved(true);
    }else{
      setFaved(false);
    }
    */
  }

  addFav() async{
    /*
    await favDB.add({"id": entry.published.t, "item": entry.toJson()});
    checkFav();
     */
  }

  removeFav() async{
    /*
    favDB.remove({"id": entry.published.t}).then((v){
      print(v);
      checkFav();
    });
     */
  }


  void setLoading(value) {
    loading = value;
    notifyListeners();
  }

  bool isLoading() {
    return loading;
  }

  void setMessage(value) {
    message = value;
    Fluttertoast.showToast(
      msg: value,
      toastLength: Toast.LENGTH_SHORT,
      timeInSecForIos: 1,
    );
    notifyListeners();
  }

  String getMessage() {
    return message;
  }

  void setRelated(value) {
    related = value;
    notifyListeners();
  }

  List<FeedContent> getRelated() {
    return related;
  }

  void setEntry(value) {
    entry = value;
    notifyListeners();
  }

  void setFaved(value) {
    faved = value;
    notifyListeners();
  }

}
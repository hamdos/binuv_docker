package com.binuvcore.config;

import javax.servlet.*;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApiConfig {
    @Bean
    public Filter httpsEnforcerFilter() {
        return new HttpsEnforcer();
    }
}

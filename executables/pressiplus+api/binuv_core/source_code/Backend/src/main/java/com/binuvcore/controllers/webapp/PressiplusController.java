package com.binuvcore.controllers.webapp;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.config.keys.ConfigurationKeyServices;
import com.binuvcore.config.keys.ConfigurationMsgServices;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PressiplusController {

    @Autowired
    private UserServices userServices;

    @Autowired
    private ConfigurationKeyServices configurationKeyServices;

    @Autowired
    private ConfigurationMsgServices configurationMsgServices;

    @GetMapping("/pressiplus")
    public ModelAndView userPage(Model model) {


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());
        ParameterPasser parameterPasser = (ParameterPasser) model.asMap().get("parameterPasser");

        if (parameterPasser == null) {
            parameterPasser = new ParameterPasser();
        }

        ModelAndView mav = new ModelAndView();
        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());

            mav.addObject("user", user);

        } else {
            parameterPasser.setLogged_in(false);
            mav.addObject("user", new User());
        }

        if (configurationKeyServices.getConfigValue("connection.data.is.enabled").equals("false")) {
            parameterPasser.setMessage(configurationMsgServices.getConfigValue("connection.data.is.disabled"));
            parameterPasser.setError(true);
        }

        parameterPasser.setPageName("pressiplus");
        mav.addObject("parameterPasser", parameterPasser);
        mav.setViewName("pressiplus");
        return mav;
    }
}

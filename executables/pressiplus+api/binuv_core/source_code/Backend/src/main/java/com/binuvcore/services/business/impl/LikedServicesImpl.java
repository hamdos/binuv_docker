package com.binuvcore.services.business.impl;

import com.binuvcore.businessobjects.Liked;
import com.binuvcore.businessobjects.Article;
import com.binuvcore.businessobjects.access.User;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.repositories.LikedRepository;
import com.binuvcore.services.business.LikedServices;
import com.binuvcore.services.business.ArticleServices;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LikedServicesImpl implements LikedServices {

    @Autowired
    LikedRepository likedRepository;

    @Autowired
    UserServices userServices;

    @Autowired
    ArticleServices articleServices;

    private List<Liked> likedList = new ArrayList<>();

    @Override
    public Optional<Liked> getLikedById(Integer id) {
        return likedRepository.findById(id);
    }

    @Override
    public List<Liked> getLikedWithUserAndArticle() {

        List<Liked> list = new ArrayList<>();
        Iterable<Liked> likedIterable = likedRepository.findAll();
        likedIterable.forEach(list::add);
        return list;
    }

    @Override
    public List<Liked> getLikedByUser(Integer userId) {

        User user = userServices.getUserById(userId);
        return likedRepository.findLikedByUser(user);
    }

    @Override
    public List<Liked> getlikedByArticle(Integer articleId) {

        Article article = articleServices.getArticleById(articleId);
        return likedRepository.findLikedByArticle(article);
    }

    @Override
    public boolean addLikedWithUserIdAndArticleId(Integer userId, Integer articleId) {
        boolean response = false;
        User user = userServices.getUserById(userId);
        Article article = articleServices.getArticleById(articleId);

        long createdDate = DateTimeUtils.getCurrentDateInLong();
        if (user != null && article != null){
        	likedList = likedRepository.findLikedByUserAndArticle(user, article);
            if (likedList.size() == 0){
            	likedRepository.save(new Liked(createdDate, 0,0, false, user, article));
                response = true;
            }
        }
        return response;
    }

    @Override
    public List<Liked> getLikedByArticleAndUserId(Integer userId, Integer articleId) {

        User user = userServices.getUserById(userId);
        Article article = articleServices.getArticleById(articleId);
        if (user != null && article != null){
             return likedRepository.findLikedByUserAndArticle(user, article);
        } else {
            return likedList;
        }
    }


}

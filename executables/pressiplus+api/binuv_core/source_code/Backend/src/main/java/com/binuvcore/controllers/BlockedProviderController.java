package com.binuvcore.controllers;

import com.binuvcore.businessobjects.BlockedProvider;
import com.binuvcore.services.business.BlockedProviderServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Implementation class for {@link com.binuvcore.businessobjects.BlockedProvider}
 *
 * @author doston
 * @since 24.12.2019
 */
@RequestMapping("/blockedprovider")
@RestController
public class BlockedProviderController {

    @Autowired
    BlockedProviderServices blockedProviderServices;

    private List<BlockedProvider> blockedProviderList;

    @GetMapping(path = "/all")
    public ResponseEntity<List<BlockedProvider>> getAllBlockedProviders(){

        blockedProviderList = blockedProviderServices.getAllBlockedProviderWithUserAndProvider();
        if (blockedProviderList.size() > 0){
            return new ResponseEntity<>(blockedProviderList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/get-by-id")
    public ResponseEntity<BlockedProvider> getBlockedProviderById(@RequestParam Integer id){

        Optional<BlockedProvider> optionalBlockedProvider = blockedProviderServices.getBlockedProviderById(id);
        return optionalBlockedProvider.map(
                blockedProvider -> new ResponseEntity<>(blockedProvider, HttpStatus.OK)).orElseGet(
                        () -> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));

    }

    @GetMapping(path = "/get-by-user-id")
    public ResponseEntity<List<BlockedProvider>> getAllBlockedProvidersByUserId(@RequestParam Integer userId){

        blockedProviderList = blockedProviderServices.getBlockedProvidersByUser(userId);
        if (blockedProviderList.size() > 0){
            return new ResponseEntity<>(blockedProviderList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/get-by-provider-id")
    public ResponseEntity<List<BlockedProvider>> getAllBlockedProvidersByProviderId(@RequestParam Integer providerId){

        blockedProviderList = blockedProviderServices.getBlockedProvidersByProvider(providerId);
        if (blockedProviderList.size() > 0){
            return new ResponseEntity<>(blockedProviderList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/get-by-user-and-provider-ids")
    public ResponseEntity<List<BlockedProvider>> getAllBlockedProvidersByUserIdProviderId(@RequestParam Integer userId,
                                                                                          @RequestParam Integer providerId){

        blockedProviderList = blockedProviderServices.getBlockedProvidersByProviderIdAndUserId(userId, providerId);
        if (blockedProviderList.size() > 0){
            return new ResponseEntity<>(blockedProviderList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/add")
    public ResponseEntity<Boolean> addBlockedProvider(@RequestParam Integer userId, @RequestParam Integer providerId){

        return blockedProviderServices.addBlockedProviderWithUserIdAndProviderId(userId, providerId) ? new ResponseEntity<>(true, HttpStatus.OK) :
                new ResponseEntity<>(false, HttpStatus.NOT_IMPLEMENTED);
    }
}

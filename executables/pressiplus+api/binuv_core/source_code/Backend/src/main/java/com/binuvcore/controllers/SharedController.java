package com.binuvcore.controllers;

import com.binuvcore.businessobjects.Shared;
import com.binuvcore.services.business.SharedServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Implementation class for {@link com.binuvcore.businessobjects.Liked}
 *
 * @author kafle
 * @since 03.01.2020
 */
@RequestMapping("/shared")
@RestController
public class SharedController {

    @Autowired
    SharedServices sharedServices;

    private List<Shared> sharedList;

    @GetMapping(path = "/all")
    public ResponseEntity<List<Shared>> getAllShared(){

    	sharedList = sharedServices.getSharedWithUserAndArticle();
        if (sharedList.size() > 0){
            return new ResponseEntity<>(sharedList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/get-by-id")
    public ResponseEntity<Shared> getSharedById(@RequestParam Integer id){

        Optional<Shared> optionalShared = sharedServices.getSharedById(id);
        return optionalShared.map(
                liked -> new ResponseEntity<>(liked, HttpStatus.OK)).orElseGet(
                        () -> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));

    }

    @GetMapping(path = "/get-by-user-id")
    public ResponseEntity<List<Shared>> getAllSharedByUserId(@RequestParam Integer userId){

    	sharedList = sharedServices.getSharedByUser(userId);
        if (sharedList.size() > 0){
            return new ResponseEntity<>(sharedList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/get-by-article-id")
    public ResponseEntity<List<Shared>> getAllLikedByArticleId(@RequestParam Integer articleId){

    	sharedList = sharedServices.getSharedByArticle(articleId);
        if (sharedList.size() > 0){
            return new ResponseEntity<>(sharedList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/get-by-user-and-article-ids")
    public ResponseEntity<List<Shared>> getAllLikedByUserIdProviderId(@RequestParam Integer userId,
                                                                                          @RequestParam Integer articleId){

    	sharedList = sharedServices.getSharedByArticleAndUserId(userId, articleId);
        if (sharedList.size() > 0){
            return new ResponseEntity<>(sharedList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/add")
    public ResponseEntity<Boolean> addLiked(@RequestParam Integer userId, @RequestParam Integer articleId){

        return sharedServices.addSharedWithUserIdAndArticleId(userId, articleId) ? new ResponseEntity<>(true, HttpStatus.OK) :
                new ResponseEntity<>(false, HttpStatus.NOT_IMPLEMENTED);
    }
}

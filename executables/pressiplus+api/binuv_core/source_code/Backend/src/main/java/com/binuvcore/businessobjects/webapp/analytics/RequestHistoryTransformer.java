package com.binuvcore.businessobjects.webapp.analytics;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class RequestHistoryTransformer {

    @Getter
    @Setter
    private String requestUrl;

    @Getter
    @Setter
    private String ip_address;

    @Getter
    @Setter
    private String visite_dates;

    @Getter
    @Setter
    private String postcode;

    @Getter
    @Setter
    private String state;

    @Getter
    @Setter
    private String city;

    @Getter
    @Setter
    private String country;
}

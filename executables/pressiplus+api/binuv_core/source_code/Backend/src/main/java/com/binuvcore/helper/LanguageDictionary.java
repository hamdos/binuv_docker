package com.binuvcore.helper;

public enum LanguageDictionary {
    EN, DE, RU, UZ;

    public static boolean isValidLanguage(String lang) {
        return EN.name().equals(lang) || DE.name().equals(lang) || RU.name().equals(lang);
    }
}

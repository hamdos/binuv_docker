package com.binuvcore.helper.http;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.net.MalformedURLException;
import java.net.URL;


/**
 * Class where we keep some method to make operation with HTTP.
 *
 * @author doston, 18.12.2019
 */
public class HttpAndUrlHelper {

    //~ Constructors ---------------------------------------------------------------------------------------------------

    /**
     * Creates a new {@link HttpAndUrlHelper} object.
     */
    public HttpAndUrlHelper() {
    }

    //~ Methods --------------------------------------------------------------------------------------------------------

    /**
     * Get a default header JSON.
     *
     * @return header
     */
    public HttpHeaders getDefaultHeader() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }

    public static String baseUrl(final String referrer) {

        StringBuilder stringBuilder = new StringBuilder();
        try {
            URL url = new URL(referrer);
            stringBuilder.append(url.getProtocol());
            stringBuilder.append("://");
            stringBuilder.append(url.getHost());
            if (url.getPort() > 0) {
                stringBuilder.append(":");
                stringBuilder.append(url.getPort());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

//
//    public static void main(String[] args) {
//        System.out.println(baseUrl("http://binuv.com/app/passwordreset?token=a104fb12-e4ce-4c00-a008-74935df6c0e8"));
//    }
}

package com.binuvcore.services.webapp;

import com.binuvcore.businessobjects.webapp.ContactForm;
import com.binuvcore.businessobjects.webapp.ContactMail;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@ComponentScan(lazyInit = true)
public interface MailServiceBinuv {

    boolean sendEmailContactForm(ContactForm contactForm);
    void sendEmail(final String toEmail, final String subject, final String body);

}

package com.binuvcore.controllers.error;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EmailConfigurationController {

    @Autowired
    private UserServices userServices;

    @GetMapping(path = "/email-configuration-disabled")
    public ModelAndView displayEmailConfiguration() {
        ModelAndView modelAndView = new ModelAndView();
        ParameterPasser parameterPasser = new ParameterPasser();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        if (user != null) {
            parameterPasser.setLogged_in(true);
        } else {
            parameterPasser.setLogged_in(false);
        }

        parameterPasser.setSuccess(false);
        parameterPasser.setError(false);
        parameterPasser.setPageName("error");

        modelAndView.addObject("parameterPasser", parameterPasser);
        modelAndView.setViewName("/error/email-configuration");
        return modelAndView;
    }
}

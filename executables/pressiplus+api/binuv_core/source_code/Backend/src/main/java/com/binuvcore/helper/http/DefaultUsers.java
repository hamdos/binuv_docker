package com.binuvcore.helper.http;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.helper.date.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;

public class DefaultUsers {


    public static List<User> getStaticListOfPerson() {
        User personJpa1 = new User("", "doston", "Doston", "Ham", "doston@binuv.com", "$2a$10$rKs9lf936Y9IgIDOMEws4uJ35cBi6l71H0S70cfqtfK/AZ0TLao/y", DateTimeUtils.getCurrentDateInLong(),
                DateTimeUtils.getCurrentDateInLong(), 1, false, false, "ROLE_ADMIN"
        );
        List<User> personJpaList = new ArrayList<>();
        personJpaList.add(personJpa1);
        personJpaList.add(getStaticPerson());

        return personJpaList;
    }

    /**
     * Get a person
     *
     * @return a single person
     */
    public static User getStaticPerson() {
        User personJpa1 = new User("", "john", "Johnton", "Smith", "john@binuv.com", "$2a$10$rKs9lf936Y9IgIDOMEws4uJ35cBi6l71H0S70cfqtfK/AZ0TLao/y", DateTimeUtils.getCurrentDateInLong(),
                DateTimeUtils.getCurrentDateInLong(), 1, false, false, "ROLE_USER");

        return personJpa1;
    }
}

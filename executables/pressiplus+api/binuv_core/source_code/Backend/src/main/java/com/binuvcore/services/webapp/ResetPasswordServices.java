package com.binuvcore.services.webapp;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@ComponentScan(lazyInit = true)
public interface ResetPasswordServices {

    boolean resetPassword(final String tokenValue, final String newPassword);

    void requestPasswordReset(final String email, final String baseUrl);
}

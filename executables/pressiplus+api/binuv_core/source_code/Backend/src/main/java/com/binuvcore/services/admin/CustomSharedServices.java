package com.binuvcore.services.admin;

import com.binuvcore.businessobjects.custom.CustomSharedDto;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@ComponentScan(lazyInit = true)
public interface CustomSharedServices {

    void saveCustomSharedDtoForReport(CustomSharedDto customSharedDto);
    void updateCutomSharedDto(CustomSharedDto customSharedDto);

    CustomSharedDto getCustomSharedByUserIdAndProgressReport(Integer userId, boolean progressReport);
}

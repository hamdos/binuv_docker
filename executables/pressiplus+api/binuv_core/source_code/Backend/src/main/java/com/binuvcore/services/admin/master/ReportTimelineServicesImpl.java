package com.binuvcore.services.admin.master;

import com.binuvcore.businessobjects.webapp.master.ReportTimelineDto;
import com.binuvcore.businessobjects.webapp.master.ReportTimelineTransformer;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.repositories.master.ReportTimelineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportTimelineServicesImpl implements ReportTimelineServices {

    @Autowired
    private ReportTimelineRepository reportTimelineRepository;

    @Override
    public List<ReportTimelineTransformer> getReportTimelines() {

        List<ReportTimelineDto> reportTimelineDtos = new ArrayList<>();
        reportTimelineRepository.findAll().forEach(reportTimelineDtos::add);

        return timelineTransformers(reportTimelineDtos);
    }

    @Override
    public List<ReportTimelineDto> getReportTimelineDtosAsList() {
        List<ReportTimelineDto> reportTimelineDtos = new ArrayList<>();
        reportTimelineRepository.findAll().forEach(reportTimelineDtos::add);
        return reportTimelineDtos;
    }

    private List<ReportTimelineTransformer> timelineTransformers(final List<ReportTimelineDto> timelineDtoList) {

        timelineDtoList.sort(Comparator.comparing(ReportTimelineDto::getWeek).reversed());

        final List<ReportTimelineTransformer> reportTimelineTransformers = new ArrayList<>();

        ReportTimelineTransformer transformer;
        for (ReportTimelineDto reportTimline : timelineDtoList) {
            transformer = new ReportTimelineTransformer();
            transformer.setDay(reportTimline.getDay());
            transformer.setWeek(reportTimline.getWeek());
            transformer.setMonth(reportTimline.getMonth());
            transformer.setCreatedDate("Created at " + DateTimeUtils.convertLongDateToStringDate(reportTimline.getPublishedDate()));
            transformer.setUpdateDate("Updated at: " + DateTimeUtils.convertLongDateToStringDate(reportTimline.getUpdatedDate()));
            transformer.setImplemented(makeListFromString(reportTimline.getImplemented()));
            transformer.setPlannedToDo(makeListFromString(reportTimline.getPlannedToDo()));
            reportTimelineTransformers.add(transformer);
        }


        return reportTimelineTransformers;
    }

    private static List<String> makeListFromString(final String text) {

        String[] slitted = text.split("==");
        for (int i = 0; i < slitted.length; i++) {
            slitted[i] = slitted[i].trim();
        }
        List<String> stringList = Arrays.asList(slitted);
        return stringList.stream().filter(i -> !i.equals("")).collect(Collectors.toList());
    }

    private ReportTimelineTransformer generateStaticDate() {

        ReportTimelineTransformer reportTimelineDto = new ReportTimelineTransformer();
        List<String> implemented = new ArrayList<>();
        implemented.add("I have learned basic concepts of ElasticSearch");
        implemented.add("I have tried to build several index representation, where I have defined one object set as whole index and multiple object sets as one index.");
        implemented.add("I have started developing sketch for the work to simulate concepts which are defined in the research directions section of the Vision Document. It is need for me to demonstrate my solution in prototype and provide the solution for examined problems in the project.");
        implemented.add("Besides, I am developing one sample website where I will allocate all resources and research results (at the end) for public work and feedback. In that website, I will also try to visual my performance, and research process. I will provide an access to you once it is functional.");

        List<String> plannedToDo = new ArrayList<>();
        plannedToDo.add("I will measure current performance of ElasticSearch in the project");
        plannedToDo.add("Chemmedia have asked me to document all existing features of current ElasticSearch in their project, I will document it and I will share the document with you if it is against the confidentiality between me and chemmedia.");
        plannedToDo.add("Hopefully, I will finish and host the website which I am developing for visualisation of my progress and open-feedback platform to discuss research questions in the domain of Full-text search technologies.");
        plannedToDo.add("I will learn more about ElasticSearch and multi-tenancy to better understand the requirements and find relevant solutions.");

       reportTimelineDto.setImplemented(implemented);
       reportTimelineDto.setPlannedToDo(plannedToDo);
       reportTimelineDto.setWeek("Week 1");
       reportTimelineDto.setDay(22);
       reportTimelineDto.setMonth("March");
       return reportTimelineDto;
    }
}

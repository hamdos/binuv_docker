package com.binuvcore.businessobjects.transformers.ticket;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
public class TicketInDto {

    @Getter
    @Setter
    private String subject;

    @Getter
    @Setter
    private String message;
}

package com.binuvcore.businessobjects.webapp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClassifierDto {

    @Getter
    @Setter
    @SerializedName("auto_id")
    private int auto_id;

    @Getter
    @Setter
    @SerializedName("class_label")
    private String class_label;

    @Getter
    @Setter
    @SerializedName("rating")
    private double rating;
}

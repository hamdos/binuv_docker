package com.binuvcore.businessobjects.webapp;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ArticleRaterDto {

    @Getter
    @Setter
    private Integer value;

    @Getter
    @Setter
    private String category;

    @Getter
    @Setter
    private String section;

    @Getter
    @Setter
    private int image;

    @Getter
    @Setter
    private List<String> entities;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String subtitle;

    @Getter
    @Setter
    private String text;

    @Getter
    @Setter
    private String url;
}

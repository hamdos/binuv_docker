package com.binuvcore.repositories;

import com.binuvcore.businessobjects.pressifier.PressifierRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PressifierRequestResitory extends CrudRepository<PressifierRequest, Integer> {

    Iterable<PressifierRequest> findAllByDeleted(boolean deleted);


    @Override
    void deleteAll(Iterable<? extends PressifierRequest> iterable);
}

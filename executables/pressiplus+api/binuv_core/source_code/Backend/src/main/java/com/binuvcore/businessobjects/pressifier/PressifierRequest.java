package com.binuvcore.businessobjects.pressifier;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
@Table(name = "pressifier_requests", schema = "public")
public class PressifierRequest {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter@Column(name = "version")
    private String version;

    @Getter
    @Setter
    @Column(name = "all_articles")
    private Integer numOfAllArticles;

    @Getter
    @Setter
    @Column(name = "all_articles_lang")
    private Integer numOfAllArticlesWithLang;

    @Getter
    @Setter
    @Column(name = "all_articles_lang_location")
    private Integer numOfAllArticlesWithLangAndLocation;

    @Getter
    @Setter
    @Column(name = "all_articles_location")
    private Integer numOfAllArticlesWithLocation;

    @Getter
    @Setter
    @Column(name = "is_expired")
    private boolean deleted;
}

package com.binuvcore.services.business;

import com.binuvcore.businessobjects.Liked;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@ComponentScan(lazyInit = true)
public interface LikedServices {

    public Optional<Liked> getLikedById(Integer id);

    public List<Liked> getLikedWithUserAndArticle();
    public List<Liked> getLikedByUser(Integer userId);
    public List<Liked> getlikedByArticle(Integer articleId);
    public List<Liked> getLikedByArticleAndUserId(Integer userId, Integer articleId);

    public boolean addLikedWithUserIdAndArticleId(Integer userId, Integer articleId);

}

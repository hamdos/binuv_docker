package com.binuvcore.helper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class CategoryDto {

    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    private String name;
}

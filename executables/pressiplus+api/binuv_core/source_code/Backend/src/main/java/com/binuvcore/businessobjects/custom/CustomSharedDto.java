package com.binuvcore.businessobjects.custom;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity(name = "custom_shared")
@Table(name = "custom_shared", schema = "public")
public class CustomSharedDto {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "shared")
    private boolean shared;

    @Getter
    @Setter
    @Column(name = "user_id")
    private Integer userId;

    @Getter
    @Setter
    @Column(name = "token")
    private String token;

    @Getter
    @Setter
    @Column(name = "progress_report")
    private boolean progressReport;

    @Getter
    @Setter
    @Column(name = "shared_date")
    private long sharedDate;

    @Getter
    @Setter
    @Column(name = "deleted")
    private boolean deleted;
}

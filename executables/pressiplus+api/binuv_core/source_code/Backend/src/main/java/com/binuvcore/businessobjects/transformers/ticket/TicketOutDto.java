package com.binuvcore.businessobjects.transformers.ticket;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class TicketOutDto {

    @Getter
    @Setter
    private String subject;

    @Getter
    @Setter
    private String message;

    @Getter
    @Setter
    private String createdDate;

    @Getter
    @Setter
    private List<TicketContentOutDto> ticketContents;

    @Getter
    @Setter
    private String formAction;

    @Getter
    @Setter
    private TicketMenuActions ticketMenuActions;
}

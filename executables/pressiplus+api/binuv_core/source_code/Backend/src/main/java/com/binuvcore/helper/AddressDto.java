package com.binuvcore.helper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class AddressDto {
    @Getter
    @Setter
    private String road;

    @Getter
    @Setter
    private String village;

    @Getter
    @Setter
    private String state_district;

    @Getter
    @Setter
    private String state;
    @Getter
    @Setter
    private String postcode;

    @Getter
    @Setter
    private String country;

    @Getter
    @Setter
    private String country_code;
}

package com.binuvcore.services.business.impl;

import com.binuvcore.businessobjects.BlockedProvider;
import com.binuvcore.businessobjects.Provider;
import com.binuvcore.businessobjects.access.User;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.repositories.BlockedProviderRepository;
import com.binuvcore.services.business.BlockedProviderServices;
import com.binuvcore.services.business.ProviderServices;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BlockedProviderServicesImpl implements BlockedProviderServices {

    @Autowired
    BlockedProviderRepository blockedProviderRepository;

    @Autowired
    UserServices userServices;

    @Autowired
    ProviderServices providerServices;

    private List<BlockedProvider> blockedProviderList = new ArrayList<>();

    @Override
    public Optional<BlockedProvider> getBlockedProviderById(Integer id) {
        return blockedProviderRepository.findById(id);
    }

    @Override
    public List<BlockedProvider> getAllBlockedProviderWithUserAndProvider() {

        List<BlockedProvider> list = new ArrayList<>();
        Iterable<BlockedProvider> blockedProviderIterable = blockedProviderRepository.findAll();
        blockedProviderIterable.forEach(list::add);
        return list;
    }

    @Override
    public List<BlockedProvider> getBlockedProvidersByUser(Integer userId) {

        User user = userServices.getUserById(userId);
        return blockedProviderRepository.findBlockedProvidersByUser(user);
    }

    @Override
    public List<BlockedProvider> getBlockedProvidersByProvider(Integer providerId) {

        Provider provider = providerServices.getProviderById(providerId);
        return blockedProviderRepository.findBlockedProvidersByProvider(provider);
    }

    @Override
    public boolean addBlockedProviderWithUserIdAndProviderId(Integer userId, Integer providerId) {
        boolean response = false;
        User user = userServices.getUserById(userId);
        Provider provider = providerServices.getProviderById(providerId);

        long createdDate = DateTimeUtils.getCurrentDateInLong();
        if (user != null && provider != null){
            blockedProviderList = blockedProviderRepository.findBlockedProvidersByUserAndProvider(user, provider);
            if (blockedProviderList.size() == 0){
                blockedProviderRepository.save(new BlockedProvider(createdDate, 0, false, user, provider));
                response = true;
            }
        }
        return response;
    }

    @Override
    public List<BlockedProvider> getBlockedProvidersByProviderIdAndUserId(Integer userId, Integer providerId) {

//        blockedProviderList = blockedProviderRepository.findBlockedProvidersByProviderIdAndUserId(userId, providerId);
        User user = userServices.getUserById(userId);
        Provider provider = providerServices.getProviderById(providerId);
        if (user != null && provider != null){
             return blockedProviderRepository.findBlockedProvidersByUserAndProvider(user, provider);
        } else {
            return blockedProviderList;
        }
    }
}

package com.binuvcore.controllers;

import com.binuvcore.businessobjects.Article;
import com.binuvcore.helper.http.HttpAndUrlHelper;

import com.binuvcore.services.business.ArticleServices;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Controller class for {@link Article}
 *
 * @author kafle
 * @since 27.12.2019
 */
@RequestMapping("/article")
@RestController
public class ArticleController {

    //~ Instance fields ------------------------------------------------------------------------------------------------

    /**
     * Instance
     */
    @Autowired
    ArticleServices articleServices;

    /**
     * Instance
     */
    private HttpAndUrlHelper httpHelper = new HttpAndUrlHelper();

    //~ Methods --------------------------------------------------------------------------------------------------------

    @PostMapping(
                 path = "/add", consumes = {MediaType.APPLICATION_JSON_VALUE}
                )
    public ResponseEntity<Boolean> addArticle(@RequestBody Article article) {
        return articleServices.addArticle(article) ? new ResponseEntity<>(true, httpHelper.getDefaultHeader(),
                HttpStatus.OK) : new ResponseEntity<>(false, httpHelper.getDefaultHeader(),
                HttpStatus.NOT_IMPLEMENTED);

    }

    @PostMapping(
            path = "/add/bulk", consumes = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<String> addArticles(@RequestBody List<Article> articles) {
        int addedArticles = 0;
        if (articles == null || articles.size() == 0) {
            return new ResponseEntity<>("Empty articles cannot be added. Please provide articles", HttpStatus.NOT_IMPLEMENTED);
        }
        for (Article article : articles) {
            if (articleServices.addArticle(article)) {
                addedArticles += 1;
            }
        }
        String message = addedArticles + " of "+ articles.size() + " given articles are added";
        return new ResponseEntity<>(message, HttpStatus.OK);

    }

    @RequestMapping(path = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<Article>> getAllArticle() {
        return new ResponseEntity<>(articleServices.getAllArticle(), HttpStatus.OK);
    }
    
    
    
    /**
     * Get article by id
     *
     * @return response for Front-end
     */
    
//    @RequestMapping(path = "/article-by-id", method = RequestMethod.GET)
//    public ResponseEntity<Article> getArticleById(@RequestParam Integer id){
//        return new ResponseEntity<>(articleServices.getArticleById(id), HttpStatus.OK);
//    }
    
    @GetMapping(path = "/article-by-id")
    public ResponseEntity<Article> getArticleById(@RequestParam Integer id){
        return new ResponseEntity<>(articleServices.getArticleById(id), HttpStatus.OK);
    }
    /**
     * Get metadata of article by url
     *
     * @return response for Front-end
     */
    
    @RequestMapping(path = "/article-metadata", method = RequestMethod.GET)
    public ResponseEntity<String> getArticleMetaDatabyUrl(@RequestParam String url){
    	
        return new ResponseEntity<>(articleServices.getArticleMetaDatabyUrl(url), HttpStatus.OK);
    }

}

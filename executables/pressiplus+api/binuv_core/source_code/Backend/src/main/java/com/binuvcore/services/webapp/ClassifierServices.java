package com.binuvcore.services.webapp;

import com.binuvcore.businessobjects.webapp.ArticleRaterDto;
import com.binuvcore.businessobjects.webapp.ClassifierDto;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan(lazyInit = true)
public interface ClassifierServices {

    ClassifierDto classifyRequest(ArticleRaterDto articleRaterDto);
    List<ClassifierDto> classifyRequest(List<ArticleRaterDto> articleRaterDtoList);
}

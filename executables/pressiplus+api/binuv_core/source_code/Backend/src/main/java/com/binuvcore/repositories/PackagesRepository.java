package com.binuvcore.repositories;

import com.binuvcore.businessobjects.Packages;
import com.binuvcore.businessobjects.access.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PackagesRepository extends CrudRepository<Packages, Integer> {

    List<Packages> findPackagesByUserPackage(User user);
}

package com.binuvcore.services.webapp.ticket;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.businessobjects.transformers.ticket.TicketOutDto;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan(lazyInit = true)
public interface TicketServices {

    public TicketOutDto getTicketById(Integer ticketId, User loggedUser);

    public List<TicketOutDto> allTicketsByUser(User loggedUser);
    public List<TicketOutDto> allPublicTickets();
}

package com.binuvcore.services.business;

import com.binuvcore.businessobjects.Packages;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Implementation class for {@link com.binuvcore.businessobjects.Provider}.
 *
 * @author doston
 * @since  25.12.2019
 */
@Component
@ComponentScan(lazyInit = true)
public interface PackagesServices {

    //~ Methods --------------------------------------------------------------------------------------------------------

    /**
     * Add a package by user id and category id
     *
     * @param  userId     given user id
     * @param  categoryId given category id
     *
     * @return response
     */
    public boolean addPackageByUserIdAndCategoryId(Integer userId, Integer categoryId);

    /**
     * Get all packages by user id
     *
     * @param  userId given user id
     *
     * @return response
     */
    public List<Packages> getAllPackagesByUserId(Integer userId);

    /**
     * Get all packages with theirs users and categories
     *
     * @return response
     */
    public List<Packages> getAlPackages();
}

package com.binuvcore.controllers.error;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.config.keys.ConfigurationMsgServices;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.helper.http.HttpAndUrlHelper;
import com.binuvcore.services.admin.RequestHistoryServices;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ErrorPageController implements ErrorController {

    @Autowired
    private ConfigurationMsgServices configurationMsgServices;

    @Autowired
    private UserServices userServices;

    @Autowired
    private RequestHistoryServices requestHistoryServices;

    private final static String baseUrl = "http:localhost:8080";

    @RequestMapping("/error")
    public ModelAndView error(Model model, HttpServletRequest request) {

        //        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());

        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        ParameterPasser parameterPasser = (ParameterPasser) model.asMap().get("parameterPasser");
        if (parameterPasser == null) {
            parameterPasser = new ParameterPasser();
        }

        if (user != null) {
            parameterPasser.setLogged_in(true);
            modelAndView.addObject("user", user);
        } else {
            parameterPasser.setLogged_in(false);
        }

        parameterPasser.setSuccess(false);
        parameterPasser.setError(false);
        parameterPasser.setPageName("error");

        modelAndView.addObject("parameterPasser", parameterPasser);

        modelAndView.addObject("baseUrl", HttpAndUrlHelper.baseUrl(request.getRequestURL().toString()));
        modelAndView.setViewName("error/error");
        return modelAndView;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}

package com.binuvcore.businessobjects;


import com.binuvcore.businessobjects.access.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity(name = "blocked_provider")
@Table(name = "blocked_provider", schema = "public")
public class BlockedProvider implements Serializable {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "created_date")
    private long createdDate;

    @Getter
    @Setter
    @Column(name = "deleted_date")
    private long deletedDate;

    @Getter
    @Setter
    @Column(name = "deleted", columnDefinition = "BOOLEAN")
    private boolean deleted;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "provider_id", nullable = false)
    private Provider provider;

    public BlockedProvider(long createdDate, long deletedDate, boolean deleted, User user,
                           Provider provider) {
        this.createdDate = createdDate;
        this.deletedDate = deletedDate;
        this.deleted = deleted;
        this.user = user;
        this.provider = provider;
    }

}

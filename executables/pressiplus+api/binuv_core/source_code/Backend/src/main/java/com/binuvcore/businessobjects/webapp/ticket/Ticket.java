package com.binuvcore.businessobjects.webapp.ticket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity(name = "ticket_support")
@Table(name = "ticket_support", schema = "public")
public class Ticket {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "title")
    private String title;

    @Getter
    @Setter
    @Column(name = "created_date")
    private long createdDate;

    @Getter
    @Setter
    @Column(name = "closed", columnDefinition = "BOOLEAN")
    private boolean closed;

    @Getter
    @Setter
    @Column(name = "deleted", columnDefinition = "BOOLEAN")
    private boolean deleted;

    @Getter
    @Setter
    @Column(name = "owner_id")
    private Integer ownerId;

    @Getter
    @Setter
    @Column(name = "public_ticket", columnDefinition = "BOOLEAN")
    private boolean publicTicket;
}

package com.binuvcore.services.webapp.impl;

import com.binuvcore.businessobjects.access.Token;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.helper.StringHelper;
import com.binuvcore.repositories.TokenRepository;
import com.binuvcore.services.webapp.TokenServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenServicesImpl implements TokenServices {

    @Autowired
    private TokenRepository tokenRepository;

    private StringHelper stringHelper = new StringHelper();

    @Override
    public String generateNewToken(final String email, final boolean isPasswordReset) {

        final String uniqueToken = stringHelper.getRandomString();

        Token newToken = new Token();
        newToken.setCreatedDate(DateTimeUtils.getCurrentDateInLong());
        newToken.setEmail(email);
        newToken.setValid(true);
        newToken.setValue(uniqueToken);
        newToken.setPassword_reset(isPasswordReset);
        tokenRepository.save(newToken);
        return newToken.getValue();
    }

    @Override
    public boolean validateToken(final String value) {

        Token token = tokenRepository.findByValue(value);
        if (token == null) {
            return false;
        }

        return token.isValid();
    }
}

package com.binuvcore.businessobjects.webapp;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ArticlePublisherResultDto {

    @Getter
    @Setter
    private String article_unique_id;

    @Getter
    @Setter
    private String class_label;

    @Getter
    @Setter
    private double rating;

    @Getter
    @Setter
    private List<String> good_words;

    @Getter
    @Setter
    private List<String> bad_words;
}

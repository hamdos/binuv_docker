package com.binuvcore.businessobjects.pressifier;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class AuthorDtoTransformer {

    @Getter
    @Setter
    private String public_id;

    @Getter
    @Setter
    private String fullname;

    @Getter
    @Setter
    private String firstName;

    @Getter
    @Setter
    private String lastName;

    @Getter
    @Setter
    private String image_url;
}

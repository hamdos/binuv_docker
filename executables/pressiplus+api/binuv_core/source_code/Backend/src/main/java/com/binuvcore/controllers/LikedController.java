package com.binuvcore.controllers;

import com.binuvcore.businessobjects.Liked;
import com.binuvcore.services.business.LikedServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Implementation class for {@link com.binuvcore.businessobjects.Liked}
 *
 * @author kafle
 * @since 03.01.2020
 */
@RequestMapping("/liked")
@RestController
public class LikedController {

    @Autowired
    LikedServices likedServices;

    private List<Liked> likedList;

    @GetMapping(path = "/all")
    public ResponseEntity<List<Liked>> getAllLiked(){

    	likedList = likedServices.getLikedWithUserAndArticle();
        if (likedList.size() > 0){
            return new ResponseEntity<>(likedList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/get-by-id")
    public ResponseEntity<Liked> getLikedById(@RequestParam Integer id){

        Optional<Liked> optionalLiked = likedServices.getLikedById(id);
        return optionalLiked.map(
                liked -> new ResponseEntity<>(liked, HttpStatus.OK)).orElseGet(
                        () -> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));

    }

    @GetMapping(path = "/get-by-user-id")
    public ResponseEntity<List<Liked>> getAllLikedByUserId(@RequestParam Integer userId){

    	likedList = likedServices.getLikedByUser(userId);
        if (likedList.size() > 0){
            return new ResponseEntity<>(likedList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/get-by-article-id")
    public ResponseEntity<List<Liked>> getAllLikedByArticleId(@RequestParam Integer articleId){

    	likedList = likedServices.getlikedByArticle(articleId);
        if (likedList.size() > 0){
            return new ResponseEntity<>(likedList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/get-by-user-and-article-ids")
    public ResponseEntity<List<Liked>> getAllLikedByUserIdProviderId(@RequestParam Integer userId,
                                                                                          @RequestParam Integer articleId){

    	likedList = likedServices.getLikedByArticleAndUserId(userId, articleId);
        if (likedList.size() > 0){
            return new ResponseEntity<>(likedList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/add")
    public ResponseEntity<Boolean> addLiked(@RequestParam Integer userId, @RequestParam Integer articleId){

        return likedServices.addLikedWithUserIdAndArticleId(userId, articleId) ? new ResponseEntity<>(true, HttpStatus.OK) :
                new ResponseEntity<>(false, HttpStatus.NOT_IMPLEMENTED);
    }
}

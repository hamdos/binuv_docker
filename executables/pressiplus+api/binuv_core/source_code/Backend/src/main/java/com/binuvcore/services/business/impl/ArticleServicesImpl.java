package com.binuvcore.services.business.impl;

import com.binuvcore.businessobjects.Article;
import com.binuvcore.businessobjects.Provider;
import com.binuvcore.helper.CategoryDto;
import com.binuvcore.helper.ConstantCategoryList;
import com.binuvcore.helper.RandomValueGeneratorHelper;
import com.binuvcore.helper.StringHelper;
import com.binuvcore.repositories.ArticleRepository;
import com.binuvcore.repositories.ProviderRepository;
import com.binuvcore.services.business.ArticleServices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Collector;
import org.jsoup.select.Evaluator;

import javax.transaction.Transactional;

@Service
public class ArticleServicesImpl implements ArticleServices {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ProviderRepository providerRepository;

    private StringHelper stringHelper = new StringHelper();

    private ConstantCategoryList constantCategoryList = ConstantCategoryList.getInstance();

    private Logger logger = LoggerFactory.getLogger(ArticleServicesImpl.class);

    @Override
    public Article getArticleById(Integer id) {

        Optional<Article> optionalArticle = articleRepository.findById(id);

        return optionalArticle.orElse(null);
    }

    @Override
    public List<Article> getAllArticle() {
        List<Article> articleArrayList = new ArrayList<>();
        Iterable<Article> articleIterable = articleRepository.findAll();
        articleIterable.forEach(articleArrayList::add);

        return articleArrayList;
    }

    @Override
    public List<Article> getAllArticleByProviderId(Integer provider_id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Article> getAllArticleByCategoryId(Integer category_id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Article> getAllArticleByProviderAndCategory(Integer provider_id, Integer category_id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    @Transactional
    public boolean addArticle(final Article article) {

        Provider provider;
        if (article.getProvider_id() != null && providerRepository.findById(article.getProvider_id()).isPresent()) {
            provider = providerRepository.findById(article.getProvider_id()).get();
            article.setProvider_id(provider.getId());
        } else {
            if (providerRepository.findByName("FreiePresse") != null) {
                article.setProvider_id(providerRepository.findByName("FreiePresse").getId());
            } else {
                provider = new Provider();
                provider.setName("FreiePresse");
                provider.setAddress("Chemnitz, Saxony, Germany");
                provider.setApiUrl("https://www.freiepresse.de/");
                provider.setApiKey("");
                provider.setLogoUrl("https://www.freiepresse.de/img/logo.png");
                providerRepository.save(provider);
                article.setProvider_id(providerRepository.findByName("FreiePresse").getId());
            }
        }
        article.setCategory(updatedCategory(article));
        articleRepository.save(article);

        return articleRepository.findAllByTitleContains(article.getTitle()) != null;
    }

    @Override
    public List<Article> getAllByTitleContains(String title) {
        return articleRepository.findAllByTitleContains(title);
    }

//    private boolean saveArticle(final Article article) {
////		List<Article> articleList = articleRepository.findArticleByUrl(article.getUrl());
////		if (articleList == null || articleList.isEmpty()){
//        article.setCategory(article.getCategory());
//        article.setProvider(article.getProvider());
//        article.setImportedDate(new DateAndTimeHelper().getCurrentDateInLong());
//        article.setLastUpdatedDate(new DateAndTimeHelper().getCurrentDateInLong());
//        articleRepository.save(article);
//        return true;
////		}
////		return false;
//    }

    @Override
    public String getArticleMetaDatabyUrl(String url) {
        String URL = url;

        try {
            Document document = Jsoup.connect(URL).get();
            List<String> tags = new ArrayList<String>();
            System.out.println("Number of tags by getAllElements() method =" + document.getAllElements().size());
            System.out.println("Number of tags by Collector.collect() method =" + Collector.collect(new Evaluator.AllElements(), document).size());
            System.out.println("Number of tags by select(\"*\") method =" + document.select("*").size());
            for (Element e : document.getAllElements()) {
                tags.add(e.tagName().toLowerCase());
            }
            System.out.println("The tags = " + tags);
            System.out.println("Distinct tags = " + new HashSet<String>(tags));
        } catch (IOException e) {
            logger.error("Get ArticleMetaDataByUrl in Article Service is causing error");
            e.printStackTrace();
        }

        return url;
    }

    private String updatedCategory(final Article article) {
        String categoryName = null;

        if (!stringHelper.isWhiteSpaceOrNull(article.getCategory())) {
            categoryName = article.getCategory().toUpperCase();

            List<CategoryDto> categoryList = constantCategoryList.getCategoryList();
            for (CategoryDto categoryDto: categoryList) {
                if (categoryName.equals(categoryDto.getName())) {
                    break;
                } else {
                    categoryName =categoryList.get(
                            RandomValueGeneratorHelper.generareRandomInt(categoryList.size())).getName();
                }
            }
        }
        return categoryName;
    }
}

package com.binuvcore.repositories;

import com.binuvcore.businessobjects.access.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * PersonService Implementation of {@link User}.
 *
 * @author doston
 * @since  12.12.2019
 */
@Repository
@Transactional
public interface UserRepository extends CrudRepository<User, Integer> {

    //~ Methods --------------------------------------------------------------------------------------------------------

    /**
     * Get all undeleted users
     *
     * @return list of users
     */
    public List<User> findUsersByDeletedFalse();

    /**
     * Get all deleted users
     *
     * @return list of users
     */
    public List<User> findUsersByDeletedTrue();

    /**
     * Get a user who have this email
     *
     * @param  email TODO DOCUMENT ME!
     *
     * @return list of users
     */
    User findUserByEmail(String email);

    /**
     * Get all users those have this first name
     *
     * @param  firstName given name
     *
     * @return list of users
     */
    public List<User> findUsersByFirstName(String firstName);

    User findUserByEmailAndPassword(String email, String password);
}

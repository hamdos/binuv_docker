package com.binuvcore.services.business;

import com.binuvcore.businessobjects.Shared;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@ComponentScan(lazyInit = true)
public interface SharedServices {

    public Optional<Shared> getSharedById(Integer id);

    public List<Shared> getSharedWithUserAndArticle();
    public List<Shared> getSharedByUser(Integer userId);
    public List<Shared> getSharedByArticle(Integer articleId);
    public List<Shared> getSharedByArticleAndUserId(Integer userId, Integer articleId);

    public boolean addSharedWithUserIdAndArticleId(Integer userId, Integer articleId);

}

package com.binuvcore.controllers.security_access;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.config.keys.ConfigurationMsgServices;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.services.business.UserServices;
import com.binuvcore.services.admin.RequestHistoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController {

    @Autowired
    private UserServices userServices;

    @Autowired
    private ConfigurationMsgServices configurationMsgServices;

    @Autowired
    private RequestHistoryServices requestHistoryServices;

    @GetMapping("/app/login")
    public ModelAndView login(Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) {

        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("security/login");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        if (auth.isAuthenticated() && !request.getSession(false).isNew() && user != null) {
            redirectAttrs.addFlashAttribute("loggedInMessage",
                    configurationMsgServices.getConfigValue("login.user.logged.already"));
            modelAndView.setViewName("redirect:/start");
            return modelAndView;
        }

        ParameterPasser parameterPasser = (ParameterPasser) model.asMap().get("parameterPasser");

        if (parameterPasser != null) {
            modelAndView.addObject("parameterPasser", parameterPasser);
            return modelAndView;
        }

        parameterPasser = new ParameterPasser();
        parameterPasser.setSuccess(false);
        parameterPasser.setError(false);
        parameterPasser.setLogged_in(false);
        modelAndView.addObject("parameterPasser", parameterPasser);

        return modelAndView;
    }

    @GetMapping("/app/login-error")
    public ModelAndView loginError(HttpServletRequest request) {
        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());

        ModelAndView mav = new ModelAndView();
        ParameterPasser parameterPasser = new ParameterPasser();
        parameterPasser.setError(true);
        parameterPasser.setMessage(configurationMsgServices.getConfigValue("login.error"));
        mav.addObject("parameterPasser", parameterPasser);
        mav.setViewName("security/login");
        return mav;
    }



}

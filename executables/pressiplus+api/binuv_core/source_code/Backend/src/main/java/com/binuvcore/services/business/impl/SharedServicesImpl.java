package com.binuvcore.services.business.impl;

import com.binuvcore.businessobjects.Shared;
import com.binuvcore.businessobjects.Article;
import com.binuvcore.businessobjects.access.User;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.repositories.SharedRepository;
import com.binuvcore.services.business.SharedServices;
import com.binuvcore.services.business.ArticleServices;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SharedServicesImpl implements SharedServices {

    @Autowired
    SharedRepository sharedRepository;

    @Autowired
    UserServices userServices;

    @Autowired
    ArticleServices articleServices;

    private List<Shared> sharedList = new ArrayList<>();

    @Override
    public Optional<Shared> getSharedById(Integer id) {
        return sharedRepository.findById(id);
    }

    @Override
    public List<Shared> getSharedWithUserAndArticle() {

        List<Shared> list = new ArrayList<>();
        Iterable<Shared> sharedIterable = sharedRepository.findAll();
        sharedIterable.forEach(list::add);
        return list;
    }

    @Override
    public List<Shared> getSharedByUser(Integer userId) {

        User user = userServices.getUserById(userId);
        return sharedRepository.findSharedByUser(user);
    }

    @Override
    public List<Shared> getSharedByArticle(Integer articleId) {

        Article article = articleServices.getArticleById(articleId);
        return sharedRepository.findSharedByArticle(article);
    }

    @Override
    public boolean addSharedWithUserIdAndArticleId(Integer userId, Integer articleId) {
        boolean response = false;
        User user = userServices.getUserById(userId);
        Article article = articleServices.getArticleById(articleId);

        long createdDate = DateTimeUtils.getCurrentDateInLong();
        if (user != null && article != null){
        	sharedList = sharedRepository.findSharedByUserAndArticle(user, article);
            if (sharedList.size() == 0){
            	sharedRepository.save(new Shared(createdDate, false, user, article));
                response = true;
            }
        }
        return response;
    }

    @Override
    public List<Shared> getSharedByArticleAndUserId(Integer userId, Integer articleId) {

        User user = userServices.getUserById(userId);
        Article article = articleServices.getArticleById(articleId);
        if (user != null && article != null){
             return sharedRepository.findSharedByUserAndArticle(user, article);
        } else {
            return sharedList;
        }
    }


}

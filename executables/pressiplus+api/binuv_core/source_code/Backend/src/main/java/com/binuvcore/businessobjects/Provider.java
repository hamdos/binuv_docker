package com.binuvcore.businessobjects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity(name = "provider")
@Table(name = "provider", schema = "public")
public class Provider implements Serializable {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "name")
    private String name;

    @Getter
    @Setter
    @Column(name = "address")
    private String address;

    @Getter
    @Setter
    @Column(name = "api_key")
    private String apiKey;

    @Getter
    @Setter
    @Column(name = "api_url")
    private String apiUrl;

    @Getter
    @Setter
    @Column(name = "created_date")
    private long createdDate;

    @Getter
    @Setter
    @Column(name = "deleted", columnDefinition = "BOOLEAN")
    private boolean deleted;

    @Getter
    @Setter
    @Column(name = "deleted_date")
    private long deletedDate;

    @Getter
    @Setter
    @Column(name = "logo_url")
    private String logoUrl;

    public Provider(String name, String address, String apiKey, String apiUrl, long createdDate,
                    boolean deleted, long deletedDate) {
        this.name = name;
        this.address = address;
        this.apiKey = apiKey;
        this.apiUrl = apiUrl;
        this.createdDate = createdDate;
        this.deleted = deleted;
        this.deletedDate = deletedDate;
    }
}

package com.binuvcore.businessobjects;


import com.binuvcore.businessobjects.access.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity(name = "liked")
@Table(name = "liked", schema = "public")
public class Liked implements Serializable {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "liked_date")
    private long likedDate;

    @Getter
    @Setter
    @Column(name = "disliked")
    private long disliked;

    @Getter
    @Setter
    @Column(name = "disliked_date")
    private long dislikedDate;

    @Getter
    @Setter
    @Column(name = "deleted", columnDefinition = "BOOLEAN")
    private boolean deleted;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "article_id", nullable = false)
    private Article article;

    public Liked(long likedDate, long disliked, long dislikedDate, boolean deleted, User user,
                 Article article) {
        this.likedDate = likedDate;
        this.disliked = disliked;
        this.dislikedDate = dislikedDate;
        this.deleted = deleted;
        this.user = user;
        this.article = article;
    }
}

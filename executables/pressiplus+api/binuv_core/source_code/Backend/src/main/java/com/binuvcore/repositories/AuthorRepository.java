package com.binuvcore.repositories;

import com.binuvcore.businessobjects.Author;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends CrudRepository<Author, Integer> {

    Author findByFirstNameAndLastNameAndImageUrl(String firstName, String lastName, String imageUrl);

}

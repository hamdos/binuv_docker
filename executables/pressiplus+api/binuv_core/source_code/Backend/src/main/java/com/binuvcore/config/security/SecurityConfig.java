package com.binuvcore.config.security;

import com.binuvcore.businessobjects.custom.CustomAccessDeniedHandler;
import com.binuvcore.businessobjects.custom.MyLogoutSuccessHandler;
import com.binuvcore.businessobjects.custom.MySimpleUrlAuthenticationSuccessHandler;
import com.binuvcore.config.MyAppUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled=true)
@Order(1)
public class SecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

    @Autowired
    private MyAppUserDetailsService myAppUserDetailsService;

    @Autowired
    private MyLogoutSuccessHandler myLogoutSuccessHandler;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS);

        http.authorizeRequests().antMatchers(HttpMethod.GET, "/registration", "/js/**", "/css/**",
                "/img/**" ,"/pressiplus", "/public/**", "/index", "/", "/start", "/article" ,"/publish", "/app/login", "/app/login-error",
                "/app/master-thesis/report", "/email-configuration-disabled").permitAll();
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/article/add").permitAll();
        http.authorizeRequests()
                .antMatchers("/app/secure/**", "/app/support/**", "/user/**", "/app/master-thesis/**", "/app/user-profile").hasAnyRole("ADMIN","USER")
                .antMatchers("/admin/**").hasAnyRole("ADMIN")
                .and()
                .formLogin()  //login configuration
                    .loginPage("/app/login")
                    .failureUrl("/app/login-error")
                    .loginProcessingUrl("/app/login")
                    .usernameParameter("app_email")
                    .passwordParameter("app_password")
                    .successHandler(myAuthenticationSuccessHandler())
                .and()
                .logout()    //logout configuration
                .logoutUrl("/app/logout")
                .logoutSuccessHandler(myLogoutSuccessHandler)
                .and().exceptionHandling() //exception handling configuration
                .accessDeniedHandler(accessDeniedHandler());
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        auth.userDetailsService(myAppUserDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/app/login").setViewName("security/login");
        registry.addViewController("/app/passwordreset").setViewName("/security/passwordreset");
        registry.addViewController("/app/signup").setViewName("security/signup");
        registry.addViewController("/start").setViewName("home/ticket/all-tickets");
        registry.addViewController("/email-configuration-disabled").setViewName("error/email-configuration");
        registry.addViewController("/pressiplus").setViewName("pressiplus");
    }

    @Bean
    public AuthenticationSuccessHandler myAuthenticationSuccessHandler(){
        return new MySimpleUrlAuthenticationSuccessHandler();
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler(){
        return new CustomAccessDeniedHandler();
    }
}

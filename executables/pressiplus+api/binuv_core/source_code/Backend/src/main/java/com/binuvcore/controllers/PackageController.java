package com.binuvcore.controllers;

import com.binuvcore.businessobjects.Packages;

import com.binuvcore.services.business.PackagesServices;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Controller class for {@link Packages}.
 *
 * @author doston
 * @since  24.12.2019
 */
@RequestMapping("/package")
@RestController
public class PackageController {

    //~ Instance fields ------------------------------------------------------------------------------------------------

    /**
     * Services {@link PackagesServices}
     */
    @Autowired
    private PackagesServices packagesServices;

    //~ Methods --------------------------------------------------------------------------------------------------------

    /**
     * Add a packages
     *
     * @param  userId     given user id
     * @param  categoryId given category id
     *
     * @return TODO DOCUMENT ME!
     */
    @GetMapping(path = "/add-package")
    public ResponseEntity<Boolean> addPackage(@RequestParam Integer userId, @RequestParam Integer categoryId) {

        boolean response = packagesServices.addPackageByUserIdAndCategoryId(userId, categoryId);
        if (response){
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(response, HttpStatus.NOT_IMPLEMENTED);
        }

    }

    /**
     * Get all packages with their category and user
     *
     * @return response
     */
    @GetMapping(path = "/all")
    public ResponseEntity<List<Packages>> getAllPackages() {
        return new ResponseEntity<>(packagesServices.getAlPackages(), HttpStatus.OK);
    }

    /**
     * Get packages by user id
     *
     * @param  userId given
     *
     * @return response
     */
    @GetMapping(path = "/get-by-user-id")
    public ResponseEntity<List<Packages>> getPackagesByUserId(@RequestParam Integer userId) {
        List<Packages> list = packagesServices.getAllPackagesByUserId(userId);

        if (list.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}

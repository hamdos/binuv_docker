package com.binuvcore.services.password.impl;

import com.binuvcore.services.password.PasswordServices;
import org.springframework.stereotype.Service;

@Service
public class PasswordServiceImpl implements PasswordServices {

    private MessageDigestEncryption messageDigestEncryption = new MessageDigestEncryption();

    // TODO -  put these keys into .properties file
    private final String SALT_FOR_PASSWORD = "binuv34@der!serf";
    private final String HASH_ALGORITHM = "SHA-256";

    @Override
    public String encryptPlaintext(String plaintext) {
        messageDigestEncryption.prepareMessageDigest(HASH_ALGORITHM);
        return messageDigestEncryption.encrypt(saltPlaintext(plaintext));
    }

    @Override
    public boolean verify(String userPassPlaintext, String encryptedString) {
        return encryptedString.equals(saltPlaintext((userPassPlaintext)));
    }

    @Override
    public String saltPlaintext(String plaintext) {
        return plaintext.concat(SALT_FOR_PASSWORD);
    }
}

package com.binuvcore.repositories.custom;

import com.binuvcore.businessobjects.custom.CustomSharedDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomSharedDtoRepository extends CrudRepository<CustomSharedDto, Integer> {

    CustomSharedDto findByUserIdAndProgressReport(Integer userId, boolean progressReport);
}

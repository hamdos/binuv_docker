package com.binuvcore.repositories;

import com.binuvcore.businessobjects.Liked;
import com.binuvcore.businessobjects.Article;
import com.binuvcore.businessobjects.access.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LikedRepository extends CrudRepository<Liked, Integer> {

    List<Liked> findLikedByUser(User user);
    List<Liked> findLikedByArticle(Article article);
    List<Liked> findLikedByUserAndArticle(User user, Article article);


}

package com.binuvcore.services.data_processor;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CustomJsonParser {
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("/Users/dosha/Uni/binuv-core/binuv-data-processor/data/output.json"))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONArray employeeList = (JSONArray) obj;

            List<Results> list = new ArrayList<>();
//
            for (int i = 0; i < employeeList.size(); i++) {
                Results results = new Results();
                JSONObject jsonObject = (JSONObject) employeeList.get(i);
                results.setClass_label("class_label");
                results.setRating("rating");
                list.add(results);
            }

            for (int i = 0; i < list.size(); i++) {

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}

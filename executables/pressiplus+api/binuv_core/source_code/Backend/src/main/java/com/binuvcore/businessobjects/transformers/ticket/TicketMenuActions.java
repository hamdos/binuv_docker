package com.binuvcore.businessobjects.transformers.ticket;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class TicketMenuActions {

    @Getter
    @Setter
    private String deleteTicket;

    @Getter
    @Setter
    private String labelPrivate;

    @Getter
    @Setter
    private String labelPublic;

    @Getter
    @Setter
    private String labelClosed;

    @Getter
    @Setter
    private String labelOpen;

    @Getter
    @Setter
    private boolean closed;

    @Getter
    @Setter
    private boolean publicTicket;
}

package com.binuvcore.services.business.impl;

import com.binuvcore.businessobjects.Packages;
import com.binuvcore.repositories.PackagesRepository;
import com.binuvcore.services.business.PackagesServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PackagesServicesImpl implements PackagesServices {

    @Autowired
    PackagesRepository packagesRepository;

    @Override
    public boolean addPackageByUserIdAndCategoryId(Integer userId, Integer categoryId) {
        return false;
    }

    @Override
    public List<Packages> getAllPackagesByUserId(Integer userId) {
        return null;
    }

    @Override
    public List<Packages> getAlPackages() {
        return null;
    }
}

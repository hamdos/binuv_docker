package com.binuvcore.businessobjects.webapp.ticket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity(name = "ticket_support_content")
@Table(name = "ticket_support_content", schema = "public")
public class TicketContent {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "message")
    private String message;

    @Getter
    @Setter
    @Column(name = "created_date")
    private long createdDate;

    @Getter
    @Setter
    @Column(name = "ticket_support_id")
    private Integer ticketId;

    @Getter
    @Setter
    @Column(name = "deleted", columnDefinition = "BOOLEAN")
    private boolean deleted;

    @Getter
    @Setter
    @Column(name = "user_id")
    private Integer userId;

    @Getter
    @Setter
    @Column(name = "owner", columnDefinition = "BOOLEAN")
    private boolean owner;

}

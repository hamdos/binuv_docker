package com.binuvcore.controllers.admin;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.businessobjects.transformers.ContactFormTransformer;
import com.binuvcore.businessobjects.webapp.ContactForm;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.repositories.ContactFormRepository;
import com.binuvcore.services.business.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Controller
public class ContactControllerAdmin {

    @Autowired
    private UserServices userServices;

    @Autowired
    private ContactFormRepository contactFormRepository;

    @GetMapping("/admin/history/contact-requests")
    public ModelAndView requests(){
        ModelAndView modelAndView = new ModelAndView();
        ParameterPasser parameterPasser = new ParameterPasser();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());

        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
        } else {
            parameterPasser.setLogged_in(false);
        }


        parameterPasser.setError(false);
        parameterPasser.setSuccess(false);
        parameterPasser.setPageName("admin");
        modelAndView.addObject("parameterPasser", parameterPasser);
        modelAndView.addObject("contactFormList", getAllContactForms());
        modelAndView.setViewName("admin/contact-form-history");
        return modelAndView;
    }

    private List<ContactFormTransformer> getAllContactForms() {
        List<ContactForm> contactFormList = new ArrayList<>();
        List<ContactFormTransformer> transformers = new ArrayList<>();
        contactFormRepository.findAll().forEach(contactFormList::add);

        contactFormList.sort(Comparator.comparing(ContactForm::getId).reversed());

        for (ContactForm contactForm :
                contactFormList) {
            final String date = DateTimeUtils.convertLongDateToStringDateShortFormat(contactForm.getCreatedDate());
            ContactFormTransformer contactFormTransformer = new ContactFormTransformer(contactForm.getName(), contactForm.getEmail(), contactForm.getMessage(), date);
            transformers.add(contactFormTransformer);
        }
        return transformers;
    }
}

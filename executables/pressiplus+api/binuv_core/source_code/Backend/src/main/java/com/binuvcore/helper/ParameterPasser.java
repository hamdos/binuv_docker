package com.binuvcore.helper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class ParameterPasser {

    @Getter
    @Setter
    private boolean passwordResetRequest;

    @Getter
    @Setter
    private boolean passwordResetValidate;

    @Getter
    @Setter
    private String token;

    @Getter
    @Setter
    private boolean success;

    @Getter
    @Setter
    private boolean error;

    @Getter
    @Setter
    private String message;

    @Getter
    @Setter
    private boolean logged_in;

    @Getter
    @Setter
    private String logged_user_email;

    @Getter
    @Setter
    private String user_role;

    @Getter
    @Setter
    private String pageName;
}

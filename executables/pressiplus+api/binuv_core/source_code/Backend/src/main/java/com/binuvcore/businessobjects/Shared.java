package com.binuvcore.businessobjects;


import com.binuvcore.businessobjects.access.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity(name = "shared")
@Table(name = "shared", schema = "public")
public class Shared implements Serializable {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "shared_date")
    private long sharedDate;

    @Getter
    @Setter
    @Column(name = "deleted", columnDefinition = "BOOLEAN")
    private boolean deleted;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "article_id", nullable = false)
    private Article article;

    public Shared(long sharedDate, boolean deleted, User user,
                  Article article) {
        this.sharedDate = sharedDate;
        this.deleted = deleted;
        this.user = user;
        this.article = article;
    }
}

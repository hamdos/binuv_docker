package com.binuvcore.config.keys;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@ComponentScan(lazyInit = true)
@PropertySource("classpath:config-files/config_keys.properties")
public class ConfigurationKeyServices {

    @Autowired
    private Environment env;

    public String getConfigValue(String configKey){
        return env.getProperty(configKey);
    }

}

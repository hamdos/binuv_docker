package com.binuvcore.controllers.webapp;

import javax.servlet.http.HttpServletResponse;

import com.binuvcore.businessobjects.webapp.ContactForm;
import com.binuvcore.businessobjects.webapp.ContactMail;
import com.binuvcore.config.keys.ConfigurationKeyServices;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.services.webapp.MailServiceBinuv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(path = "/public")
public class ContactFormController {

    @Autowired
    private MailServiceBinuv mailServiceBinuv;

    @Autowired
    private ConfigurationKeyServices configurationKeyServices;

    private Logger logger = LoggerFactory.getLogger(ContactFormController.class);

    @RequestMapping(value = "/contact", method = RequestMethod.POST)
    public String handleContactForm(ContactMail contactMail, HttpServletResponse httpResponse) {
        ContactForm contactForm = null;
        try {
            contactForm = createContactFormObject(contactMail);
            if (configurationKeyServices.getConfigValue("email.configuration.enabled").equals("true")){
                boolean delivered = mailServiceBinuv.sendEmailContactForm(contactForm);
                contactForm.setDelivered(delivered);
                httpResponse.sendRedirect("/");
                return "redirect:index";
            } else {
                return "redirect:/email-configuration-disabled";
            }
        } catch (Exception ex) {
            logger.error("Exception in contact form: " + ex.getMessage());
            contactForm.setDelivered(false);
        }
        return "redirect:index";
    }

    private ContactForm createContactFormObject(ContactMail contactMail){
        ContactForm contactForm = new ContactForm();
        contactForm.setCreatedDate(DateTimeUtils.getCurrentDateInLong());
        contactForm.setEmail(contactMail.getEmail());
        contactForm.setMessage(contactMail.getMessage());
        contactForm.setName(contactMail.getName());
        return contactForm;
    }
}

package com.binuvcore.businessobjects.webapp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
@Table(name = "published_article", schema = "public")
public class ArticlePublisher {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "title")
    private String title;

    @Getter
    @Setter
    @Column(name = "content")
    private String content;

    @Getter
    @Setter
    @Column(name = "article_unique_id")
    private String articleUniqueId;

    @Getter
    @Setter
    @Column(name = "last_updated_date")
    private long lastUpdatedDate;

    @Getter
    @Setter
    @Column(name = "created_date")
    private long createdDate;

    @Getter
    @Setter
    @Column(name = "tags")
    private String tags;
}

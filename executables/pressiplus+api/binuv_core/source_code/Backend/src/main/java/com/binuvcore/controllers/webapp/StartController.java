package com.binuvcore.controllers.webapp;

import com.binuvcore.businessobjects.access.User;
import com.binuvcore.businessobjects.transformers.ticket.TicketInDto;
import com.binuvcore.helper.ParameterPasser;
import com.binuvcore.services.business.UserServices;
import com.binuvcore.services.admin.RequestHistoryServices;
import com.binuvcore.services.webapp.ticket.TicketServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class StartController {

    @Autowired
    private RequestHistoryServices requestHistoryServices;

    @Autowired
    private UserServices userServices;

    @Autowired
    private TicketServices ticketServices;

    @RequestMapping(value = {"/start"}, method = RequestMethod.GET)
    public ModelAndView home(HttpServletRequest request, Model model) {

        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());

        // getting redirected messages
        ParameterPasser parameterPasser = (ParameterPasser) model.asMap().get("parameterPasser");
        if (parameterPasser == null) {
            parameterPasser = new ParameterPasser();
        }

        ModelAndView modelAndView = new ModelAndView();

        //Logged in message
        String loggedInMessage = (String) model.asMap().get("loggedInMessage");
        if (loggedInMessage != null) {

            modelAndView.addObject("loggedInMessage", loggedInMessage);
        } else {
            modelAndView.addObject("loggedInMessage", null);
        }

        // user authentication:
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userServices.getUserByEmail(auth.getName());
        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
            modelAndView.addObject("userName", user.getFirstName() + " " + user.getLastName());

        } else {
            parameterPasser.setLogged_in(false);
        }

        if (user != null) {
            parameterPasser.setLogged_in(true);
            parameterPasser.setLogged_user_email(user.getEmail());
            parameterPasser.setUser_role(user.getRole());
            modelAndView.addObject("userName", user.getFirstName() + " " + user.getLastName());
            modelAndView.addObject("allTickets", ticketServices.allTicketsByUser(user));
        } else {
            parameterPasser.setLogged_in(false);
            modelAndView.addObject("allTickets", ticketServices.allPublicTickets());
        }


        parameterPasser.setPageName("start");
        modelAndView.addObject("parameterPasser", parameterPasser);
        modelAndView.addObject("ticketInputObject", new TicketInDto());



        modelAndView.addObject("formAction",  "/app/publish-support-ticket-post?ticketId=0" );
        modelAndView.setViewName("home/ticket/all-tickets");
        return modelAndView;
    }
}

package com.binuvcore.helper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
public class LocationDto {

    @Getter
    @Setter
    private String place_id;

    @Getter
    @Setter
    private String licence;

    @Getter
    @Setter
    private String osm_type;

    @Getter
    @Setter
    private String osm_id;

    @Getter
    @Setter
    private String lat;

    @Getter
    @Setter
    private String lon;

    @Getter
    @Setter
    private String place_rank;

    @Getter
    @Setter
    private String category;

    @Getter
    @Setter
    private String type;

    @Getter
    @Setter
    private String importance;

    @Getter
    @Setter
    private String addresstype;

    @Getter
    @Setter
    private String display_name;

    @Getter
    @Setter
    private List<String> boundingbox;

    @Getter
    @Setter
    private AddressDto address;
}

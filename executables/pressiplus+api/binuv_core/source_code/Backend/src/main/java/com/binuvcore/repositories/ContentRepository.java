package com.binuvcore.repositories;

import com.binuvcore.businessobjects.webapp.ContentDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContentRepository extends CrudRepository<ContentDto, Integer> {

    List<ContentDto> findAllByArticleUniqueId(String article_unique_id);
}

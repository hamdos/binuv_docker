package com.binuvcore.services.webapp.impl;

import com.binuvcore.businessobjects.webapp.analytics.ESubscription;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.helper.StringHelper;
import com.binuvcore.repositories.ESubscriptionRepository;
import com.binuvcore.services.webapp.ESubscriptionServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ESubscriptionServicesImpl implements ESubscriptionServices {

    @Autowired
    private ESubscriptionRepository eSubscriptionRepository;

    private StringHelper stringHelper = new StringHelper();

    @Override
    public void subscribe(String email) {
        if (!stringHelper.isWhiteSpaceOrNull(email) && !eSubscriptionRepository.existsByEmail(email)) {
            ESubscription eSubscription = new ESubscription();
            eSubscription.setEmail(email);
            eSubscription.setSubscribed_date(DateTimeUtils.getCurrentDateInLong());
            eSubscription.setUnsubscribed(false);
            eSubscriptionRepository.save(eSubscription);
        }
    }

    @Override
    public Iterable<ESubscription> getAllSubscription() {
        return eSubscriptionRepository.findAll();
    }
}

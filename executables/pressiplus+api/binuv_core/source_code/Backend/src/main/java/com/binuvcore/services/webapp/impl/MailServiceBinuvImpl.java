package com.binuvcore.services.webapp.impl;

import com.binuvcore.businessobjects.webapp.ContactForm;
import com.binuvcore.config.keys.ConfigurationKeyServices;
import com.binuvcore.config.keys.ConfigurationMsgServices;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.helper.StringHelper;
import com.binuvcore.services.business.UserServices;
import com.binuvcore.services.webapp.MailServiceBinuv;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Date;
import java.util.Properties;

@Service
public class MailServiceBinuvImpl implements MailServiceBinuv {

    private StringHelper stringHelper = new StringHelper();

    @Autowired
    private UserServices userServices;

    @Autowired
    private ConfigurationMsgServices configurationMsgServices;

    @Autowired
    private ConfigurationKeyServices configurationKeyServices;

    @Getter
    @Setter
    private boolean response;

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Override
    public boolean sendEmailContactForm(ContactForm contactForm) {
        if (stringHelper.isWhiteSpaceOrNull(contactForm.getEmail()) ||
                stringHelper.isWhiteSpaceOrNull(contactForm.getMessage())) {
            return false;
        }

        final String subject = configurationMsgServices.getConfigValue("contact.form.email.subject");
        final String body = configurationMsgServices.getConfigValue("contact.form.email.body");
        sendEmailServices(contactForm.getEmail(), subject, body);

        // send email to info@binuv.com
        sendEmailServices(configurationKeyServices.getConfigValue("contact.form.receiver.email"), "New request from Contact Form", prepareEmailBodyToBINUV(contactForm));

        return response;
    }

    @Override
    public void sendEmail(final String toEmail, final String subject, final String body) {
        sendEmailServices(toEmail, subject, body);

    }

    private String prepareEmailBodyToBINUV(final ContactForm contactForm) {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Dear BINUV team, <br> <br> yu have a request from one client. Please check the content of the message below: <br><br>From: ");
        stringBuilder.append(contactForm.getName());
        stringBuilder.append("<br>Email: ");
        stringBuilder.append(contactForm.getEmail());
        stringBuilder.append("<br>Message: ");
        stringBuilder.append(contactForm.getMessage());
        stringBuilder.append("<br>Phone Number: ");
        stringBuilder.append(contactForm.getPhone());
        stringBuilder.append("<br>Date: ");
        stringBuilder.append(DateTimeUtils.getCurrentDateInString());
        return stringBuilder.toString();
    }

    private void sendEmailServices(final String toEmail, final String subject, final String body) {
        Properties props = new Properties();

        props.put("mail.smtp.auth", configurationKeyServices.getConfigValue("mail.smtp.auth"));
        props.put("mail.smtp.starttls.enable", configurationKeyServices.getConfigValue("mail.smtp.starttls.enable"));
        props.put("mail.smtp.ssl.enable", configurationKeyServices.getConfigValue("mail.smtp.ssl.enable"));
        props.put("mail.smtp.host", configurationKeyServices.getConfigValue("mail.smtp.host"));
        props.put("mail.smtp.port", configurationKeyServices.getConfigValue("mail.smtp.port"));

        final String senderEmailAddress = configurationKeyServices.getConfigValue("mail.email.address");
        final String senderEmailPassword = configurationKeyServices.getConfigValue("mail.email.password");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderEmailAddress, senderEmailPassword);
            }
        });
        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(senderEmailAddress, false));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
            msg.setSubject(subject);
            msg.setContent(subject, "text/html");
            msg.setSentDate(new Date());
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(body, "text/html");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
//        MimeBodyPart attachPart = new MimeBodyPart();
//
//        attachPart.attachFile("/Users/dosha/Uni/Backend/src/main/resources/static/img/background1.jpg");
//        multipart.addBodyPart(attachPart);
            response = true;
            msg.setContent(multipart);
            Transport.send(msg);
        } catch (MessagingException e) {
            logger.error("Cannot send email. The reason: " + e);
            response = false;
        }
    }

}

package com.binuvcore.helper;


import java.util.ArrayList;
import java.util.List;

public class ConstantCategoryList {

    private final List<CategoryDto> categoryList = new ArrayList<CategoryDto>();

    private static  ConstantCategoryList constantCategoryList = null;
    private ConstantCategoryList() {
        categoryList.add(new CategoryDto(1, "TECHNOLOGY"));
        categoryList.add(new CategoryDto(2, "SCIENCE"));
        categoryList.add(new CategoryDto(3, "SPORT"));
        categoryList.add(new CategoryDto(4, "MAGAZINES"));
        categoryList.add(new CategoryDto(5, "POLITICS"));
        categoryList.add(new CategoryDto(6, "MEDIA"));
        categoryList.add(new CategoryDto(7, "WEATHER"));
        categoryList.add(new CategoryDto(8, "WEBLOGS"));
        categoryList.add(new CategoryDto(9, "BUSINESS"));
        categoryList.add(new CategoryDto(10, "SOCIETY"));
        categoryList.add(new CategoryDto(11, "ENVIRONMENT"));
        categoryList.add(new CategoryDto(12, "LIFESTYLE"));
        categoryList.add(new CategoryDto(13, "BREAKING NEWS"));
        categoryList.add(new CategoryDto(14, "EDUCATION"));
    }


    public List<CategoryDto> getCategoryList() {
        return categoryList;
    }

    public static ConstantCategoryList getInstance() {
        if (constantCategoryList == null) {
            constantCategoryList = new ConstantCategoryList();
        }
        return constantCategoryList;
    }
}

package com.binuvcore.controllers;

import com.binuvcore.businessobjects.Subscription;
import com.binuvcore.services.business.SubscriptionServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Implementation class for {@link com.binuvcore.businessobjects.BlockedProvider}
 *
 * @author doston
 * @since 24.12.2019
 */
@RequestMapping("/subscription")
@RestController
public class SubscriptionController {

    @Autowired
    SubscriptionServices subscriptionServices;

    @GetMapping(path = "/add-subscription-by-user-and-provider-ids")
    public ResponseEntity<String> addSubscription(@RequestParam Integer userId, @RequestParam Integer providerId){
        return subscriptionServices.addSubscriptionByUserIdAndProviderId(userId, providerId) ? new ResponseEntity<>("added", HttpStatus.OK)
                : new ResponseEntity<>("not added", HttpStatus.NOT_IMPLEMENTED);
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<Subscription>> getAll(){
        return new ResponseEntity<>(subscriptionServices.getAllSubscriptions(), HttpStatus.OK);
    }

    @GetMapping(path = "/get-by-user-id")
    public ResponseEntity<List<Subscription>> getSubscriptionsByUserId(@RequestParam Integer userId){
        List<Subscription> subscriptions = subscriptionServices.getSubscriptionsByUserId(userId);
        if (subscriptions.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(subscriptions, HttpStatus.OK);
    }

    @PutMapping(path = "/unsubscribe-by-user-and-provider-ids")
    public ResponseEntity<Boolean> unsubscribeUserFromProvider(@RequestParam Integer userId,
                                                               @RequestParam Integer providerId){
        return subscriptionServices.unsubscribeUserFromProvider(userId, providerId) ?
                new ResponseEntity<>(true, HttpStatus.OK) :
                new ResponseEntity<>(false, HttpStatus.NOT_IMPLEMENTED);
    }
}

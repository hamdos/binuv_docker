package com.binuvcore.businessobjects.webapp.analytics;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity(name = "email_subscription")
@Table(name = "email_subscription", schema = "public")
public class ESubscription {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "email")
    private String email;

    @Getter
    @Setter
    @Column(name = "unsubscribed")
    private boolean unsubscribed;

    @Getter
    @Setter
    @Column(name = "subscribed_date")
    private long subscribed_date;
}

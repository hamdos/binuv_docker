package com.binuvcore.services.business;

import com.binuvcore.businessobjects.Subscription;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan(lazyInit = true)
public interface SubscriptionServices {

    public boolean addSubscriptionByUserIdAndProviderId(Integer userId, Integer providerId);
    public List<Subscription> getAllSubscriptions();
    Subscription getSubscriptionById(Integer id);
    public List<Subscription> getSubscriptionsByUserId(Integer userId);
    public List<Subscription> getSubscriptionsByProviderId(Integer providerId);
    boolean unsubscribeUserFromProvider(Integer userId, Integer providerId);
}

package com.binuvcore.businessobjects;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity(name = "article")
@Table(name = "article", schema = "public")
public class Article implements Serializable {

    @Getter
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Getter
    @Setter
    @Column(name = "value")
    private Integer value;

    @Getter
    @Setter
    @Column(name = "category")
    private String category;

    @Getter
    @Setter
    @Column(name = "section")
    private String section;

    @Getter
    @Setter
    @Column(name = "title")
    private String title;

    @Getter
    @Setter
    @Column(name = "subtitle")
    private String subtitle;

    @Getter
    @Setter
    @Column(name = "description")
    private String description;

    @Getter
    @Setter
    @Column(name = "url")
    private String url;

    @Getter
    @Setter
    @Column(name = "image")
    private Integer image;

    @Getter
    @Setter
    @Column(name = "image_url")
    private String image_url;

    @Getter
    @Setter
    @Column(name = "entities")
    private String entities;

    @Getter
    @Setter
    @Column(name = "imported_date")
    private long importedDate;

    @Getter
    @Setter
    @Column(name = "last_updated_date")
    private long lastUpdatedDate;

    @Getter
    @Setter
    @Column(name = "deleted", columnDefinition = "BOOLEAN")
    private boolean deleted;

    @Getter
    @Setter
    @Column(name = "class_label")
    private String class_label;

    @Getter
    @Setter
    @Column(name = "rating")
    private double rating;

    @Getter
    @Setter
    @Column(name = "language")
    private String language;

    @Getter
    @Setter
    @Column(name = "location")
    private String location;

    @Getter
    @Setter
    @Column(name = "thumbnail_url")
    private String thumbnailUrl;

    @Getter
    @Setter
    @Column(name = "short_description")
    private String shortDescription;

    @Getter
    @Setter
    @Column(name = "category_id")
    private Integer category_id;

    @Getter
    @Setter
    @Column(name = "provider_id")
    private Integer provider_id;

    @Getter
    @Setter
    @Column(name = "author_id")
    private Integer author_id;

    public Article(Integer value, String category, String title, String subtitle,String description, String section,
                   String url, Integer image, String image_url, String entities,
                   long importedDate, long lastUpdatedDate, boolean deleted, Integer categoryObject,
                   Integer provider) {
        this.value = value;
        this.category = category;
        this.section = section;
        this.title = title;
        this.subtitle = subtitle;
        this.description = description;
        this.url = url;
        this.image = image;
        this.image_url = image_url;
        this.entities = entities;
        this.importedDate = importedDate;
        this.lastUpdatedDate = lastUpdatedDate;
        this.deleted = deleted;
        this.category_id = categoryObject;
        this.provider_id = provider;
    }
}

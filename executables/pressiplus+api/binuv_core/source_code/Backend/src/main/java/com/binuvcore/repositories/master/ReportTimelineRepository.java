package com.binuvcore.repositories.master;

import com.binuvcore.businessobjects.webapp.master.ReportTimelineDto;
import org.springframework.data.repository.CrudRepository;

public interface ReportTimelineRepository extends CrudRepository<ReportTimelineDto, Integer> {

    ReportTimelineDto findByWeekAndUserId(String week, Integer userId);

}

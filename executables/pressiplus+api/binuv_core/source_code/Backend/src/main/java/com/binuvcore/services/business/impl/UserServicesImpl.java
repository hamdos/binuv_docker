package com.binuvcore.services.business.impl;

import com.binuvcore.businessobjects.access.User;

import com.binuvcore.helper.date.DateTimeUtils;

import com.binuvcore.helper.StringHelper;
import com.binuvcore.repositories.UserRepository;

import com.binuvcore.services.business.UserServices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * PersonService Implementation of {@link UserServices}.
 *
 * @author doston
 * @since  24.12.2019
 */
@Service
public class UserServicesImpl implements UserServices {

    //~ Instance fields ------------------------------------------------------------------------------------------------

    private final StringHelper stringHelper = new StringHelper();

    private Logger logger = LoggerFactory.getLogger(UserServicesImpl.class);

    /**
     * See more info {@link UserRepository}.
     */
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    //~ Methods --------------------------------------------------------------------------------------------------------

    /**
     * Adding a person to database.
     *
     * @param  user given parameter
     *
     * @return some text
     */
    @Override
    public boolean addUser(User user) {

        if (stringHelper.isWhiteSpaceOrNull(user.getEmail())){
            return false;
        }

        User foundUser = userRepository.findUserByEmail(user.getEmail());

        if (foundUser == null|| !foundUser.getEmail().equals(user.getEmail())) {
            user.setCreatedDate(DateTimeUtils.getCurrentDateInLong());
            user.setLastUpdatedDate(DateTimeUtils.getCurrentDateInLong());
            user.setDeleted(false);
            user.setPasswordChangeRequired(false);
            user.setLoginAttemptCount(1);
            user.setPublicId(stringHelper.getRandomString());
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            user.setRole(getRole(user.getEmail()));
            userRepository.save(user);
            return true;
        } else if (foundUser.getEmail().equals(user.getEmail()) && foundUser.isDeleted()) {
            foundUser.setLastUpdatedDate(DateTimeUtils.getCurrentDateInLong());
            foundUser.setDeleted(false);
            foundUser.setFirstName(user.getFirstName());
            foundUser.setLastName(user.getLastName());
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            user.setRole(getRole(user.getEmail()));
            userRepository.save(foundUser);
            return true;
        } else {
            return false;
        }

    }

    /**
     * Get all deleted users
     *
     * @return list of users
     */
    @Override
    public List<User> getAllDeletedUsers() {
        return userRepository.findUsersByDeletedTrue();
    }

    /**
     * Get all undeleted users
     *
     * @return list of users
     */
    @Override
    public List<User> getAllUnDeletedUsers() {
        return userRepository.findUsersByDeletedFalse();
    }

    /**
     * Get all users from db.
     *
     * @return list of users
     */
    @Override
    public List<User> getAllUsers() {
        List<User> userArrayList = new ArrayList<>();
        Iterable<User> userIterable = userRepository.findAll();
        userIterable.forEach(userArrayList::add);

        return userArrayList;
    }

    /**
     * Get all users those have this first name
     *
     * @param  firstName given first name
     *
     * @return list of users
     */
    @Override
    public List<User> getAllUsersByThisFirstName(String firstName) {
        return userRepository.findUsersByFirstName(firstName);
    }

    /**
     * Find a user by a given email.
     *
     * @param  email a given email
     *
     * @return found user
     */
    @Override
    public User getUserByEmail(String email) {
        User user = userRepository.findUserByEmail(email);
        return user;
    }

    /**
     * Find a user by id.
     *
     * @param  personId a given id
     *
     * @return a found user
     */
    @Override
    public User getUserById(Integer personId) {
        Optional<User> optionalUser = userRepository.findById(personId);

        return optionalUser.orElse(null);
    }

    @Override
    public User loginUser(User user) {
        if (stringHelper.isWhiteSpaceOrNull(user.getEmail()) || stringHelper.isWhiteSpaceOrNull(user.getPassword())) {
            return null;
        }

        User foundUser = userRepository.findUserByEmailAndPassword(user.getEmail(), user.getPassword());

        if (foundUser == null || foundUser.isDeleted()){
            return null;
        }
        return foundUser;
    }

    @Override
    public boolean deleteUserById(Integer id) {
        boolean res = false;
        if (id == null){
            return false;
        }
        User user = userRepository.findById(id).get();

        if (user != null && !user.isDeleted()){

            user.setDeleted(true);
            user.setLastUpdatedDate(DateTimeUtils.getCurrentDateInLong());
            userRepository.save(user);
            res = true;
        }

        User ss = userRepository.findUserByEmail(user.getEmail());
        return res;
    }

    @Override
    public void updateUserRole(User user) {
        Optional<User> optionalUser = userRepository.findById(user.getId());
        if (optionalUser.isPresent()) {
            User useFoundr = optionalUser.get();
            useFoundr.setRole(user.getRole());
            userRepository.save(useFoundr);
        }
    }

    private String getRole(final String email) {
        if (email.contains("@binuv.com")) {
            return "ROLE_ADMIN";
        } else {
            return "ROLE_USER";
        }
    }
}

package com.binuvcore.controllers.webapp;

import com.binuvcore.businessobjects.webapp.ArticleRaterDto;
import com.binuvcore.businessobjects.webapp.ClassifierDto;
import com.binuvcore.helper.StringHelper;
import com.binuvcore.services.webapp.ClassifierServices;
import com.binuvcore.services.admin.RequestHistoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
@RequestMapping(path = "/data-processor")
public class ClassifierExternalController {

    @Autowired
    private ClassifierServices classifierServices;

    @Autowired
    private RequestHistoryServices requestHistoryServices;

    private static final String BINUV_KEY = "53e45489-945a-4a4c-b685-b682066f0960";
    private static final String ERROR_MSG = "HTTP Headers are not valid! Please make sure that you are sending valid BINUV-KEY and correct Content-Type.";
    private static final String ERROR_SERVER_MSG = "Our Servers are having internal problems. Please, send the request later.";

    private final StringHelper stringHelper = new StringHelper();

    @CrossOrigin
    @RequestMapping(value = "/rate",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity createProducts(
            @RequestHeader(name = "Content-Type", required = true) String contentType,
            @RequestHeader(name = "BINUV-KEY", required = true) String binuvKey,
            @RequestBody ArticleRaterDto articleRaterDto,
            HttpServletRequest request) {

        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());
        if (stringHelper.isWhiteSpaceOrNull(contentType) || stringHelper.isWhiteSpaceOrNull(binuvKey)) {
            return new ResponseEntity<>(new CustomException(ERROR_MSG), HttpStatus.BAD_REQUEST);
        }

        ClassifierDto classifierDto = classifierServices.classifyRequest(articleRaterDto);
        if (classifierDto != null) {
            return new ResponseEntity<>(classifierDto, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new CustomException(ERROR_SERVER_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rate-bulk",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity createProductsEde(
            @RequestHeader(name = "Content-Type", required = true) String contentType,
            @RequestHeader(name = "BINUV-KEY", required = true) String binuvKey,
            @RequestBody List<ArticleRaterDto> articleRaterDtoList,
            HttpServletRequest request) {


        requestHistoryServices.addRequestHistory(request.getRemoteAddr(), request.getRequestURL().toString());

        if (stringHelper.isWhiteSpaceOrNull(contentType) || stringHelper.isWhiteSpaceOrNull(binuvKey)) {
            return new ResponseEntity<>(new CustomException(ERROR_MSG), HttpStatus.BAD_REQUEST);
        }

        List<ClassifierDto> classifierDtoList = classifierServices.classifyRequest(articleRaterDtoList);
        if (classifierDtoList != null) {
            return new ResponseEntity<>(classifierDtoList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new CustomException(ERROR_SERVER_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    class CustomException {

        private String message;

        public CustomException(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}

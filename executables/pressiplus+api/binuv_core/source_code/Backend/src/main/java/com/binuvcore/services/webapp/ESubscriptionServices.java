package com.binuvcore.services.webapp;

import com.binuvcore.businessobjects.webapp.analytics.ESubscription;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@ComponentScan(lazyInit = true)
public interface ESubscriptionServices {

    void subscribe(String email);

    Iterable<ESubscription> getAllSubscription();
}

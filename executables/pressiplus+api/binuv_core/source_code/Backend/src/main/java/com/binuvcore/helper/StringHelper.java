package com.binuvcore.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class StringHelper {

    public boolean isWhiteSpaceOrNull(String text){
        if (text == null || text.equals("")){
            return true;
        }
        return false;
    }

    public StringHelper() {
    }

    public String generateRandomClass(){
        String classes[] = {"positive", "negative"};
        return classes[new Random().nextInt(2)];
    }

    public double generateRandomRating(){
        Random r = new Random();
        double min = 0.0;
        double max = 1.0;
        return min + r.nextDouble() * (max - min);
    }

    public String getUniqueTags(String oldTags, String newTags) {

        StringBuilder stringBuilder = new StringBuilder();
        String[] tags = newTags.split(",");
        stringBuilder.append(oldTags);
        for (int i = 0; i < tags.length; i++) {
            if (!stringBuilder.toString().contains(tags[i])) {
                stringBuilder.append("," + tags[i]);
            }
        }
        return stringBuilder.toString();
    }

    public List<String> convertStringIntoTagsList(String tags){
        final String[] re = tags.split(",");
        List<String> tagList = new ArrayList<>();
        for (String tag: re) {
            tagList.add(tag.trim());
        }
        return tagList;
    }

    public String getRandomString(){
        UUID randomString = UUID.randomUUID();
        return String.valueOf(randomString);
    }

    public static void main(String[] args) {
        StringHelper stringHelper = new StringHelper();
        System.out.println(stringHelper.getRandomString());
        System.out.println(stringHelper.getRandomString());
        System.out.println(stringHelper.getRandomString());
    }
}

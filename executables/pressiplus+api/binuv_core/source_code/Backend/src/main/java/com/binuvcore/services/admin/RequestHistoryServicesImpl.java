package com.binuvcore.services.admin;

import com.binuvcore.businessobjects.webapp.analytics.RequestHistory;
import com.binuvcore.businessobjects.webapp.analytics.RequestHistoryTransformer;
import com.binuvcore.helper.date.DateTimeUtils;
import com.binuvcore.helper.StringHelper;
import com.binuvcore.repositories.RequestHistoryRepository;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class RequestHistoryServicesImpl implements RequestHistoryServices {

    @Autowired
    private RequestHistoryRepository requestHistoryRepository;

    private Logger logger = LoggerFactory.getLogger(RequestHistoryServicesImpl.class);

    private StringHelper stringHelper = new StringHelper();

    private InputStream getGeoLiteDatabaseFile() throws IOException {
            return new ClassPathResource("helper/GeoLite2-City.mmdb").getInputStream();
    }


    @Override
    public void addRequestHistory(String ipAddress, String requestUrl) {

        final RequestHistory requestHistory = getAddressDetails(ipAddress);
        if (requestHistory == null || stringHelper.isWhiteSpaceOrNull(ipAddress)) {
            logger.error("Cannot fulfill the request for the given ip address: " + ipAddress);
            return;
        }

        if (ipAddress.equals("134.109.84.203")) {
            return;
        }

        requestHistory.setIpAddress(ipAddress);
        requestHistory.setRequestUrl(requestUrl);
        requestHistory.setVisitedDates(DateTimeUtils.getCurrentDateInLong());
        requestHistoryRepository.save(requestHistory);
    }

    @Override
    public List<RequestHistoryTransformer> getAll() {
        List<RequestHistory> historyList = new ArrayList<>();
        requestHistoryRepository.findAll().forEach(historyList::add);
        if (historyList.size() > 0) {
            historyList.sort(Comparator.comparing(RequestHistory::getId).reversed());
            return transformRequestHistoryList(historyList);
        }
        return null;
    }

    @Override
    public List<RequestHistoryTransformer> getRequestHistroyByIpAddress(String ipAddress) {
        if (stringHelper.isWhiteSpaceOrNull(ipAddress)) {
            logger.error("Request ip Addrss is null.");
            return null;
        }
        List<RequestHistory> historyList = requestHistoryRepository.findAllByIpAddress(ipAddress);
        if (historyList != null) {
            return transformRequestHistoryList(historyList);
        }
        return null;
    }

    @Override
    public List<RequestHistoryTransformer> getRequestHistroyByCity(String city) {
        if (stringHelper.isWhiteSpaceOrNull(city)) {
            logger.error("Request ip Addrss is null.");
            return null;
        }
        List<RequestHistory> historyList = requestHistoryRepository.findAllByIpAddress(city);
        if (historyList != null) {
            return transformRequestHistoryList(historyList);
        }
        return null;
    }

    @Override
    public List<RequestHistoryTransformer> getRequestHistroyByRequestUrl(String requestUrl) {
        if (stringHelper.isWhiteSpaceOrNull(requestUrl)) {
            logger.error("Request ip Addrss is null.");
            return null;
        }
        List<RequestHistory> historyList = requestHistoryRepository.findAllByIpAddress(requestUrl);
        if (historyList != null) {
            return transformRequestHistoryList(historyList);
        }
        return null;
    }

    private RequestHistory getAddressDetails(final String ipAddress) {

        InputStream database = null;
        try {
            database = getGeoLiteDatabaseFile();
        } catch (FileNotFoundException e) {
            logger.error("GeoLite2City database file cannot be found. " + e);
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("File is found and send to Database Reader");
        DatabaseReader dbReader = null;
        RequestHistory requestHistory = null;
        try {
            dbReader = new DatabaseReader.Builder(database)
                    .build();
            InetAddress ipAdd = InetAddress.getByName(ipAddress);
            CityResponse response = dbReader.city(ipAdd);
            if (response != null) {
                 requestHistory = new RequestHistory();
                requestHistory.setCountry(response.getCountry().getName());
                requestHistory.setPostcode(response.getPostal().getCode());
                requestHistory.setState(response.getLeastSpecificSubdivision().getName());
                requestHistory.setCity(response.getCity().getName());
            }
        } catch (IOException | GeoIp2Exception e) {
            logger.error("Cannot identify location for give ip address" + ipAddress + ". " +e);
        }
        return requestHistory;
    }

    private List<RequestHistoryTransformer> transformRequestHistoryList(List<RequestHistory> requestHistories) {
        List<RequestHistoryTransformer> responseList = new ArrayList<>();
        for (RequestHistory request : requestHistories) {
            RequestHistoryTransformer transformer = new RequestHistoryTransformer();
            transformer.setCity(request.getCity());
            transformer.setCountry(request.getCountry());
            transformer.setIp_address(request.getIpAddress());
            transformer.setPostcode(request.getPostcode());
            transformer.setRequestUrl(request.getRequestUrl());
            transformer.setState(request.getState());
            transformer.setVisite_dates(DateTimeUtils.convertLongDateToStringDateShortFormat(request.getVisitedDates()));
            responseList.add(transformer);
        }
        return responseList;
    }
}

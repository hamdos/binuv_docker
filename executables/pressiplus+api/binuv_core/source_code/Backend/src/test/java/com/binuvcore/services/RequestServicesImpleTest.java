package com.binuvcore.services;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import static org.junit.Assert.assertTrue;

public class RequestServicesImpleTest {

    @Test
    public void whenResourceAsFile_thenReadSuccessful() throws IOException {

        File resource = new ClassPathResource("helper/GeoLite2-City.mmdb").getFile();
        assertTrue(resource.getAbsolutePath().contains("GeoLite2-City.mmdb"));
    }
}

package com.binuvcore.services;

import com.binuvcore.businessobjects.access.User;

import static com.binuvcore.static_data.PersonStaticData.getStaticListOfPerson;
import static com.binuvcore.static_data.PersonStaticData.getStaticPerson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.binuvcore.services.business.impl.UserServicesImpl;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import org.junit.runner.RunWith;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


/**
 * Test for {@link UserServicesImpl}
 *
 * @author doston, 18.12.2019
 */
public class UserBaseServiceImplTest {

}

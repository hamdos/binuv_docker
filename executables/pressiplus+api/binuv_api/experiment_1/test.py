import pandas as pd
import numpy as np
from . import model_builder
import json
from . import APP_DIR
from . import evaluator

df = pd.read_json(f'{APP_DIR}/data/input.json', orient='records')
docs = df['entities']

result = evaluator.evaluate(docs)

print(result)
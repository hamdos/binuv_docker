import pandas as pd
import numpy as np
import json
from . import model_builder
from . import APP_DIR

# Loading data
df = pd.read_json(f'{APP_DIR}/data/fp_ds_1.json', orient='records')
df = df.apply(lambda x: (x["entities"], x["class"]), axis=1)

docs = df.tolist()

# Extracting features
vocab, class_labels = [], []
for doc in docs:
  vocab.extend(doc[0])
  class_labels.append(doc[1])
vocab = sorted(set(vocab))
# TODO: stem each word in vocab
class_labels = sorted(set(class_labels))

features = {
  'vocab': vocab,
  'class_labels': class_labels
}

with open(f'{APP_DIR}/model/features.json', 'w') as fp:
  json.dump(features, fp, ensure_ascii=False)


# Building training data
train_set = []
output_empty = [0] * len(class_labels)  # an empty array for our output
for doc in docs:
  tokens = doc[0]
  bow = model_builder.build_bow(tokens, vocab)

  cls = doc[1]
  output_row = list(output_empty)
  output_row[class_labels.index(cls)] = 1

  train_set.append([bow, output_row])

train_set = np.array(train_set)

x_train = list(train_set[:, 0])
y_train = list(train_set[:, 1])

# building model
model = model_builder.build_nn(len(x_train[0]), len(y_train[0]))
model.fit(x_train,
          y_train,
          validation_set=0.33,
          n_epoch=10,
          batch_size=8,
          show_metric=True
)
model.save(f'{APP_DIR}/model/fp.tfl')

print('==============================')
print('Done!')
